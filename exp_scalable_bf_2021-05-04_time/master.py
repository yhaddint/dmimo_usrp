#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Master
# GNU Radio version: 3.7.13.5
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import commpy
import howto
import numpy
import numpy as np
import sip
import sys
import time
from gnuradio import qtgui


class master(gr.top_block, Qt.QWidget):

    def __init__(self, const=digital.constellation_calcdist((digital.psk_2()[0]), (digital.psk_2()[1]), 2, 1).base(), fname='../data_bf.dat', freq=921e6, n_pkts=1000, skp_pkts=200, trig_level=1, tx_gain=0.85):
        gr.top_block.__init__(self, "Master")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Master")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "master")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Parameters
        ##################################################
        self.const = const
        self.fname = fname
        self.freq = freq
        self.n_pkts = n_pkts
        self.skp_pkts = skp_pkts
        self.trig_level = trig_level
        self.tx_gain = tx_gain

        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 8
        self.samp_rate = samp_rate = 1e6
        self.eb = eb = 0.35
        self.timing_margin2 = timing_margin2 = int(11000 *samp_rate/1e6)
        self.rxmod = rxmod = digital.generic_mod(const, False, sps, True, eb, False, False)
        self.preamble = preamble = [0xac, 0xdd, 0xa4, 0xe2, 0xf2, 0x8c, 0x20, 0xfc]
        self.nfilts = nfilts = 32
        self.data_bytes = data_bytes = [0x5a,0x5c,0x4a,0xdf,0xcf,0xe1,0xe2,0xb9,0xe0,0x10,0xc7,0xcd,0xd1,0x14,0x32,0xe3,0x8e,0x49,0xb0,0x8d,0xa3,0x06,0xb5,0x8e,0x3a,0x7b,0xa7,0xa9,0x95,0x22,0x42,0xd6,0xd9,0x13,0x6d,0x91,0xfe,0xca,0x03,0x63,0x33,0xd2,0x89,0xa5,0x34,0x81,0x87,0x34,0xc6,0x78,0xce,0xa7,0x57,0xe2,0x3e,0x69,0x8b,0xe6,0x1b,0x9b,0x6e,0xb8,0x52,0xd3,0xa3,0xea,0x6e,0x4a,0xf3,0x38,0xa5,0xee,0x61,0x2f,0xdc,0x56,0x40,0x18,0xe5,0x89,0xad,0xe4,0x17,0x5d,0x97,0x3f,0x5c,0x59,0x8f,0x63,0xe8,0xd7,0x16,0x11,0xb3,0x11,0x6c,0xb3,0xc4,0xbf,0x12,0x71,0x4b,0xb4,0x63,0xb6,0x49,0xb4,0x88,0xf9,0x7b,0xce,0xbd,0x13,0xe6,0xcc,0x4c,0x54,0x4f,0x4d,0xba,0xa3,0x6d,0xa9,0xc3,0x8b,0x3c,0xe4,0x66,0x46,0x05,0x26,0xab,0x64,0xe1,0x46,0x43,0xfb,0x84,0xf6,0x0d,0xb7,0x74,0x00,0xfb,0xf0,0x23,0x1b,0xbf,0x0b,0x25,0x41,0xed,0x9f,0xfe,0x77,0x0c,0xe0,0xda,0x71,0x30,0xf6,0xd5,0x66,0x0c,0x62,0x40,0x2f,0xd5,0xc1,0x01,0x5f,0xfd,0x13,0xfa,0x40,0xc0,0xb9,0xec,0x97,0x35,0xfd,0x9b,0x4b,0xf4,0x3e,0xf0,0x3b,0x1e,0xdc,0x5b,0x7b,0x46,0x7b,0x7b,0x05,0x83,0xea,0x13,0xcd,0xea,0xdb,0x18,0xd1,0x85,0xfa,0x56,0x75,0x51,0x9e,0xee,0xc3,0x7b,0xf8,0x88,0xa4,0x3a,0x23,0x84,0x63,0x3c,0xbe,0x8a,0x29,0xdc,0xa2,0xf2,0x8f,0x07,0x0b,0x96,0x0f,0xac,0xd3,0xb7,0x0f,0x37,0x33,0xbb,0xec,0xe0,0xdd,0x9d,0x16,0xdc,0xa3,0xa1,0x8a,0x27,0x64,0x90,0x45,0xea,0x47,0xd5,0x91,0x00,0x6c,0xe4,0xd9,0x20,0xb9,0xfd,0x6b,0x63,0x3d,0x17,0xd5,0x9e,0x31,0xc5,0x29,0x67,0x11,0x31,0xcc,0xd6,0x2d,0xca,0xd6,0x92,0xe8,0x0b,0x2d,0x33,0xa2,0x70,0x05,0x99,0x2e,0x65,0x8c,0xc9,0xc4,0x79,0x01,0x6b,0xaf,0x74,0x3d,0xd7,0xb4,0xf6,0xc6,0x96,0xa8,0x15,0x1d,0x9f,0x0f,0xe0,0xc7,0x27,0x1b,0x42,0x2c,0x27,0xb2,0x72,0x49,0xe3,0x65,0x52,0xfa,0x47,0xc6,0x74,0x64,0xba,0xfd,0x1d,0xa0,0xe2,0x21,0xb9,0x4c,0xc8,0xce,0x65,0x7b,0x98,0x12,0x70,0xd4,0xb8,0x56,0x8a,0xd4,0x6e,0x81,0xb5,0x0f,0xa9,0x28,0xa7,0x4c,0x53,0xb8,0x62,0x4b,0xfd,0xe7,0xe7,0x99,0xb7,0x93,0x06,0xa6,0x56,0xf6,0x8a,0x9d,0xbc,0xbb,0x7e,0xa0,0xe9,0x7d,0xed,0xb0,0x22,0x5b,0xf8,0xa2,0x9e,0xec,0xc6,0x76,0x01,0x2e,0x3d,0x0e,0x1a,0x4c,0x28,0xc5,0xe0,0x9c,0x0f,0xa1,0xbc,0x7b,0x5c,0xbb,0xc1,0xca,0xc2,0x3f,0x4c,0x47,0xd2,0x75,0x42,0x71,0x2e,0x00,0x48,0xd6,0x6c,0x8b,0x18,0x8e,0x81,0x51,0xe0,0xfa,0x57,0xd1,0xc1,0x77,0x1b,0xd1,0x19,0x46,0x7c,0xff,0x4b,0x30,0x5b,0xc2,0x0e,0x2d,0xbf,0x2b,0x7b,0x28,0x39,0x7d,0x3a,0xea,0xad,0xd1,0x8d,0x9d,0x9e,0xfb,0x8d,0xd2,0x11,0x0f,0xf4,0x29,0x2c,0xcd,0xd7,0x0c,0xf0,0x72,0x7f,0x4d,0x58,0xc2,0x2b,0x50,0x21,0x64,0x9d,0xbb,0x10,0x97,0xff,0xdc,0x98,0x3d,0x64,0x1e,0x73,0x38,0xe9,0x7b,0xef,0x01,0x1c,0x4a,0x38,0x9a,0xb3,0xe8,0x12,0x2e,0xf0,0x9f,0xa1,0x28,0x7a,0x4a,0xf1,0xd3,0xcd,0x87,0xe9,0x7b,0x3f,0x42,0xdd,0x18,0x8a,0xc6,0x2b,0xc6,0xd1,0xac,0xba,0xc9,0xa2,0x6a,0x8c,0x76,0x38,0x6b,0xb9,0x48,0xe3,0xe8,0x5c,0x92,0x45,0xf8,0xb7,0xd3,0x67,0x13,0x8e,0xe8,0xda,0xf0,0x6b,0x69,0x74,0x75,0x58,0xbc,0x3c,0xa6,0xb3,0x22,0x16,0x6b,0x57,0xc5,0x2c,0xde,0x05,0xe9,0x2c,0xbd,0xf4,0x40,0xd3,0xa8,0x14,0x74,0x34,0x08,0x70,0xfe,0x9e,0x18,0x55,0x1f,0xf4,0x25,0xa1,0x93,0x3a,0x11,0x50,0xd8,0xd0,0xb6,0x22,0x2a,0xaf,0xff,0xd4,0x67,0xb0,0xc3,0x5d,0xe7,0x34,0x2f,0x22,0x6b,0x98,0xc9,0x3f,0xa9,0xc0,0x6a,0x60,0xff,0x6a,0x51,0xe2,0x7e,0x39]
        self.actual_data_len = actual_data_len = 1000
        self.timing_margin = timing_margin = int((6000)*samp_rate/1e6)

        self.rrc_taps = rrc_taps = firdes.root_raised_cosine(nfilts, nfilts*sps, 1.0, eb, 5*sps*nfilts)

        self.modulated_sync_word = modulated_sync_word = digital.modulate_vector_bc(rxmod .to_basic_block(), (preamble), ([1]))
        self.modulated_data = modulated_data = digital.modulate_vector_bc(rxmod .to_basic_block(), (data_bytes), ([1]))
        self.guard_time = guard_time = int(5000*samp_rate/1e6)
        self.data_len = data_len = timing_margin2 + actual_data_len
        self.zcs = zcs = commpy.sequences.zcsequence(25,63)
        self.tx_start_guard = tx_start_guard = 0
        self.tag_delay = tag_delay = 32
        self.start_gap_1 = start_gap_1 = 400
        self.second_zeros = second_zeros = 0
        self.pkt_len = pkt_len = timing_margin + data_len + guard_time
        self.phase_feedback_len = phase_feedback_len = 100
        self.phase_feedback_delay = phase_feedback_delay = 4000
        self.phase_det_len = phase_det_len = 200
        self.payload_len = payload_len = 625
        self.payload = payload = np.concatenate((modulated_sync_word,modulated_data))
        self.ntaps = ntaps = len(rrc_taps)
        self.n_train = n_train = 10
        self.gap = gap = int(30000*samp_rate/1e6)
        self.feedback = feedback = 1
        self.addr = addr = ""

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0_1_0 = uhd.usrp_source(
        	",".join((addr, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0_1_0.set_clock_source('internal', 0)
        self.uhd_usrp_source_0_1_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0_1_0.set_center_freq(uhd.tune_request(freq,10e6), 0)
        self.uhd_usrp_source_0_1_0.set_normalized_gain(0.8, 0)
        self.uhd_usrp_source_0_1_0.set_auto_dc_offset(False, 0)
        self.uhd_usrp_source_0_1_0.set_auto_iq_balance(False, 0)
        self.uhd_usrp_sink_0_0_0 = uhd.usrp_sink(
        	",".join((addr, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0_0_0.set_clock_source('internal', 0)
        self.uhd_usrp_sink_0_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0_0_0.set_center_freq(uhd.tune_request(freq, 10e6), 0)
        self.uhd_usrp_sink_0_0_0.set_normalized_gain(tx_gain, 0)
        self.uhd_usrp_sink_0_0_0.set_antenna('TX/RX', 0)
        self.qtgui_time_sink_x_4 = qtgui.time_sink_c(
        	30000, #size
        	samp_rate, #samp_rate
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_4.set_update_time(0.10)
        self.qtgui_time_sink_x_4.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_4.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_4.enable_tags(-1, True)
        self.qtgui_time_sink_x_4.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_4.enable_autoscale(False)
        self.qtgui_time_sink_x_4.enable_grid(False)
        self.qtgui_time_sink_x_4.enable_axis_labels(True)
        self.qtgui_time_sink_x_4.enable_control_panel(False)
        self.qtgui_time_sink_x_4.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_4.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_4.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_4.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_4.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_4.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_4.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_4.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_4.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_4.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_4_win = sip.wrapinstance(self.qtgui_time_sink_x_4.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_4_win)
        self.qtgui_time_sink_x_0_1_2_1 = qtgui.time_sink_c(
        	int(30000*samp_rate/1e6), #size
        	samp_rate, #samp_rate
        	"", #name
        	2 #number of inputs
        )
        self.qtgui_time_sink_x_0_1_2_1.set_update_time(0.10)
        self.qtgui_time_sink_x_0_1_2_1.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0_1_2_1.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_1_2_1.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_1_2_1.set_trigger_mode(qtgui.TRIG_MODE_NORM, qtgui.TRIG_SLOPE_POS, trig_level, 0, 0, "")
        self.qtgui_time_sink_x_0_1_2_1.enable_autoscale(False)
        self.qtgui_time_sink_x_0_1_2_1.enable_grid(True)
        self.qtgui_time_sink_x_0_1_2_1.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_1_2_1.enable_control_panel(False)
        self.qtgui_time_sink_x_0_1_2_1.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_1_2_1.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(4):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_0_1_2_1.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0_1_2_1.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0_1_2_1.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_1_2_1_win = sip.wrapinstance(self.qtgui_time_sink_x_0_1_2_1.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_1_2_1_win)
        self.howto_log_mimo_0 = howto.log_mimo(timing_margin+timing_margin2+start_gap_1+phase_det_len*2+len(zcs)*n_train, 1, actual_data_len+100)
        self.howto_level_trigger_0_0 = howto.level_trigger(pkt_len, trig_level)
        self.howto_gen_phase_msg_0_0 = howto.gen_phase_msg(phase_feedback_len)
        self.howto_gen_phase_msg_0 = howto.gen_phase_msg(phase_feedback_len)
        self.howto_det_abs_phase_msg_0_0 = howto.det_abs_phase_msg(timing_margin+1200+phase_det_len+(n_train-10)*63, phase_det_len)
        self.howto_det_abs_phase_msg_0 = howto.det_abs_phase_msg(timing_margin+1200+(n_train-10)*63, phase_det_len)
        self.blocks_vector_source_x_0_1_1 = blocks.vector_source_c((0, ), True, 1, [])
        self.blocks_vector_source_x_0_1_0 = blocks.vector_source_c((0.8*feedback, ), True, 1, [])
        self.blocks_vector_source_x_0_1 = blocks.vector_source_c((0, ), True, 1, [])
        self.blocks_vector_source_x_0_0_0_0_0 = blocks.vector_source_c(zcs*0.8, True, 1, [])
        self.blocks_stream_mux_0_0 = blocks.stream_mux(gr.sizeof_gr_complex*1, ([zcs.size*n_train, timing_margin - zcs.size*n_train+phase_feedback_delay]+[ phase_feedback_len]*(3) + [data_len-3*phase_feedback_len-phase_feedback_delay+guard_time+gap]))
        self.blocks_skiphead_0 = blocks.skiphead(gr.sizeof_gr_complex*1, (actual_data_len+512)*skp_pkts)
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_vcc((feedback, ))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((feedback, ))
        self.blocks_head_0 = blocks.head(gr.sizeof_gr_complex*1, (actual_data_len+512)*n_pkts)
        self.blocks_float_to_complex_0_0 = blocks.float_to_complex(1)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, fname, False)
        self.blocks_file_sink_0.set_unbuffered(False)
        self.blocks_complex_to_mag_0_0_0 = blocks.complex_to_mag(1)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.howto_det_abs_phase_msg_0, 'phase_out'), (self.howto_gen_phase_msg_0, 'phase_in'))
        self.msg_connect((self.howto_det_abs_phase_msg_0_0, 'phase_out'), (self.howto_gen_phase_msg_0_0, 'phase_in'))
        self.connect((self.blocks_complex_to_mag_0_0_0, 0), (self.blocks_float_to_complex_0_0, 0))
        self.connect((self.blocks_complex_to_mag_0_0_0, 0), (self.howto_level_trigger_0_0, 0))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.qtgui_time_sink_x_0_1_2_1, 0))
        self.connect((self.blocks_head_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.blocks_head_0, 0), (self.qtgui_time_sink_x_4, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_stream_mux_0_0, 3))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_stream_mux_0_0, 4))
        self.connect((self.blocks_skiphead_0, 0), (self.blocks_head_0, 0))
        self.connect((self.blocks_stream_mux_0_0, 0), (self.uhd_usrp_sink_0_0_0, 0))
        self.connect((self.blocks_vector_source_x_0_0_0_0_0, 0), (self.blocks_stream_mux_0_0, 0))
        self.connect((self.blocks_vector_source_x_0_1, 0), (self.blocks_stream_mux_0_0, 1))
        self.connect((self.blocks_vector_source_x_0_1_0, 0), (self.blocks_stream_mux_0_0, 2))
        self.connect((self.blocks_vector_source_x_0_1_1, 0), (self.blocks_stream_mux_0_0, 5))
        self.connect((self.howto_gen_phase_msg_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.howto_gen_phase_msg_0_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))
        self.connect((self.howto_level_trigger_0_0, 0), (self.howto_det_abs_phase_msg_0, 0))
        self.connect((self.howto_level_trigger_0_0, 0), (self.howto_det_abs_phase_msg_0_0, 0))
        self.connect((self.howto_level_trigger_0_0, 0), (self.howto_log_mimo_0, 0))
        self.connect((self.howto_log_mimo_0, 0), (self.blocks_skiphead_0, 0))
        self.connect((self.uhd_usrp_source_0_1_0, 0), (self.blocks_complex_to_mag_0_0_0, 0))
        self.connect((self.uhd_usrp_source_0_1_0, 0), (self.howto_det_abs_phase_msg_0, 1))
        self.connect((self.uhd_usrp_source_0_1_0, 0), (self.howto_det_abs_phase_msg_0_0, 1))
        self.connect((self.uhd_usrp_source_0_1_0, 0), (self.howto_log_mimo_0, 1))
        self.connect((self.uhd_usrp_source_0_1_0, 0), (self.qtgui_time_sink_x_0_1_2_1, 1))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "master")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_const(self):
        return self.const

    def set_const(self, const):
        self.const = const
        self.set_rxmod(digital.generic_mod(self.const, False, self.sps, True, self.eb, False, False))

    def get_fname(self):
        return self.fname

    def set_fname(self, fname):
        self.fname = fname
        self.blocks_file_sink_0.open(self.fname)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.uhd_usrp_source_0_1_0.set_center_freq(uhd.tune_request(self.freq,10e6), 0)
        self.uhd_usrp_sink_0_0_0.set_center_freq(uhd.tune_request(self.freq, 10e6), 0)

    def get_n_pkts(self):
        return self.n_pkts

    def set_n_pkts(self, n_pkts):
        self.n_pkts = n_pkts
        self.blocks_head_0.set_length((self.actual_data_len+512)*self.n_pkts)

    def get_skp_pkts(self):
        return self.skp_pkts

    def set_skp_pkts(self, skp_pkts):
        self.skp_pkts = skp_pkts

    def get_trig_level(self):
        return self.trig_level

    def set_trig_level(self, trig_level):
        self.trig_level = trig_level
        self.qtgui_time_sink_x_0_1_2_1.set_trigger_mode(qtgui.TRIG_MODE_NORM, qtgui.TRIG_SLOPE_POS, self.trig_level, 0, 0, "")

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self.uhd_usrp_sink_0_0_0.set_normalized_gain(self.tx_gain, 0)


    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.set_rxmod(digital.generic_mod(self.const, False, self.sps, True, self.eb, False, False))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_timing_margin2(int(11000 *self.samp_rate/1e6))
        self.set_timing_margin(int((6000)*self.samp_rate/1e6))
        self.set_guard_time(int(5000*self.samp_rate/1e6))
        self.set_gap(int(30000*self.samp_rate/1e6))
        self.uhd_usrp_source_0_1_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_0_0_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_4.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_0_1_2_1.set_samp_rate(self.samp_rate)

    def get_eb(self):
        return self.eb

    def set_eb(self, eb):
        self.eb = eb
        self.set_rxmod(digital.generic_mod(self.const, False, self.sps, True, self.eb, False, False))

    def get_timing_margin2(self):
        return self.timing_margin2

    def set_timing_margin2(self, timing_margin2):
        self.timing_margin2 = timing_margin2
        self.set_data_len(self.timing_margin2 + self.actual_data_len)

    def get_rxmod(self):
        return self.rxmod

    def set_rxmod(self, rxmod):
        self.rxmod = rxmod

    def get_preamble(self):
        return self.preamble

    def set_preamble(self, preamble):
        self.preamble = preamble

    def get_nfilts(self):
        return self.nfilts

    def set_nfilts(self, nfilts):
        self.nfilts = nfilts

    def get_data_bytes(self):
        return self.data_bytes

    def set_data_bytes(self, data_bytes):
        self.data_bytes = data_bytes

    def get_actual_data_len(self):
        return self.actual_data_len

    def set_actual_data_len(self, actual_data_len):
        self.actual_data_len = actual_data_len
        self.set_data_len(self.timing_margin2 + self.actual_data_len)
        self.blocks_head_0.set_length((self.actual_data_len+512)*self.n_pkts)

    def get_timing_margin(self):
        return self.timing_margin

    def set_timing_margin(self, timing_margin):
        self.timing_margin = timing_margin
        self.set_pkt_len(self.timing_margin + self.data_len + self.guard_time)

    def get_rrc_taps(self):
        return self.rrc_taps

    def set_rrc_taps(self, rrc_taps):
        self.rrc_taps = rrc_taps
        self.set_ntaps(len(self.rrc_taps))

    def get_modulated_sync_word(self):
        return self.modulated_sync_word

    def set_modulated_sync_word(self, modulated_sync_word):
        self.modulated_sync_word = modulated_sync_word
        self.set_payload(np.concatenate((self.modulated_sync_word,self.modulated_data)))

    def get_modulated_data(self):
        return self.modulated_data

    def set_modulated_data(self, modulated_data):
        self.modulated_data = modulated_data
        self.set_payload(np.concatenate((self.modulated_sync_word,self.modulated_data)))

    def get_guard_time(self):
        return self.guard_time

    def set_guard_time(self, guard_time):
        self.guard_time = guard_time
        self.set_pkt_len(self.timing_margin + self.data_len + self.guard_time)

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len
        self.set_pkt_len(self.timing_margin + self.data_len + self.guard_time)

    def get_zcs(self):
        return self.zcs

    def set_zcs(self, zcs):
        self.zcs = zcs
        self.blocks_vector_source_x_0_0_0_0_0.set_data(self.zcs*0.8, [])

    def get_tx_start_guard(self):
        return self.tx_start_guard

    def set_tx_start_guard(self, tx_start_guard):
        self.tx_start_guard = tx_start_guard

    def get_tag_delay(self):
        return self.tag_delay

    def set_tag_delay(self, tag_delay):
        self.tag_delay = tag_delay

    def get_start_gap_1(self):
        return self.start_gap_1

    def set_start_gap_1(self, start_gap_1):
        self.start_gap_1 = start_gap_1

    def get_second_zeros(self):
        return self.second_zeros

    def set_second_zeros(self, second_zeros):
        self.second_zeros = second_zeros

    def get_pkt_len(self):
        return self.pkt_len

    def set_pkt_len(self, pkt_len):
        self.pkt_len = pkt_len

    def get_phase_feedback_len(self):
        return self.phase_feedback_len

    def set_phase_feedback_len(self, phase_feedback_len):
        self.phase_feedback_len = phase_feedback_len

    def get_phase_feedback_delay(self):
        return self.phase_feedback_delay

    def set_phase_feedback_delay(self, phase_feedback_delay):
        self.phase_feedback_delay = phase_feedback_delay

    def get_phase_det_len(self):
        return self.phase_det_len

    def set_phase_det_len(self, phase_det_len):
        self.phase_det_len = phase_det_len

    def get_payload_len(self):
        return self.payload_len

    def set_payload_len(self, payload_len):
        self.payload_len = payload_len

    def get_payload(self):
        return self.payload

    def set_payload(self, payload):
        self.payload = payload

    def get_ntaps(self):
        return self.ntaps

    def set_ntaps(self, ntaps):
        self.ntaps = ntaps

    def get_n_train(self):
        return self.n_train

    def set_n_train(self, n_train):
        self.n_train = n_train

    def get_gap(self):
        return self.gap

    def set_gap(self, gap):
        self.gap = gap

    def get_feedback(self):
        return self.feedback

    def set_feedback(self, feedback):
        self.feedback = feedback
        self.blocks_vector_source_x_0_1_0.set_data((0.8*self.feedback, ), [])
        self.blocks_multiply_const_vxx_0_0.set_k((self.feedback, ))
        self.blocks_multiply_const_vxx_0.set_k((self.feedback, ))

    def get_addr(self):
        return self.addr

    def set_addr(self, addr):
        self.addr = addr


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "", "--fname", dest="fname", type="string", default='../data_bf.dat',
        help="Set fname [default=%default]")
    parser.add_option(
        "", "--freq", dest="freq", type="eng_float", default=eng_notation.num_to_str(921e6),
        help="Set freq [default=%default]")
    parser.add_option(
        "", "--n-pkts", dest="n_pkts", type="intx", default=1000,
        help="Set n_pkts [default=%default]")
    parser.add_option(
        "", "--skp-pkts", dest="skp_pkts", type="intx", default=200,
        help="Set skp_pkts [default=%default]")
    parser.add_option(
        "", "--trig-level", dest="trig_level", type="eng_float", default=eng_notation.num_to_str(1),
        help="Set trig_level [default=%default]")
    parser.add_option(
        "", "--tx-gain", dest="tx_gain", type="eng_float", default=eng_notation.num_to_str(0.85),
        help="Set tx_gain [default=%default]")
    return parser


def main(top_block_cls=master, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(fname=options.fname, freq=options.freq, n_pkts=options.n_pkts, skp_pkts=options.skp_pkts, trig_level=options.trig_level, tx_gain=options.tx_gain)
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
