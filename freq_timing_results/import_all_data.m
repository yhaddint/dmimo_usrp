
fldr ='/media/samer/CORES/Samer/transfer/freq_timing_8/'
fls = dir([fldr,'cap5-01_*.csv']);
d = cell(length(fls),1);
idx = 1;
for fni = 1: length(fls)
    x = import_data(sprintf([fldr,'%s'],fls(fni).name));
    if rms(x(:,1))>1e-3 && rms(x(:,2))>1e-3 && rms(x(:,3))>1e-3
        d{idx}=x;
        idx
        idx=idx+1;
    else
        0;
    end
    fni
end
Ts=8e-9;
d = d(1:idx-1);
save('data9','d','Ts','-v7.3')
