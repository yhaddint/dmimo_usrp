%load sn
function [T1,ph1,a1] = estimate_frequency(sn)
x=sn;
[mx,mx_loc] = findpeaks(x);
[mn,mn_loc] = findpeaks(-x);
mn = -mn;

sd = diff(sign(x));
xrs_up = find(sd>0)+1;
xrs_dn = find(sd<0)+1;

pnts = sort([mx_loc;mn_loc;xrs_up;xrs_dn]);

qrtr= median(diff(pnts));
T1= qrtr*4;
ph1 = xrs_up(1);

a1 = (median(mx) - median(mn))/2;


% t=1:length(x);

% figure;
% s1= a1*sin(2*pi/T1*(t-ph1));
% hold off
% plot(x)
% hold on;
% plot(s1)
end

% t= interp(real(lteZadoffChuSeq(979,981)),25);
% plot(t*1e4)
% hold on
% plot(real(pkt1))
% lags,cr = xcorr(real(pkt1),t);
% plot(cr)