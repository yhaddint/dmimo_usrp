

function [d1,d2] = read_my_format (filename,index)




    count = 52e3*25;


  f = fopen (filename, 'rb');
  if (f < 0)
    cv = 0;
  else
    fseek(f,index*2*2*count,-1);
    v = fread (f, count*2, 'short');
    fclose (f);
    cv = v(1:2:end)+v(2:2:end)*1j;
  end
d1=cv(1:2:end);
d2=cv(2:2:end);