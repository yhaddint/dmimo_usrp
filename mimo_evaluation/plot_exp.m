path = '2020-02-13/';

exp1 = {'a1','a2','a3','a4','a5','a6','a7','a8'};
exp2 = {'b1','b2','b3','b4','b5','b6','b7','b8'};

exp = exp2;

phase_diff_tot = [];
indx_tot = [];
for i = 1:length(exp)
     exp_i = exp{i};
     [phase_diff_tx1, phase_diff_tx2, mag_rx1_tx1, mag_rx1_tx2, mag_rx2_tx1 ,mag_rx2_tx2] = ...
     process_measurement(path,exp_i);
     phase_diff = mod(phase_diff_tx1 -phase_diff_tx2  + pi ,2*pi)-pi;
     
    phase_diff_tot = [phase_diff_tot,phase_diff];
    indx_tot = [indx_tot, i*ones(size(phase_diff)) ];
     
end
figure
boxplot(phase_diff_tot,indx_tot)
axis([0.5 length(exp)+0.5 -pi pi])
xlabel('Point')
ylabel('Phase Difference')