% path = '2020-02-04/';
% 
% usrpRx1 = read_complex_binary([path 'usrp1.dat']);
% usrpRx2 = read_complex_binary([path 'usrp2.dat']);
% 
% close all
% 
% indx = (900000:1100000)-300000;
% usrpRx1 = usrpRx1(indx);
% usrpRx2 = usrpRx2(indx);

function [phase_rx1_tx1,phase_rx2_tx1,phase_rx1_tx2,phase_rx2_tx2,mag_rx1_tx1, mag_rx1_tx2, mag_rx2_tx1 ,mag_rx2_tx2] ...
                      =  process_iq(usrpRx1,usrpRx2)

           
[mag_rx1_tx1,mag_rx1_tx2,phase_rx1_tx1,phase_rx1_tx2,keep_rx1]=process_usrpRx(usrpRx1,0.5e-3);
[mag_rx2_tx1,mag_rx2_tx2,phase_rx2_tx1,phase_rx2_tx2,keep_rx2]=process_usrpRx(usrpRx2,0.5e-3);

[keep1,keep2]= align_end(keep_rx1,keep_rx2);
keep = keep1&keep2;

phase_rx1_tx1 = apply_keep(phase_rx1_tx1,keep);
phase_rx1_tx2 = apply_keep(phase_rx1_tx2,keep);
phase_rx2_tx1 = apply_keep(phase_rx2_tx1,keep);
phase_rx2_tx2 = apply_keep(phase_rx2_tx2,keep);

mag_rx1_tx1 = apply_keep(mag_rx1_tx1,keep);
mag_rx1_tx2 = apply_keep(mag_rx1_tx2,keep);
mag_rx2_tx1 = apply_keep(mag_rx2_tx1,keep);
mag_rx2_tx2 = apply_keep(mag_rx2_tx2,keep);
 


% figure;
% plot((phase_rx1_tx1-phase_rx2_tx1))
% hold on
% plot((phase_rx1_tx2-phase_rx2_tx2))

% hold off
% plot(real(usrp1))
% hold on
% plot(imag(usrp1))

end

function x=apply_keep(x,keep)
    x = x(1:length(keep));
    x = x(keep);
end

function [x,y]=align_end(x,y)
    t = min(length(x),length(y));
    x = x(1:t);
    y = y(1:t);
end
function [mag_part1,mag_part2,phase_part1,phase_part2,keep]=process_usrpRx(usrpRx,thresh)
    zrs_loc = find(usrpRx==0);
    lst_zrs = diff(zrs_loc)>1;
    lst_zrs = zrs_loc(lst_zrs);


    parts_len = (20000+512)/8;

    i = 25;

    strt_offset = lst_zrs(1);
    if strt_offset ==1
        strt_offset = lst_zrs(2);
    end
    n_pkts = floor( (length(usrpRx)-strt_offset)/parts_len);
    end_offset = strt_offset + n_pkts*parts_len-1;
    usrpRx_sync = usrpRx(strt_offset: end_offset);

    usrpRx_pkts = reshape(usrpRx_sync, [parts_len,n_pkts] );

    part_skip = 140; 
    part1_indx = part_skip: parts_len/2 -part_skip;
    part2_indx = part1_indx +parts_len/2;

    part1 = usrpRx_pkts(part1_indx,:);
    part2 = usrpRx_pkts(part2_indx,:);

    samp_part1 = sample_part(part1);
    samp_part2 = sample_part(part2);

    mag_samp_part1 = abs(samp_part1);
    mag_samp_part2 = abs(samp_part2);

    mag_part1 = mean(mag_samp_part1,1);
    mag_part2 = mean(mag_samp_part2,1);

    phase_samp_part1 = unwrap(angle(samp_part1));
    phase_samp_part2 = unwrap(angle(samp_part2));

    phase_part1 = mean(phase_samp_part1,1);
    phase_part2 = mean(phase_samp_part2,1);

    %thresh = 1e-3;
    keep = mag_part1>thresh & mag_part2>thresh;



%     phase_part1 = phase_part1(keep);
%     phase_part2 = phase_part2(keep);
%     mag_part1 =  mag_part1(keep);
%     mag_part2 =  mag_part2(keep);

    %plot( phase_part1 )

    %plot( mod(phase_part1 - phase_part2,2*pi))
end


function samp_part = sample_part(part)
    seq_len = 20;

    nseq = floor(size(part,1)/seq_len );
    part_len = nseq * seq_len;
    part = part(1:part_len,:);
    seq_part = reshape(part, [part_len/nseq,nseq,size(part,2)]);

    mag_seq_part = mean(abs(seq_part),2);
    [~,max_loc_seq_part] = max(mag_seq_part,[],1);


    samp_part = zeros(size(seq_part,2),size(seq_part,3));
    for j=1:size(seq_part,3)
        samp_part(:,j) = seq_part(max_loc_seq_part(j),:,j);
    end
end
% part1 = usrpRx1(lst_zrs(i)+ after_zr :lst_zrs(i)+after_zr+part_len);
% part2 = usrpRx1(lst_zrs(i)+ after_zr + part_len :lst_zrs(i)+after_zr+2*part_len);
% plot(real(part1))