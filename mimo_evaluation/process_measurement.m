% path = '2020-02-09/';
% 
% close all ;
% 
% prefix  ='b4'; 

function [phase_diff_tx1,phase_diff_tx2,  mag_rx1_tx1, mag_rx1_tx2, mag_rx2_tx1 ,mag_rx2_tx2] =...
             process_measurement(path,prefix)
         
usrpRx1 = read_complex_binary([path prefix '_usrp1.dat']);
usrpRx2 = read_complex_binary([path  prefix '_usrp2.dat']);

[phase_rx1_tx1,phase_rx2_tx1,phase_rx1_tx2,phase_rx2_tx2, ...
    mag_rx1_tx1, mag_rx1_tx2, mag_rx2_tx1 ,mag_rx2_tx2]  ...
                      =  process_iq(usrpRx1,usrpRx2);
   
phase_diff_tx1 = phase_rx1_tx1-phase_rx2_tx1;
phase_diff_tx2 = phase_rx1_tx2-phase_rx2_tx2;



% 
% figure;
% plot(mod(phase_diff_tx1 -phase_diff_tx2  + pi ,2*pi)-pi)
% figure;
% plot(mod(phase_diff_tx1,2*pi))
% hold on
% plot(mod(phase_diff_tx2,2*pi))
% 
% figure;
% plot(mag_rx1_tx1)
% hold on
% plot(mag_rx1_tx2)
% plot(mag_rx2_tx1)
% plot(mag_rx2_tx2)