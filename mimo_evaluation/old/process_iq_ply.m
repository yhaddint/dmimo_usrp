path = '2020-02-02/';

usrpRx1 = read_complex_binary([path 'usrp1.dat']);

usrpRx1 = usrpRx1(1:100000);

% hold off
% plot(real(usrp1))
% hold on
% plot(imag(usrp1))

close all;

zrs_loc = find(usrpRx1==0);
lst_zrs = diff(zrs_loc)>1;
lst_zrs = zrs_loc(lst_zrs);

after_zr = 88-8+13-1;

parts_len = (20000+512)/8;

i = 25;

strt_offset = lst_zrs(1);
n_pkts = floor( (length(usrpRx1)-strt_offset)/parts_len);
end_offset = strt_offset + n_pkts*parts_len-1;
usrpRx1_sync = usrpRx1(strt_offset: end_offset);

usrpRx1_pkts = reshape(usrpRx1_sync, [parts_len,n_pkts] );

part_skip = 140; 
part1_indx = part_skip: parts_len/2 -part_skip;
part2_indx = part1_indx +parts_len/2;

part1 = usrpRx1_pkts(part1_indx,:);
part2 = usrpRx1_pkts(part2_indx,:);

plot(abs(part1(:,i)))

seq_len = 20;

nseq = floor(size(part1,1)/seq_len );
part_len = nseq * seq_len;
part = part1(1:part_len,:);
seq_part = reshape(part, [part_len/nseq,nseq,size(part,2)]);

plot(abs(seq_part(:,:,i)))
mag_seq_part = mean(abs(seq_part),2);
plot(mag_seq_part(:,i))
[~,max_loc_seq_part] = max(mag_seq_part,[],1);
[~,min_loc_seq_part] = min(mag_seq_part,[],1);


samp_part = zeros(size(seq_part,2),size(seq_part,3));
for j=1:size(seq_part,3)
    samp_part(:,j) = seq_part(max_loc_seq_part(j),:,j);
end
abs(samp_part(i))

mag_samp_part = abs(samp_part);
phase_samp_part = angle(samp_part);
figure;
plot(phase_samp_part)
figure;
plot(mag_samp_part)

% part1 = usrpRx1(lst_zrs(i)+ after_zr :lst_zrs(i)+after_zr+part_len);
% part2 = usrpRx1(lst_zrs(i)+ after_zr + part_len :lst_zrs(i)+after_zr+2*part_len);
% plot(real(part1))