%function [mag1_mean,mag2_mean,phase_diff_mean,phase1_mean,phase2_mean] = get_mag_phase(fname)
% fname = '0.389386177063_-1.0266225338';
% [mag1_mean,mag2_mean,phase_diff_mean] = get_mag_phase(fname)
fname = '2019-12-16/data_';
%offset = 15*8;

%fname = '2020-02-02/data_';

offset = 15*8;


close all

samp_len = 10000/8;

mag1 = trim(get_data([fname 'usrp_1_mag']),samp_len,offset);
mag2 = trim(get_data([fname 'usrp_2_mag']),samp_len,offset);
phase1 = trim(get_data([fname 'usrp_1_phase']),samp_len,offset);
phase2 = trim(get_data([fname 'usrp_2_phase']),samp_len,offset);









phase_diff1 = mod((phase1-phase2+pi),2*pi)-pi;
phase_diff2 = mod((phase1-phase2),2*pi);
if var(phase_diff1)<var(phase_diff2)
    phase_diff = phase_diff1;
else
    phase_diff = phase_diff2;
end
shft =pi - mean(phase_diff);
phase_diff = mod((phase1-phase2+shft),2*pi)-shft;


% 
% figure;
% hold off
% plot((mag1(:)))
% hold on
% plot((mag2(:)))
% plot(phase_diff(:))


mag1 = reshape(mag1,samp_len,length(mag1)/samp_len);
mag2 = reshape(mag2,samp_len,length(mag2)/samp_len);
phase_diff = reshape(phase_diff,samp_len,length(phase_diff)/samp_len);


s1 = 1;
s2 = 2;
stp = 2;
usrp11_mag = mag1(:,s1:stp:end);
usrp12_mag = mag1(:,s2:stp:end);

usrp21_mag = mag2(:,s1:stp:end);
usrp22_mag = mag2(:,s2:stp:end);





usrp1_phase_diff = phase_diff(:,s1:stp:end); 
usrp2_phase_diff = phase_diff(:,s2:stp:end);




% figure;
% hold off
% plot((usrp11_mag(1:samp_len:end)))
% hold on
% plot((usrp12_mag(1:samp_len:end)))
% plot(usrp1_phase_diff(1:samp_len:end))
% 
% figure;
% hold off
% plot((usrp21_mag(100:samp_len:end)))
% hold on
% plot((usrp22_mag(100:samp_len:end)))
% plot(usrp2_phase_diff(100:samp_len:end))

figure;
hold on
plot(usrp1_phase_diff(100:samp_len:end))
plot(usrp2_phase_diff(100:samp_len:end))
legend('Tx1','Tx2')
title('Phase Difference between Receivers')
xlabel('Packet Index')
ylabel('Phase')

figure;
hold on
plot((usrp11_mag(100:samp_len:end)))
plot((usrp12_mag(100:samp_len:end)))
legend('|h12|','|h21|')
title('Mag from Tx 1')
xlabel('Packet Index')
ylabel('Magnitude')


figure;
hold on
plot((usrp21_mag(100:samp_len:end)))
plot((usrp22_mag(100:samp_len:end)))
legend('|h21|','|h22|')
title('Mag from Tx 2')
xlabel('Packet Index')
ylabel('Magnitude')

%end

function x = trim(x,samp_len,offset)
x = x (offset:end);
x =x(1:floor(length(x)/samp_len)*samp_len);
end

function v = get_data(filename)
f = fopen ([filename '.dat'], 'rb');
 v = fread (f, inf, 'float');
 v = v(1:end);
 fclose all;
end
