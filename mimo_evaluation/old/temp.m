x = phase1;
d = [true; diff(x(:)) ~= 0];   % TRUE if values change
B = x(d);                      % Elements without repetitions
k = find([d', true]);          % Indices of changes
N = diff(k);                   % Number of repetitions
strts = find(N>2500);
i = 184;
hold off
plot(phase1(k(strts(i)+1): k(strts(i)+1) +20000/8 ))
hold on
plot(phase2(k(strts(i)+1): k(strts(i)+1) +20000/8 ))