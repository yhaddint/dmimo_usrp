fc= 915e6;
lb = 3e8/fc;


dr= 0.22 ;



x_max = 5000;
y_max = 5000;



dt = 15;
strt = 20;
stp = 1.25;


% tx1 = [ones(1,8)*dt/2; strt+(0:7)*stp];
% tx2 = [ones(1,8)*-dt/2; strt+(0:7)*stp];

tx1 = [ones(1,4)*dt/2 -ones(1,4)*dt/2; strt+([0:3 3:-1:0])*stp];
tx2 = [ones(1,4)*dt/2 -ones(1,4)*dt/2; strt+([4:7 7:-1:4])*stp];

rx1 = [-dr/2;0];
rx2 = [dr/2;0];

d_tx1_rx1 = vecnorm(rx1-tx1);
d_tx1_rx2 =  vecnorm(rx2-tx1);
d_tx2_rx1 = vecnorm(rx1-tx2);
d_tx2_rx2 =  vecnorm(rx2-tx2);


dph_tx1 = mod(2*pi*(d_tx1_rx1-d_tx1_rx2)/lb + pi,2*pi) -  pi;
dph_tx2 = mod(2*pi*(d_tx2_rx1-d_tx2_rx2)/lb+ pi,2*pi) - pi;


dph = mod(dph_tx2 - dph_tx1+pi,2*pi)-pi;
figure;

plot(dph)
axis([0.5 length(exp)+0.5 -pi pi])

xlabel('Point')
ylabel('Phase Difference')