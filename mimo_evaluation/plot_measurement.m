path = '2020-02-13/';

close all ;

prefix  ='a5'; 

[phase_diff_tx1,phase_diff_tx2,  mag_rx1_tx1, mag_rx1_tx2, mag_rx2_tx1 ,mag_rx2_tx2] =...
             process_measurement(path,prefix);
         
         

figure;
plot(mod(phase_diff_tx1 -phase_diff_tx2  + pi ,2*pi)-pi)
figure;
plot(mod(phase_diff_tx1,2*pi))
hold on
plot(mod(phase_diff_tx2,2*pi))

figure;
plot(mag_rx1_tx1)
hold on
plot(mag_rx1_tx2)
plot(mag_rx2_tx1)
plot(mag_rx2_tx2)