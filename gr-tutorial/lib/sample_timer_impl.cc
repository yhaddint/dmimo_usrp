/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <gnuradio/gr_complex.h>
#include "sample_timer_impl.h"

namespace gr {
  namespace tutorial {

    sample_timer::sptr
    sample_timer::make(float sample_rate)
    {
      return gnuradio::get_initial_sptr
        (new sample_timer_impl(sample_rate));
    }

    /*
     * The private constructor
     */
    sample_timer_impl::sample_timer_impl(float sample_rate)
      : gr::sync_block("sample_timer",
              gr::io_signature::make2(2, 2, sizeof(gr_complex), sizeof(uint8_t)),
              gr::io_signature::make(2, 2, sizeof(gr_complex))),
        Ts(1 / sample_rate), current_time(0.0f)
    {
    }

    /*
     * Our virtual destructor.
     */
    sample_timer_impl::~sample_timer_impl()
    {
    }

    int
    sample_timer_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in_signal = (const gr_complex *) input_items[0];
      const uint8_t *in_reset = (const uint8_t *) input_items[1];
      gr_complex *out_signal = (gr_complex *) output_items[0];
      gr_complex *out_time = (gr_complex *) output_items[1];

      memcpy(out_signal, in_signal, sizeof(gr_complex) * noutput_items);

      for (int i = 0; i < noutput_items; i++)
      {
        if(in_reset[i])
            current_time = 0.0f;
        out_time[i] = current_time;
        current_time += Ts;
      }
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace tutorial */
} /* namespace gr */

