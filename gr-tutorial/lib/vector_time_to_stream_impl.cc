/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <gnuradio/gr_complex.h>
#include <vector>
#include "vector_time_to_stream_impl.h"

namespace gr {
  namespace tutorial {

    vector_time_to_stream::sptr
    vector_time_to_stream::make(std::vector<gr_complex> tx_vector, float sample_rate)
    {
      return gnuradio::get_initial_sptr
        (new vector_time_to_stream_impl(tx_vector, sample_rate));
    }

    /*
     * The private constructor
     */
    vector_time_to_stream_impl::vector_time_to_stream_impl(std::vector<gr_complex> tx_vector, float sample_rate)
      : gr::sync_block("vector_time_to_stream",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
        tx_vector(tx_vector), sample_rate(sample_rate)
    {}

    /*
     * Our virtual destructor.
     */
    vector_time_to_stream_impl::~vector_time_to_stream_impl()
    {
    }

    int
    vector_time_to_stream_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];

      int dec = noutput_items;
      while(dec--)
      {
        *out++ = tx_vector[(int)((*in++).real() * sample_rate) % tx_vector.size()];
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace tutorial */
} /* namespace gr */

