/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_TUTORIAL_CFO_EKF_IMPL_H
#define INCLUDED_TUTORIAL_CFO_EKF_IMPL_H

#include <tutorial/CFO_EKF.h>

namespace gr {
  namespace tutorial {

    class CFO_EKF_impl : public CFO_EKF
    {
     private:
      // Nothing to declare in this block.

      // struct Holdoff
      // {
      //   int counter;
      //   int holdoff_length;

      //   Holdoff(int length) : counter(0), holdoff_length(length) {}

      //   bool trigger()
      //   {
      //       if(counter > 0)
      //           return false;

      //       counter = holdoff_length;

      //       return true;
      //   }

      //   void tick()
      //   {
      //       if(counter > 0)
      //           counter--;
      //   }
      // } holdoff;

      bool d_first_samp;
      bool d_cycle_active;

      bool perform_oneshot;
      // bool last_trigger_level;
      int cnt_samp;
      int cnt_train_samp;
      float oneshot_cfo_est;
      gr_complex corr_sum;
      int d_Ntrain;
      int d_Ndelay;
      float d_Tcycle;
      float d_samp_rate;
      float Ts;
      float mag_iq;
      int cycle_samples;

      struct State
      {
        // 2x1
        float x_hat[2][1];

        // 3x1
        float z[3][1];

        // 3x1
        float y_tilde[3][1];

        float H[3][2];
        float P[2][2];
        float Q[2][2];
        float F[2][2];
        float F_t[2][2];
        float R[3][3];
        float K[2][3];
        float S[3][3];
      } state;

     public:
      CFO_EKF_impl(float samp_rate,
                    int Ntrain,
                    int Ndelay,
                    float initial_frequency,
                    std::vector<float> state_update_cov_matrix,
                    std::vector<float> one_shot_matrix,
                    float t_holdoff);
      ~CFO_EKF_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace tutorial
} // namespace gr

#endif /* INCLUDED_TUTORIAL_CFO_EKF_IMPL_H */

