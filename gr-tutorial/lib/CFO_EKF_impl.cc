/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "CFO_EKF_impl.h"
#include <vector>
#include <cassert>

#define _USE_MATH_DEFINES

#include <math.h>

#define ELEM(type,ptr,sr,sc,r,c) (*(((type*)(ptr)) + ((sc) * (r) + (c))))

namespace gr {
  namespace tutorial {

    template<class Tvv>
    bool check_dimension(Tvv v, int r, int c)
    {
        return v.size() == r * c;
    }

    void mat_zero(void* mat_a, int mr, int mc)
    {
        memset(mat_a, 0, sizeof(float) * mr * mc);
    }

    void mat_ident(void* mat_a, int mr, int mc)
    {
        assert(mr == mc);

        mat_zero(mat_a, mr, mc);
        for(int i = 0; i < mr; i++)
            ELEM(float,mat_a,mr,mc,i,i) = 1;
    }

    void mmul(const void* mat_a, int mar, int mac,
              const void* mat_b, int mbr, int mbc,
              void* mat_o)
    {
        assert(mac == mbr);

        // inner product length
        int ipl = mac;

        #define AELEM(r,c) ELEM(float,mat_a,mar,mac,r,c)
        #define BELEM(r,c) ELEM(float,mat_b,mbr,mbc,r,c)
        #define OELEM(r,c) ELEM(float,mat_o,mar,mbc,r,c)

        for(int orow = 0; orow < mar; orow++)
        {
            for(int oc = 0; oc < mbc; oc++)
            {
                float accum = 0.0f;
                for(int ipi = 0; ipi < ipl; ipi++)
                {
                    accum += AELEM(orow,ipi) * BELEM(ipi,oc);
                }
                OELEM(orow,oc) = accum;
            }

        }

        #undef AELEM
        #undef BELEM
        #undef OELEM
    }

    void madd(const void* mat_a, const void* mat_b, int mr, int mc, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mr,mc,i,j) = ELEM(float,mat_a,mr,mc,i,j) +
                                              ELEM(float,mat_b,mr,mc,i,j);
    }

    void msub(const void* mat_a, const void* mat_b, int mr, int mc, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mr,mc,i,j) = ELEM(float,mat_a,mr,mc,i,j) -
                                              ELEM(float,mat_b,mr,mc,i,j);
    }

    void mtrans(const void* mat, int mr, int mc, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mc,mr,j,i) = ELEM(float,mat,mr,mc,i,j);
    }

    void m_s_mul(const void* mat, int mr, int mc, float m, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mr,mc,i,j) = ELEM(float,mat,mr,mc,i,j) * m;
    }

    void m_s_div(const void* mat, int mr, int mc, float d, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mr,mc,i,j) = ELEM(float,mat,mr,mc,i,j) / d;
    }

    float mdet_2_2(const void* mat)
    {
        return ELEM(float,mat,2,2,0,0) * ELEM(float,mat,2,2,1,1) -
               ELEM(float,mat,2,2,0,1) * ELEM(float,mat,2,2,1,0);
    }

    float mdet_3_3(const void* mat)
    {
        float det =
            (ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,1,1) * ELEM(float,mat,3,3,2,2)) +
            (ELEM(float,mat,3,3,1,0) * ELEM(float,mat,3,3,2,1) * ELEM(float,mat,3,3,0,2)) +
            (ELEM(float,mat,3,3,2,0) * ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,1,2)) -
            (ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,2,1) * ELEM(float,mat,3,3,1,2)) -
            (ELEM(float,mat,3,3,2,0) * ELEM(float,mat,3,3,1,1) * ELEM(float,mat,3,3,0,2)) -
            (ELEM(float,mat,3,3,1,0) * ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,2,2));

        return det;
    }

    void minv_2_2(const void* mat, void* mat_o)
    {
        float det = mdet_2_2(mat);

        ELEM(float,mat_o,2,2,0,0) = ELEM(float,mat,2,2,1,1) / det;
        ELEM(float,mat_o,2,2,0,1) = ELEM(float,mat,2,2,0,1) / -det;
        ELEM(float,mat_o,2,2,1,0) = ELEM(float,mat,2,2,1,0) / -det;
        ELEM(float,mat_o,2,2,1,1) = ELEM(float,mat,2,2,0,0) / det;
    }

    void minv_3_3(const void* mat, void* mat_o)
    {
        float det = mdet_3_3(mat);

        float mat_i[3][3];

        ELEM(float,mat_i,3,3,0,0) = ELEM(float,mat,3,3,1,1) * ELEM(float,mat,3,3,2,2) -
                                    ELEM(float,mat,3,3,1,2) * ELEM(float,mat,3,3,2,1);
        ELEM(float,mat_i,3,3,0,1) = ELEM(float,mat,3,3,0,2) * ELEM(float,mat,3,3,2,1) -
                                    ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,2,2);
        ELEM(float,mat_i,3,3,0,2) = ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,1,2) -
                                    ELEM(float,mat,3,3,0,2) * ELEM(float,mat,3,3,1,1);
        ELEM(float,mat_i,3,3,1,0) = ELEM(float,mat,3,3,1,2) * ELEM(float,mat,3,3,2,0) -
                                    ELEM(float,mat,3,3,1,0) * ELEM(float,mat,3,3,2,2);
        ELEM(float,mat_i,3,3,1,1) = ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,2,2) -
                                    ELEM(float,mat,3,3,0,2) * ELEM(float,mat,3,3,2,0);
        ELEM(float,mat_i,3,3,1,2) = ELEM(float,mat,3,3,0,2) * ELEM(float,mat,3,3,1,0) -
                                    ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,1,2);
        ELEM(float,mat_i,3,3,2,0) = ELEM(float,mat,3,3,1,0) * ELEM(float,mat,3,3,2,1) -
                                    ELEM(float,mat,3,3,1,1) * ELEM(float,mat,3,3,2,0);
        ELEM(float,mat_i,3,3,2,1) = ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,2,0) -
                                    ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,2,1);
        ELEM(float,mat_i,3,3,2,2) = ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,1,1) -
                                    ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,1,0);

        m_s_div(mat_i, 3, 3, det, mat_o);
    }

    template<class Tvv>
    void mat_from_vec(const void* mat, int mr, int mc, Tvv vec)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat,mr,mc,i,j) = vec[i*mc + j];
    }

    CFO_EKF::sptr
    CFO_EKF::make(float samp_rate,
                  int Ntrain,
                  int Ndelay,
                  float initial_frequency,
                  std::vector<float> state_update_cov_matrix,
                  std::vector<float> one_shot_matrix,
                  float t_holdoff)
    {
      assert(check_dimension(state_update_cov_matrix, 2, 2));
      assert(check_dimension(one_shot_matrix, 3, 3));

      return gnuradio::get_initial_sptr
        (new CFO_EKF_impl(samp_rate, Ntrain, Ndelay, initial_frequency,
                          state_update_cov_matrix, one_shot_matrix, t_holdoff));
    }

    /*
     * The private constructor
     */
    CFO_EKF_impl::CFO_EKF_impl(float samp_rate,
                               int Ntrain,
                               int Ndelay,
                               float initial_frequency,
                               std::vector<float> state_update_cov_matrix,
                               std::vector<float> one_shot_matrix,
                               float t_holdoff)
      : gr::sync_block("CFO_EKF",
              gr::io_signature::make2(2, 2, sizeof(gr_complex),
                                      sizeof(uint8_t)),
              gr::io_signature::make(4, 4, sizeof(float))),
      d_samp_rate(samp_rate),
      d_Ntrain(Ntrain),
      d_Ndelay(Ndelay),
      //holdoff(t_holdoff * samp_rate),
      d_Tcycle(0),
      d_cycle_active(false),
      d_first_samp(true)
    {
        //last_trigger_level = false;
        perform_oneshot = false;
        cycle_samples = 0;

        Ts = 1/d_samp_rate;
        mag_iq = 1;

        state.x_hat[0][0] = 0;
        state.x_hat[1][0] = initial_frequency;

        mat_from_vec(state.Q, 2, 2, state_update_cov_matrix);
        mat_from_vec(state.R, 3, 3, one_shot_matrix);

        // state update mtx
        // Assume 1ms to start with; this cycle time will be calculated on the
        // fly later.
        state.F[0][0] = 1; state.F[0][1] = 2*M_PI*0.001;
        state.F[1][0] = 0; state.F[1][1] = 1;

        // initial prediction as identity
        mat_ident(state.P, 2, 2);

        mtrans(state.F,2,2,state.F_t);

        cnt_samp = 0;
        cnt_train_samp = 0;
        corr_sum = 0;
        oneshot_cfo_est = 0;

        set_history(d_Ndelay + 1);
    }

    /*
     * Our virtual destructor.
     */
    CFO_EKF_impl::~CFO_EKF_impl()
    {
    }

    int
    CFO_EKF_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in_iq = (const gr_complex *) input_items[0];
      const uint8_t *in_trigger = (const uint8_t *) input_items[1];
      float *out_phase = (float *) output_items[0];
      float *out_cfo_est = (float *) output_items[1];
      float *out_oneshotcfo_est = (float *) output_items[2];
      float *out_tcycle = (float *) output_items[3];

      float i2_2_2[2][2];
      float i_2_1[2][1];
      float i_2_2[2][2];
      float i_2_3[2][3];
      float i_3_2[3][2];
      float i_3_3[3][3];
      float H_t[2][3];
      float x_update[2][1];

      float ident_2_2[2][2] = {{1, 0},
                               {0, 1}};

      float P_update[2][2];

      for (int i = 0; i < noutput_items; i++)
      {
          // one shot estimation

          //holdoff.tick();

          cycle_samples++;

          //if (in_trigger[i] && !last_trigger_level && holdoff.trigger())
          if (in_trigger[i])
          {  
             if (d_cycle_active == false){
                //std::cout << "New cycle started " << std::endl;
                d_cycle_active = true;

                perform_oneshot = true;
                cnt_samp = 0;
                cnt_train_samp = 0;
                corr_sum = 0;

                /* Update F with the delta in samples since the last oneshot
                 * estimation
                 */
                state.F[0][1] = (2 * M_PI * cycle_samples) / d_samp_rate;
                d_Tcycle = cycle_samples;
                cycle_samples = 0;
              }
          }

          out_tcycle[i] = d_Tcycle;


          //last_trigger_level = in_trigger[i];

          if(perform_oneshot)
          {
            // during T_train
            if ((cnt_samp > d_Ndelay) && (cnt_samp < d_Ntrain))
            {
              corr_sum = corr_sum + in_iq[i + d_Ndelay] * std::conj(in_iq[i]);
            } // end of T_train

            // trigger one-shot estimation and EKF updates
            if (cnt_samp == d_Ntrain - 1)
            {
              perform_oneshot = false;

              oneshot_cfo_est = std::arg(corr_sum) / (d_Ndelay * Ts * 2 * M_PI);

              // z is 3x1
              mag_iq = sqrt(real(in_iq[i]) * real(in_iq[i]) + imag(in_iq[i]) *
                            imag(in_iq[i]));
              if (mag_iq==0.){
                mag_iq = 1.;
              }

              // if (d_first_samp && d_cycle_active ){

              //   state.x_hat[1][0] = oneshot_cfo_est;
              //   d_first_samp = false;
              //   std::cout << "Set initial value " << oneshot_cfo_est << std::endl;

              // }

              state.z[0][0] = real(in_iq[i])/mag_iq;
              state.z[1][0] = imag(in_iq[i])/mag_iq;
              state.z[2][0] = oneshot_cfo_est;

              // std::cout << state.z[0][0] << " " << state.z[1][0] << " " << state.z[2][0] << std::endl;

              float sxp = sin(state.x_hat[0][0]);
              float cxp = cos(state.x_hat[0][0]);

              // y_tilde is 3x1
              state.y_tilde[0][0] = state.z[0][0] - cxp;
              state.y_tilde[1][0] = state.z[1][0] - sxp;
              state.y_tilde[2][0] = state.z[2][0] - state.x_hat[1][0];

              // Hessian Matrix 3x2
              state.H[0][0] = -sxp; state.H[0][1] = 0;
              state.H[1][0] =  cxp; state.H[1][1] = 0;
              state.H[2][0] = 0;    state.H[2][1] = 1;

              mmul(state.H,3,2,state.P,2,2,i_3_2);
              mtrans(state.H,3,2,H_t);
              mmul(i_3_2,3,2,H_t,2,3,i_3_3);
              madd(i_3_3,state.R,3,3,state.S);

              mmul(state.P,2,2,H_t,2,3,i_2_3);
              minv_3_3(state.S,i_3_3);
              mmul(i_2_3,2,3,i_3_3,3,3,state.K);

              mmul(state.K,2,3,state.y_tilde,3,1,i_2_1);
              madd(i_2_1,state.x_hat,2,1,x_update);

              mmul(state.K,2,3,state.H,3,2,i_2_2);
              msub(ident_2_2,i_2_2,2,2,i2_2_2);
              mmul(i2_2_2,2,2,state.P,2,2,P_update);

              mmul(state.F,2,2,x_update,2,1,state.x_hat);

              mmul(state.F,2,2,P_update,2,2,i_2_2);
              mmul(i_2_2,2,2,state.F_t,2,2,i2_2_2);
              madd(state.Q,i2_2_2,2,2,state.P);



              d_cycle_active = false;
              cnt_samp = 0;
            }
          }

          if(state.x_hat[0][0] > (2 * M_PI))
              state.x_hat[0][0] -= (2 * M_PI);

          out_phase[i] = state.x_hat[0][0];
          out_cfo_est[i] = state.x_hat[1][0];
          out_oneshotcfo_est[i] = state.z[2][0];
          
          if (d_cycle_active){
            cnt_samp++;
          }
      }
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace tutorial */
} /* namespace gr */

