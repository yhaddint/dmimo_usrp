/* -*- c++ -*- */

#define TUTORIAL_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "tutorial_swig_doc.i"

%{
#include "tutorial/square.h"
#include "tutorial/CFO_EKF.h"
#include "tutorial/sample_timer.h"
#include "tutorial/vector_time_to_stream.h"
%}


%include "tutorial/square.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, square);
%include "tutorial/CFO_EKF.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, CFO_EKF);
%include "tutorial/sample_timer.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, sample_timer);
%include "tutorial/vector_time_to_stream.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, vector_time_to_stream);
