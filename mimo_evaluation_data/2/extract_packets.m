% path = '2020-02-04/';
% 
% usrpRx1 = read_complex_binary([path 'usrp1.dat']);
% usrpRx2 = read_complex_binary([path 'usrp2.dat']);
% 
% close all
% 
% indx = (900000:1100000)-300000;
% usrpRx1 = usrpRx1(indx);
% usrpRx2 = usrpRx2(indx);

function  [p1_rx1_tx1,p2_rx1_tx2,p3_rx1,p1_rx2_tx1,p2_rx2_tx2,p3_rx2] ...
                      =  extract_packets(usrpRx1,usrpRx2)

           
[p1_rx1_tx1,p2_rx1_tx2,p3_rx1_tx2,keep_rx1]=process_usrpRx(usrpRx1,0.02e-3,18);
[p1_rx2_tx1,p2_rx2_tx2,p3_rx2_tx2,keep_rx2]=process_usrpRx(usrpRx2,0.02e-3,0);

[keep1,keep2]= align_end(keep_rx1,keep_rx2);
keep = keep1&keep2;

p1_rx1_tx1 = apply_keep(p1_rx1_tx1,keep);
p2_rx1_tx2 = apply_keep(p2_rx1_tx2,keep);
p3_rx1 = apply_keep(p3_rx1_tx2,keep);

p1_rx2_tx1 = apply_keep(p1_rx2_tx1,keep);
p2_rx2_tx2 = apply_keep(p2_rx2_tx2,keep);
p3_rx2 = apply_keep(p3_rx2_tx2,keep);
 


% figure;
% plot((phase_rx1_tx1-phase_rx2_tx1))
% hold on
% plot((phase_rx1_tx2-phase_rx2_tx2))

% hold off
% plot(real(usrp1))
% hold on
% plot(imag(usrp1))

end

function x=apply_keep(x,keep)
    x = x(:,1:length(keep));
    x = x(:,keep);
end

function [x,y]=align_end(x,y)
    t = min(length(x),length(y));
    x = x(1:t);
    y = y(1:t);
end
function [part1,part2,part3,keep]=process_usrpRx(usrpRx,thresh,del)
    zrs_loc = find(usrpRx==0);
    lst_zrs = diff(zrs_loc)>1;
    lst_zrs = zrs_loc(lst_zrs);


    parts_len = (30000+512);

    i = 25;

    strt_offset = lst_zrs(1);
    if strt_offset ==1
        strt_offset = lst_zrs(2);
    end
    strt_offset = strt_offset+del;
    n_pkts = floor( (length(usrpRx)-strt_offset)/parts_len);
    end_offset = strt_offset + n_pkts*parts_len-1;
    usrpRx_sync = usrpRx(strt_offset: end_offset);

    usrpRx_pkts = reshape(usrpRx_sync, [parts_len,n_pkts] );


    part1_indx = 80-79: 9867;
    part2_indx = 9970+55+56-79:19868;
    part3_indx = 19970+55+56-79:29868;

    part1 = usrpRx_pkts(part1_indx,:);
    part2 = usrpRx_pkts(part2_indx,:);
    part3 = usrpRx_pkts(part3_indx,:);


    strt_indx = 1:800;
    pld_indx = 910:9800;
    part1_strt = part1(strt_indx,:);
    part2_strt = part2(strt_indx,:);
    
    mag_samp_part1 = abs(part1_strt);
    mag_samp_part2 = abs(part2_strt);

    mag_part1 = mean(mag_samp_part1,1);
    mag_part2 = mean(mag_samp_part2,1);



    %thresh = 1e-3;
    keep = mag_part1>thresh & mag_part2>thresh;



%     phase_part1 = phase_part1(keep);
%     phase_part2 = phase_part2(keep);
%     mag_part1 =  mag_part1(keep);
%     mag_part2 =  mag_part2(keep);

    %plot( phase_part1 )

    %plot( mod(phase_part1 - phase_part2,2*pi))
end



% part1 = usrpRx1(lst_zrs(i)+ after_zr :lst_zrs(i)+after_zr+part_len);
% part2 = usrpRx1(lst_zrs(i)+ after_zr + part_len :lst_zrs(i)+after_zr+2*part_len);
% plot(real(part1))