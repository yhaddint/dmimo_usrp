% path = '2020-02-09/';
% 
% close all ;
% 
% prefix  ='b4'; 

function [p1_rx1_tx1,p2_rx1_tx2,p1_rx2_tx1,p2_rx2_tx2,s1,s2] =...
             load_combine(path,prefix)
         
usrpRx1 = read_complex_binary([path prefix '_usrp1.dat']);
usrpRx2 = read_complex_binary([path  prefix '_usrp2.dat']);

[p1_rx1_tx1,p2_rx1_tx2,p3_rx1,p1_rx2_tx1,p2_rx2_tx2,p3_rx2]...
                      =  extract_packets(usrpRx1,usrpRx2);
   

strt_indx = 1:800;
h11 = mean(p3_rx1(1:120,:),1);
h12 = mean(p3_rx1(200:400,:),1);
h21 = mean(p3_rx2(1:120,:),1);
h22 = mean(p3_rx2(200:400,:),1);


% hh11 = conj(h11).* h11 + conj(h21).* h21;
% hh12 =  conj(h11).* h12 + conj(h21).* h22;
% hh21 =  conj(h12).* h11 + conj(h22).* h21;
% hh22 =  conj(h12).* h12 + conj(h22).* h22;
% 
% dt = hh11.*hh22-h12.*h21;
% hhi11 = hh22./dt;
% hhi12  = hh12./dt;
% hhi21  = hh21./dt;
% hhi22  = hh11./dt;
% 
% hzf11 = conj(h11).* hhi11 + conj(h21).* hhi21;
% hzf12 =  conj(h11).* hhi12 + conj(h21).* hhi22;
% hzf21 =  conj(h12).* hhi11 + conj(h22).* hhi21;
% hzf22 =  conj(h12).* hhi12 + conj(h22).* hhi22;
% 
% s1 = hzf11.* p3_rx1 + hzf12.* p3_tx2;
% s2 = hzf21.* p3_rx1 + hzf22.* p3_tx2;

s1 = zeros(size(p3_rx1,1),size(p3_rx1,2));
s2 = zeros(size(p3_rx1,1),size(p3_rx1,2));
for i=1:size(p3_rx1,2)
    H = [h11(i) h12(i); h21(i) h22(i)];
    mimo_sig = [p3_rx1(:,i) p3_rx2(:,i)].';% [p1_rx1_tx1(:,i)+p2_rx1_tx2(:,i)  p1_rx2_tx1(:,i)+p2_rx2_tx2(:,i)].';%
    sep_sig = inv(H'*H+(6e-7)*eye(2))*H'*mimo_sig;
    s1(:,i)=sep_sig(1,:)';
    s2(:,i)=sep_sig(2,:)';
end
'snr'
20*log10(abs(H)/6e-7)
indx = 200;


% figure;
% hold on
% plotc(p1_rx1_tx1(:,indx))
% hold on
% plotc(s1(:,indx))
% 
% figure;
% hold on
% plotc(p2_rx1_tx2(:,indx))
% hold on
% plotc(s2(:,indx))


% figure;
% hold on
% plot(real(p3_rx1(:,indx))*100)
% hold on
% plot(real(s2(:,indx)))

0;
