path = '2020-07-02/';

% close all ;

prefix  ='mimo'; 

[p1_rx1_tx1,p2_rx1_tx2,p1_rx2_tx1,p2_rx2_tx2,s1,s2] =...
             load_combine(path,prefix);
x=randn(1000,1)+1j*randn(1000,1);


[ref1,ref2] = get_ref_sig();

sps =2;

ds_ref1 = my_downsample(ref1,3,sps);
ds_ref2 = my_downsample(ref2,0,sps);


pkt_corr_rx1_tx1 = eval_data(p1_rx1_tx1,ds_ref1,sps);
pkt_corr_rx1_tx2 = eval_data(p2_rx1_tx2,ds_ref2,sps);
 
pkt_corr_rx2_tx1 = eval_data(p1_rx2_tx1,ds_ref1,sps);
pkt_corr_rx2_tx2 = eval_data(p2_rx2_tx2,ds_ref2,sps);

pkt_corr_s1 = eval_data(s1,ds_ref1,sps);
pkt_corr_s2 = eval_data(s2,ds_ref2,sps);

%%
figure(10)
hold off
plot(1-pkt_corr_rx1_tx1)
hold on
plot(1-pkt_corr_rx1_tx2)
plot(1-pkt_corr_rx2_tx1)
plot(1-pkt_corr_rx2_tx2)
plot(1-pkt_corr_s1)
plot(1-pkt_corr_s2)
legend('pkt1 rx1','pkt2 rx1','pkt1 rx2','pkt2 rx2','pkt1 MIMO','pkt2 MIMO')
ylabel('BER')
xlabel('PKT Index')

figure(11)
hold off
plot(max(pkt_corr_rx1_tx1,pkt_corr_rx2_tx1)./(pkt_corr_s1))
hold on
plot(max(pkt_corr_rx1_tx2,pkt_corr_rx2_tx2)./(pkt_corr_s2))
legend('pkt1','pkt2')
ylabel('Accuracy MIMO/SISO')
xlabel('PKT Index')