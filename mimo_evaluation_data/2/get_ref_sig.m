function [ref1,ref2]  = get_ref_sig()
node1=read_complex_binary('node1.dat');
node2=read_complex_binary('node2.dat');
ref1  = node1(117+2:10000)/4;
ref1 = ref1(1:floor(numel(ref1)/8)*8);
ref2  = node2(117+3:10000)/4;
ref2 = ref2(1:floor(numel(ref2)/8)*8);
end