path = '2020-06-25/';

% close all ;

prefix  ='mimo'; 

[p1_rx1_tx1,p2_rx1_tx2,p1_rx2_tx1,p2_rx2_tx2,s1,s2] =...
             load_combine(path,prefix);


[ref1,ref2] = get_ref_sig();

sps =2;

ds_ref1 = my_downsample(ref1,3,sps);
ds_ref2 = my_downsample(ref2,0,sps);


pkt_corr_rx1_tx1 = eval_data(p1_rx1_tx1,ds_ref1,sps);
pkt_corr_rx1_tx2 = eval_data(p2_rx1_tx2,ds_ref2,sps);

pkt_corr_rx2_tx1 = eval_data(p1_rx2_tx1,ds_ref1,sps);
pkt_corr_rx2_tx2 = eval_data(p2_rx2_tx2,ds_ref2,sps);

pkt_corr_s1 = eval_data(s1,ds_ref1,sps);
pkt_corr_s2 = eval_data(s2,ds_ref2,sps);

%%
figure(10)
hold off
plot(pkt_corr_rx1_tx1)
hold on
plot(pkt_corr_rx1_tx2)
plot(pkt_corr_rx2_tx1)
plot(pkt_corr_rx2_tx2)
plot(pkt_corr_s1)
plot(pkt_corr_s2)