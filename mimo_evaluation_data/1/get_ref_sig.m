function [ref1,ref2]  = get_ref_sig()
load('payload')
ref1  = node1(165:10000)/4;
ref1 = ref1(1:floor(numel(ref1)/8)*8);
ref2  = node2(168:10000)/4;
ref2 = ref2(1:floor(numel(ref2)/8)*8);
end