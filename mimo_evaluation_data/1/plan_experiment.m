fc= 915e6;
lb = 3e8/fc;


dr= 0.25 ;

R = 25;



x_max = 5000;
y_max = 5000;



dt = lb*R/(dr*2)

stp  = 10;
[x,y] = meshgrid(0:stp:x_max,0:stp:y_max);



rx1 = [x_max/2-dr/2;0];
rx2 = [x_max/2+dr/2;0];

d1 = vecnorm(rx1-[x(:)';y(:)']);
d2 = vecnorm(rx2-[x(:)';y(:)']);

dph1 = mod(2*pi*(d1-d2)/lb,2*pi);

imagesc([0,x_max],[0,y_max],reshape(dph1,size(x,1),size(x,2)))
axis xy;