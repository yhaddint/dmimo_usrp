fldr='';



[s1_1,s2_1,s3_1,s4_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf_test3']);
[s1_2,s2_2,s3_2,s4_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf([fldr,'data_bf_test4_ekf']);
[s1_3,s2_3,s3_3,s4_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf([fldr,'data_bf_test5_rand']);

% [s1_1,s2_1,s3_1,s4_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf_dbg2_n1_4_a3']);
% [s1_2,s2_2,s3_2,s4_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf([fldr,'data_bf_dbg2_n1_4']);
% [s1_3,s2_3,s3_3,s4_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf([fldr,'data_bf_dbg2_n1_4_a2']);



% 
% 
% 
snr1=mean(20*log10(mean([s1_1;s2_1;s3_1;s4_1;])./sn_1));
snr2=mean(20*log10(mean([s1_2;s2_2;s3_2;s4_2;])./sn_2));
snr3=mean(20*log10(mean([s1_3;s2_3;s3_3;s4_3;])./sn_3));


%%
set(0, 'DefaultLineLineWidth', 2);
%figure;
hold on
cdfplot(bf_gain_1(vld1).^2/4 )
cdfplot(bf_gain_2(vld2).^2/4 )
cdfplot(bf_gain_3(vld3).^2/4 )
%xlim([0,2.5])

legend('EKF II','EKF','Oneshot','Location','best')
% legend( cellstr(num2str(round(snr_vec_est)', 'SNR=%-ddB')),'Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
%xlim([0,2.5])
title('')
box on

mean(bf_gain_1(vld1).^2/4)
mean(bf_gain_2(vld2).^2/4)
mean(bf_gain_3(vld3).^2/4)
%%

% figure;
% hold on
% 
% bf_gain_1 =  (bf_1)./mean([s1_1; s4_1;]);
% cdfplot(bf_gain_1(bf_gain_1<3).^2/2 )
% mean(bf_gain_1(bf_gain_1<3).^2/2)
% 
% bf_gain_2 =  (bf_2)./mean([s1_2; s4_2;]);
% cdfplot(bf_gain_2(bf_gain_2<3).^2/2 )
% mean(bf_gain_2(bf_gain_2<3).^2/2)
% 
% bf_gain_3 =  (bf_3)./mean([s1_3; s4_3;]);
% cdfplot(bf_gain_3(bf_gain_3<3).^2/2 )
% mean(bf_gain_3(bf_gain_3<3).^2/2)

