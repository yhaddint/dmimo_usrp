fldr='';


file_list =  dir(fullfile(['data/',fldr],'*_iir_*'));


bf_gain_all=[];
snr_all=[];
for fi=1:numel(file_list)
    fname=file_list(fi).name(1:end-4);
    [s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,fname]);
    [~,snr1_all,snr1]= calc_snr([s1_1;s2_1],sn_1);

    snr_all=[snr_all snr1_all(vld1)];
    bf_gain_all=[bf_gain_all bf_gain_1(vld1)];
end

%%
snr_stp = 5;
snr_vec_est = 5:snr_stp:35;

n_el = numel(snr_vec_est); 
gn_mn_vec = zeros(n_el,1);
gn_std_vec = zeros(n_el,1);

for snr_i = 1:numel(snr_vec_est)
    ind = snr_all>snr_vec_est(snr_i)-snr_stp/2 & snr_all<snr_vec_est(snr_i)+snr_stp/2;
    gn_mn_vec(snr_i)=mean(bf_gain_all(ind).^2/2);
    gn_std_vec(snr_i)=std(bf_gain_all(ind).^2/2);
end

figure;
errorbar(snr_vec_est,gn_mn_vec,gn_std_vec)


       