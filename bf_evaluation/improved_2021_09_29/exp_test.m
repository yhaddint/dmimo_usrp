fldr='';
[s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf_iir_0p65']);



snr1=mean(20*log10(mean([s1_1;s2_1])./sn_1));




%%
set(0, 'DefaultLineLineWidth', 2);
figure;
hold on
cdfplot(bf_gain_1.^2/2 )


legend( cellstr(num2str(round(snr_vec_est)', 'SNR=%-ddB')),'Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
xlim([0,2.5])
title('')
box on
%%




% 
% mx= min([sum(vld1),sum(vld2),sum(vld3),sum(vld4)]);
% figure;
% hold on
% snr1p=snr1(vld1);
% snr2p=snr2(vld2);
% snr3p=snr3(vld3);
% snr4p=snr4(vld4);
% plot(snr1p(1:mx))
% plot(snr2p(1:mx))
% plot(snr3p(1:mx))
% plot(snr4p(1:mx))
% ylabel('SINR (dB)')
% xlabel('Time Index')
% legend('1','2','3','4','Location','best')
% box on

% figure;
% hold on
% plot(s1_1)
% plot(s2_1)
% plot(bf_1)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_2)
% plot(s2_2)
% plot(bf_2)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_3)
% plot(s2_3)
% plot(bf_3)
% ylabel('Magnitude')
% xlabel('Time Index')

% figure;
% hold on
% plot(sn_4)
% plot(s1_4)
% plot(s2_4)
% plot(bf_4)
% 
% ylabel('Magnitude')
% xlabel('Time Index')