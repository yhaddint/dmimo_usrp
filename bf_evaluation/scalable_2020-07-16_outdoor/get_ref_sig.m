function [ref]  = get_ref_sig()
node1=read_complex_binary('ref_data.dat');
ref  = node1(16054:end)/4;
ref = ref(1:floor(numel(ref)/8)*8);

end