

[f1,x1,ber1,bfsnr1,presnr1] = analyze_bf(13);
[f2_circle,x2_circle,ber2_circle,bfsnr2_circle,presnr2_circle] = analyze_bf(15);
[f3_circle,x3_circle,ber3_circle,bfsnr3_circle,presnr3_circle] = analyze_bf(3);


figure;
hold on
plot(x1,f1)
plot(x2_circle,f2_circle)
plot(x3_circle,f3_circle)
xlim([0,4])
xlabel('BF Gain')
ylabel('CDF')
title('BF Gain')
box on
legend('id','c1','c2')
title('circle')

%

[f2_dist,x2_dist,ber2_dist,bfsnr2_dist,presnr2_dist] = analyze_bf(10);
[f3_dist,x3_dist,ber3_dist,bfsnr3_dist,presnr3_dist] = analyze_bf(2);


figure;
hold on
plot(x1,f1)
plot(x2_dist,f2_dist)
plot(x3_dist,f3_dist)
xlim([0,4])
xlabel('BF Gain')
ylabel('CDF')
title('BF Gain')
box on
legend('id','d1','d2')
title('Line')

%%

[f2_par,x2_par,ber2_par,bfsnr2_par,presnr2_par] = analyze_bf(3);
[f3_par,x3_par,ber3_par,bfsnr3_par,presnr3_par] = analyze_bf(4);


figure;
hold on
plot(x1,f1)
plot(x2_par,f2_par)
plot(x3_par,f3_par)
xlim([0,4])
xlabel('BF Gain')
ylabel('CDF')
title('BF Gain')
box on
legend('id','d1','d2')
title('Parallel')

%%
%[f2_dist,x2_dist] = analyze_bf(10);
[f4_dist,x4_dist,ber4_dist,bfsnr4_dist,presnr4_dist] = analyze_bf(12);


figure;
hold on
plot(x1,f1)
plot(x2_dist,f2_dist)
plot(x4_dist,f4_dist)
xlim([0,4])
xlabel('BF Gain')
ylabel('CDF')
title('BF Gain')
box on
legend('id','for','back')
title('Line Two Ways')

%%

file_loc = pwd;
timestamp = datetime;


save('outdoor_fake_rx','f1','x1','ber1','bfsnr1','presnr1',...
                         'f2_circle','x2_circle','ber2_circle','bfsnr2_circle','presnr2_circle',...
                         'f3_circle','x3_circle','ber3_circle','bfsnr3_circle','presnr3_circle',...
                         'f2_dist','x2_dist','ber2_dist','bfsnr2_dist','presnr2_dist',...
                         'f3_dist','x3_dist','ber3_dist','bfsnr3_dist','presnr3_dist',...
                         'f2_par','x2_par','ber2_par','bfsnr2_par','presnr2_par',...
                         'f3_par','x3_par','ber3_par','bfsnr3_par','presnr3_par',...
                         'f4_dist','x4_dist','ber4_dist','bfsnr4_dist','presnr4_dist',...
                        'file_loc','timestamp')