function pkt_corr = eval_data(pkts)

sps = 2;

ref = get_ref_sig();

ds_ref = my_downsample(ref,0,2);
pkts= pkts(1:floor(end/8)*8,:);


seg1 = 1:4000;
seg2 = 4010:size(pkts,1);
pkt_len =size(pkts,1);


pkt_corr = nan(size(pkts,2),1);

for pkt_i =1:size(pkts,2)
    pkt = pkts(:,pkt_i);
%     plotc(pkt);
%     hold on
%     plot(ref/5)
%     hold off



    x_phase =(1:numel(seg1))';
    [~,t]=max(abs((fft(pkt))));
    pkt = pkt.*exp(-1i*2*pi*t*linspace(0,1,pkt_len)');
    pkt_phs = unwrap(angle(pkt(seg1)));
    xx = [ones(numel(seg1),1) x_phase];
    l = inv(xx'*xx)*xx'*pkt_phs;
    phs = l(1) + x_phase*l(2);
%     hold off;
%     plot(x_phase,pkt_phs)
%     hold on
%     plot(phs)
%     plotc(pkt.*exp(-1j*phs_all))
    
    phs_all= l(1) + (1:numel(pkt))'*l(2);
    pkt = pkt.*exp(-1j*phs_all);


    ds_pkt = my_downsample(pkt,0,sps);
    
    ds_loc = 2018:size(ds_pkt,1)-10;
    rec = ds_pkt(ds_loc);
    
    
%     figure;
%     hold on;
%     plot(real(ds_pkt(5000+(1:1000))))
%     plot(real(ds_ref(5000+(1:1000))))

    

    
    
%     rec = correct_phase(ds_pkt(pkt1_ds_loc));
    
%     figure;
%     plot(rec)

    ref_bits = ds_ref>0;
    pkt_bits = rec>0;

    ref_algined = ref_bits(ds_loc);
    corr = calc_corr(pkt_bits,ref_algined);


    pkt_corr(pkt_i) = corr;
end

nanmean(pkt_corr)

% figure;
% hold off
% plot(pkt_corr)

%%
%save(fname,'pkt1_err','pkt2_err','pkt3_err','sig1_pwr','sig2_pwr','sig3_pwr','noise_var')
%%
%%
end

function pwr = calc_pwr(sig)
pwr = mean(abs(sig).^2);
end

function corr = calc_corr(dec,ref)
%     [cr,idx] = xcorr(2*dec-1,2*ref-1,10);
%     [~,t]=max(abs(cr));
%     lg= idx(t);
    mx = -1;
    for lg = 0 :10
        corrA = mean(dec(lg+1:end )==ref(1:end-lg));
        corrA = max([corrA 1-corrA]);
        if corrA > mx
            mx =corrA;
            %lg
        end
    end
    for lg = -10:0
        corrA = mean(dec(1:end + lg )==ref(1-lg:end));
        corrA = max([corrA 1-corrA]);
        if corrA > mx
            mx =corrA;
            %lg
        end
    end
    corr = mx;
end


function pkts = get_packets(x,data_len)
data_len = 30000+512;
zrs_loc = find(x==0);
lst_zrs = diff(zrs_loc)>10;
lst_zrs = zrs_loc(lst_zrs(2:end));

xt = x(lst_zrs(1):end);
n_pkts = floor(numel(xt)/data_len);
xr = reshape(xt(1:n_pkts*data_len),[data_len,n_pkts]);
global ref_len
pkts = xr(1:ref_len,:);
end




function rec = correct_phase(pkt)
ph = angle(pkt);

x_phase =(1:numel(pkt))';
sig_phase = ph(x_phase);
%plot(sig_phase)
neg = sig_phase < 0;
sig_phase(neg) = sig_phase(neg) + pi;
sig_phase = unwrap(sig_phase*2)/2;



x = [ones(numel(sig_phase),1) x_phase];
l = inv(x'*x)*x'*sig_phase;
phs = l(1) + x_phase*l(2);
% hold off;
% plot(x_phase,sig_phase)
% hold on
% plot(phs)
% plotc(pkt.*exp(-1j*phs))
rec =real( pkt.*exp(-1j*phs));
end