function create_video(x,fname,shft)
samp_x = (1:size(x,1))/1000;
strt_indx = 1+shft;
end_indx = 80+shft;

for indx = 1:end_indx-strt_indx
    figure(1)  
    samp = x(:,strt_indx+indx);
    plot(samp_x,abs(x(:,indx)))
    ylim([0,0.35])
    ylabel('Magnitude')
    xlabel('time (ms)')
    F(indx) = getframe(gcf) ;
     drawnow
end


writerObj = VideoWriter([fname '.avi']);
writerObj.FrameRate = 10;
 
open(writerObj);
% write the frames to the video
for i=1:length(F)
    % convert the image to a frame
    frame = F(i) ;    
    writeVideo(writerObj, frame);
end
% close the writer object
close(writerObj);