fldr='data/';

snr1=0;snr2=0;snr3=0;snr4=0;snr5=0;snr6=0;srn7=0;snr8=0;
gn1=0;gn2=0;gn3=0;gn4=0;gn5=0;gn6=0;ng7=0;gn8=0;

%noise_mag = mean(abs(read_complex_binary('data_bf_noise2.dat')));


fname='data_bf_ground';
[s1_f1,s2_f1,~,~,bf_f1,~,noise_mag1]=analyze_bf_fun([fldr,fname]);

fname='data_bf_uav_hovering';
[s1_f2,s2_f2,~,~,bf_f2,~,noise_mag2]=analyze_bf_fun([fldr,fname]);

fname='data_bf_uav_back_forth_bad';
[s1_f3,s2_f3,~,~,bf_f3,~,noise_mag3]=analyze_bf_fun([fldr,fname]);
s1_f3 =s1_f3(1:end);
s2_f3 =s2_f3(1:end);
bf_f3 =bf_f3(1:end);

gn1= (bf_f1)./mean([s1_f1; s2_f1; ]);
gn2= (bf_f2)./mean([s1_f2; s2_f2; ]);
gn3= (bf_f3)./mean([s1_f3; s2_f3; ]);
%gn4= (bf_f4)./mean([s1_f4; s2_f4; ]);


snr1= 20*log10(mean([s1_f1; s2_f1; ])/noise_mag1);
snr1([317,431])=[];
snr2= 20*log10(mean([s1_f2; s2_f2; ])/noise_mag2);
snr2([132])=[];
snr3= 20*log10(mean([s1_f3; s2_f3; ])/noise_mag3);
%snr4= 20*log10(mean([s1_f4; s2_f4; ])/noise_mag);

%%
figure;
hold on
gn1c = gn1;
% gn1c(gn1c>2) = 2;
gn2c = gn2;
% gn2c(gn2c>2) = 2;
gn3c = gn3;
% gn3c(gn3c>2) = 2;

cdfplot(gn1c)
cdfplot(gn2c)
cdfplot(gn3c)
%cdfplot(gn4)
xlim([0.5,2.1])
xlabel('BF Gain')
ylabel('P(Gain<Abscissa)')
title('')
box on
legend('Ground','Hovering','Back and Forth','Location','Best')


%%
figure;
hold on
plot(gn1)
plot(gn2)
plot(gn3)
%cdfplot(gn4)
xlabel('Time')
ylabel('Combining Gain')
title('BF Gain')

box on
legend('Ground','Hovering','Back and Forth','Location','Best')
%%

snr_vec = [mean(snr1),mean(snr2),mean(snr3)];% ,mean(snr4)
       
gn_vec = [mean(gn1),mean(gn2),mean(gn3)];% ,mean(gn4)
figure(15);
hold on
plot(snr_vec,gn_vec)
xlabel('SNR')
ylabel('BF Gain')
title('BF Gain')
box on

%%
figure;
hold on;
cdfplot(gn1c)
cdfplot(gn2c)
xlim([0.0,2.5])
xlabel('BF Gain')
ylabel('P(Gain<Abscissa)')
title('')
box on
legend('Ground','Hovering','Location','Best')

%%

file_loc = pwd;
timestamp = datetime;


% save('dron_outdoor_2020-09-18','bf_f1','s1_f1','s2_f1','noise_mag1',...
%                         'bf_f2','s1_f2','s2_f2','noise_mag2',...
%                         'bf_f3','s1_f3','s2_f3','noise_mag3',...
%                         'file_loc','timestamp')