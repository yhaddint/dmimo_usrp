fname = 'data_2020-07-26/data_bf_f905_1msps_4n_mag0p55';
xf = read_complex_binary([fname '.dat']);
xn = read_complex_binary('data_bf_noise.dat');

data_len = 30000;
total_len = data_len + 512;


zrs_loc = find(xf==0);
zr_diff = diff(zrs_loc);
%lst_zrs = diff(zrs_loc)>905;
lst_zrs = find(zr_diff(2:end)>1000&zr_diff(1:end-1)==1)+1;
lst_zrs = zrs_loc(lst_zrs);
start_offset = lst_zrs(10);

n_captures =  floor((length(xf)-start_offset)/total_len);

% i=0;
% plot(x(start_offset + 1+i*data_len: start_offset +(i+1)*data_len))


x = xf(start_offset+1:n_captures*total_len+start_offset);



n_captures =  floor((length(x))/total_len);

x = reshape(x,total_len,n_captures);

seg1 = 10:4000;
seg2 = 4010:8000;
seg3 = 8010:12000;
seg4 = 12010:16000;
seg5 = 16010:20000;

seg6 = 20003:29905;

seg56 = 16010:29905;

pkt_len = size(x,1);

% % Frequency correction
% for pkti=100%1:shape(x,2)
%     pkt = x(:,pkti);
%     x_phase =(1:numel(seg1))';
%     [~,t]=max(abs((fft(x(:,1)))));
%     pkt = pkt.*exp(-1i*2*pi*t*linspace(0,1,pkt_len)');
%     pkt_phs = unwrap(angle(pkt(seg1)));
%     xx = [ones(numel(seg1),1) x_phase];
%     l = inv(xx'*xx)*xx'*pkt_phs;
% %     phs = l(1) + x_phase*l(2);
% %     hold off;
% %     plot(x_phase,pkt_phs)
% %     hold on
% %     plot(phs)
% %     plotc(pkt.*exp(-1j*phs))
%     
%     phs_all= l(1) + (1:numel(pkt))'*l(2);
%     pkt = pkt.*exp(-1j*phs_all);
% %     plotc(pkt)
%     
%     x(:,pkti)=pkt;
% 
% end


ber = eval_data(x(seg56,:));
disp(['Average BER = ',num2str(nanmean(ber))]);

% I did not implement BER

noise_mag = mean(abs(xn));
s1 = abs(mean(x(seg1,:)));
s2 = abs(mean(x(seg2,:)));
s3 = abs(mean(x(seg3,:)));
s4 = abs(mean(x(seg4,:)));
bf = abs(mean(x(seg5,:)));

% file_loc = pwd;
% timestamp = datetime;
% save(fname,'s1','s2','s3','s4','bf','ber','noise_mag','file_loc','timestamp')

figure;
hold on
plot(s1)
plot(s2)
plot(s3)
plot(s4)
plot(bf)
legend('s1','s2','BF')
ylabel('Received Signal Magnitude')
xlabel('Packet Time Index')
box on


figure;
hold on
plot(ber)
ylabel('BER')
xlabel('Packet Time Index')
box on
% 
% figure;
% hold on
% histogram( (bf-s1-s2-s3-s4)./sum([s1; s2; s3; s4]),linspace(-0.5,0,100),'Normalization','probability')
% xlabel('BF - (s1+s2)  ')
% ylabel('Histogram')
% box on

% figure;
% hold on
% cdfplot( (bf)./sum([s1; s2; s3; s4]))
% xlabel('BF Gain Relative to Ideal')
% ylabel('Histogram')
% box on
%%
figure;
hold on
cdfplot( (bf)./mean([s1; s2; s3; s4]))
xlabel('BF Gain')
ylabel('Histogram')
title('BF Gain')
box on

