fldr='data_2020-07-26/';

snr1=0;snr2=0;snr3=0;snr4=0;snr5=0;snr6=0;srn7=0;snr8=0;
gn1=0;gn2=0;gn3=0;gn4=0;gn5=0;gn6=0;ng7=0;gn8=0;

noise_mag = mean(abs(read_complex_binary('data_bf_noise2.dat')));


fname='data_bf_f905_1msps_2n_mag0p65';
[s1_f1,s2_f1,~,~,bf_f1]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p65_s1';
[s1_f2,s2_f2,~,~,bf_f2]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p65_s2';
[s1_f3,s2_f3,~,~,bf_f3]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p65_s3';
[s1_f4,s2_f4,~,~,bf_f4]=analyze_bf_fun([fldr,fname]);



gn1= (bf_f1)./mean([s1_f1; s2_f1; ]);
gn1([317,431])=[];
gn2= (bf_f2)./mean([s1_f2; s2_f2; ]);
gn2([132])=[];
gn3= (bf_f3)./mean([s1_f3; s2_f3; ]);
gn4= (bf_f4)./mean([s1_f4; s2_f4; ]);


snr1= 20*log10(mean([s1_f1; s2_f1; ])/noise_mag);
snr1([317,431])=[];
snr2= 20*log10(mean([s1_f2; s2_f2; ])/noise_mag);
snr2([132])=[];
snr3= 20*log10(mean([s1_f3; s2_f3; ])/noise_mag);
snr4= 20*log10(mean([s1_f4; s2_f4; ])/noise_mag);

%%
figure;
hold on
cdfplot(gn1)
cdfplot(gn2)
cdfplot(gn3)
cdfplot(gn4)
xlabel('BF Gain')
ylabel('Histogram')
title('BF Gain')
box on
legend('1','2','3','4','5','6','7')
%%

snr_vec = [mean(snr1),mean(snr2),mean(snr3),mean(snr4)];
       
gn_vec = [mean(gn1),mean(gn2),mean(gn3),mean(gn4)];
figure(15);
hold on
plot(snr_vec,gn_vec)
xlabel('SNR')
ylabel('BF Gain')
title('BF Gain')
box on



%%

file_loc = pwd;
timestamp = datetime;


save('rot_2n_2020-07-26','bf_f1','s1_f1','s2_f1',...
                        'bf_f2','s1_f2','s2_f2',...
                        'bf_f3','s1_f3','s2_f3',...
                        'bf_f4','s1_f4','s2_f4',...
                        'noise_mag','file_loc','timestamp')