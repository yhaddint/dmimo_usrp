fldr='data_2020-07-26/';

snr1=0;snr2=0;snr3=0;snr4=0;snr5=0;snr6=0;srn7=0;snr8=0;
gn1=0;gn2=0;gn3=0;gn4=0;gn5=0;gn6=0;ng7=0;gn8=0;

noise_mag = mean(abs(read_complex_binary('data_bf_noise2.dat')));

fname='data_bf_f905_1msps_2n_mag0p3';
[s1_f1,s2_f1,~,~,bf_f1]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p35';
[s1_f2,s2_f2,~,~,bf_f2]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p4';
[s1_f3,s2_f3,~,~,bf_f3]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p45';
[s1_f4,s2_f4,~,~,bf_f4]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p5';
[s1_f5,s2_f5,~,~,bf_f5]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p55';
[s1_f6,s2_f6,~,~,bf_f6]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p6';
[s1_f7,s2_f7,~,~,bf_f7]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p65';
[s1_f8,s2_f8,~,~,bf_f8]=analyze_bf_fun([fldr,fname]);



gn1= (bf_f1)./mean([s1_f1; s2_f1; ]);
gn2= (bf_f2)./mean([s1_f2; s2_f2; ]);
gn3= (bf_f3)./mean([s1_f3; s2_f3; ]);
gn4= (bf_f4)./mean([s1_f4; s2_f4; ]);
gn5= (bf_f5)./mean([s1_f5; s2_f5; ]);
gn6= (bf_f6)./mean([s1_f6; s2_f6; ]);
gn7= (bf_f7)./mean([s1_f7; s2_f7; ]);
gn8= (bf_f8)./mean([s1_f8; s2_f8; ]);
gn8([317,431])=[];

snr1= 20*log10(mean([s1_f1; s2_f1; ])/noise_mag);
snr2= 20*log10(mean([s1_f2; s2_f2; ])/noise_mag);
snr3= 20*log10(mean([s1_f3; s2_f3; ])/noise_mag);
snr4= 20*log10(mean([s1_f4; s2_f4; ])/noise_mag);
snr5= 20*log10(mean([s1_f5; s2_f5; ])/noise_mag);
snr6= 20*log10(mean([s1_f6; s2_f6; ])/noise_mag);
snr7= 20*log10(mean([s1_f7; s2_f7; ])/noise_mag);
snr8= 20*log10(mean([s1_f8; s2_f8; ])/noise_mag);
snr8([317,431])=[];
%%
figure;
hold on
cdfplot(gn1)
cdfplot(gn2)
cdfplot(gn3)
cdfplot(gn4)
cdfplot(gn5)
cdfplot(gn6)
cdfplot(gn7)
cdfplot(gn8)
xlabel('BF Gain')
ylabel('Histogram')
title('BF Gain')
box on
legend('1','2','3','4','5','6','7')
%%

snr_vec = [mean(snr1),mean(snr2),mean(snr3),mean(snr4),...
           mean(snr5),mean(snr6),mean(snr7),mean(snr8)];
       
gn_vec = [mean(gn1),mean(gn2),mean(gn3),mean(gn4),...
           mean(gn5),mean(gn6),mean(gn7),mean(gn8)];
figure(15);
hold on
plot(snr_vec,gn_vec)
xlabel('SNR')
ylabel('BF Gain')
title('BF Gain')
box on



%%

% file_loc = pwd;
% timestamp = datetime;
% 
% 
% save('snr_2n_2020-07-26','bf_f1','s1_f1','s2_f1',...
%                         'bf_f2','s1_f2','s2_f2',...
%                         'bf_f3','s1_f3','s2_f3',...
%                         'bf_f4','s1_f4','s2_f4',...
%                         'bf_f5','s1_f5','s2_f5',...
%                         'bf_f6','s1_f6','s2_f6',...
%                         'bf_f7','s1_f7','s2_f7',...
%                         'bf_f8','s1_f8','s2_f8',...
%                         'noise_mag','file_loc','timestamp')