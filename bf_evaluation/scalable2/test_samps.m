fldr='data_2020-07-21/';

% fname='data_bf_f915_1msps2';
% [s1,s2,bf]=analyze_bf_fun([fldr,fname]);

% 
% fname='data_bf_f915_0p5msps';
% [s1_f1,s2_f1,bf_f1]=analyze_bf_fun([fldr,fname]);

% fname='data_bf_f915_0p75msps';
% [s1_f2,s2_f2,bf_f2]=analyze_bf_fun([fldr,fname]);
% 
fname='data_bf_f915_1p25msps';
[s1_f3,s2_f3,bf_f3]=analyze_bf_fun([fldr,fname]);
%%
figure;
hold on
cdfplot( (bf_f1)./mean([s1_f1; s2_f1; ]))
cdfplot( (bf)./mean([s1; s2;]))
%cdfplot( (bf_f2)./mean([s1_f2; s2_f2; ]))
cdfplot( (bf_f3)./mean([s1_f3; s2_f3; ]))
xlabel('BF Gain')
ylabel('Histogram')
title('BF Gain')
box on
legend('0.5Msps','1Msps','1.25Msps')