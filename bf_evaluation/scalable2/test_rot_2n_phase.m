fldr='data_2020-07-26/';

snr1=0;snr2=0;snr3=0;snr4=0;snr5=0;snr6=0;srn7=0;snr8=0;
gn1=0;gn2=0;gn3=0;gn4=0;gn5=0;gn6=0;ng7=0;gn8=0;

noise_mag = mean(abs(read_complex_binary('data_bf_noise2.dat')));


fname='data_bf_f905_1msps_2n_mag0p65';
[s1_f1,s2_f1,~,~,bf_f1]=analyze_bf_phase_fun([fldr,fname]);


fname='data_bf_f905_1msps_2n_mag0p65_s1';
[s1_f2,s2_f2,~,~,bf_f2]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p65_s2';
[s1_f3,s2_f3,~,~,bf_f3]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_2n_mag0p65_s3';
[s1_f4,s2_f4,~,~,bf_f4]=analyze_bf_phase_fun([fldr,fname]);


phs1= angle([mean(s1_f1); mean(s2_f1)]);
phs1(:,431)=[];
phs1(:,317)=[];
phs2= angle([mean(s1_f2); mean(s2_f2)]);
phs3= angle([mean(s1_f3); mean(s2_f3)]);
phs4= angle([mean(s1_f4); mean(s2_f4)]);
%%
rng = linspace(-pi,pi,100);
figure;
hold on;
histogram(phs1(1,:)-phs1(2,:),rng)
histogram(phs2(1,:)-phs2(2,:),rng)
histogram(phs3(1,:)-phs3(2,:),rng)
histogram(phs4(1,:)-phs4(2,:),rng)
legend('Slow','Fast')

%%

phs_mn_vec = [mean(phs1(1,:)-phs1(2,:)),mean(phs2(1,:)-phs2(2,:)), ...
            mean(phs3(1,:)-phs3(2,:)),mean(phs4(1,:)-phs4(2,:))];

phs_std_vec = [std(phs1(1,:)-phs1(2,:)),std(phs2(1,:)-phs2(2,:)), ...
            std(phs3(1,:)-phs3(2,:)),std(phs4(1,:)-phs4(2,:))];

%%
figure;
%errorbar(phs_mn_vec,phs_std_vec)
boxplot([phs1(1,:)-phs1(2,:), phs2(1,:)-phs2(2,:),phs3(1,:)-phs3(2,:),phs4(1,:)-phs4(2,:)],...
    [0*ones(1,size(phs1,2)),ones(1,size(phs2,2)),2*ones(1,size(phs3,2)),3*ones(1,size(phs4,2))])

xlabel('Speed')
ylabel('Phase Difference (radian)')
%xlim([-0.25,3.25])

%%
figure;
hold on;
histogram(phs1(1,:),rng)
histogram(phs1(2,:),rng)
legend('N1','N2')

figure;
hold on;
histogram(phs2(1,:),rng)
histogram(phs2(2,:),rng)
legend('N1','N2')

figure;
hold on;
histogram(phs3(1,:),rng)
histogram(phs3(2,:),rng)
legend('N1','N2')

figure;
hold on;
histogram(phs4(1,:),rng)
histogram(phs4(2,:),rng)
legend('N1','N2')

%% 
phs_mn1_vec = [mean(phs1(1,:)),mean(phs2(1,:)),mean(phs3(1,:)),mean(phs4(1,:))];

phs_std1_vec = [std(phs1(1,:)),std(phs2(1,:)),std(phs3(1,:)),std(phs4(1,:))];

phs_mn2_vec = [mean(phs1(2,:)),mean(phs2(2,:)),mean(phs3(2,:)),mean(phs4(2,:))];

phs_std2_vec = [std(phs1(2,:)),std(phs2(2,:)),std(phs3(2,:)),std(phs4(2,:))];

figure; hold on;
errorbar(phs_mn1_vec,phs_std1_vec)
errorbar(phs_mn2_vec,phs_std2_vec)
legend('Moving','Stat')
xlabel('Speed')
ylabel('Phase (radian)')
%%

file_loc = pwd;
timestamp = datetime;


save('rot_2n_2020-07-26_phase','phs1','phs2','phs3','phs4',...
                                'file_loc','timestamp')