fldr='data_2020-07-26/';
noise_data = read_complex_binary('data_bf_noise2.dat');
noise_mag = mean(abs(noise_data));

% fname='data_bf_f915_1msps2';
% [s1,s2,bf]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f905_1msps_4n_mag0p3';
[s1_f1,s2_f1,s3_f1,s4_f1,bf_f1]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_4n_mag0p35';
[s1_f2,s2_f2,s3_f2,s4_f2,bf_f2]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_4n_mag0p4';
[s1_f3,s2_f3,s3_f3,s4_f3,bf_f3]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_4n_mag0p45';
[s1_f4,s2_f4,s3_f4,s4_f4,bf_f4]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_4n_mag0p5';
[s1_f5,s2_f5,s3_f5,s4_f5,bf_f5]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_4n_mag0p55';
[s1_f6,s2_f6,s3_f6,s4_f6,bf_f6]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_4n_mag0p6';
[s1_f7,s2_f7,s3_f7,s4_f7,bf_f7]=analyze_bf_phase_fun([fldr,fname]);

fname='data_bf_f905_1msps_4n_mag0p65';
[s1_f8,s2_f8,s3_f8,s4_f8,bf_f8]=analyze_bf_phase_fun([fldr,fname]);



phs1= angle([s1_f1; s2_f1;s3_f1; s4_f1]);
phs2= angle([s1_f2; s2_f2;s3_f2; s4_f2]);
phs3= angle([s1_f3; s2_f3;s3_f3; s4_f3]);
phs4= angle([s1_f4; s2_f4;s3_f4; s4_f4]);
phs5= angle([s1_f5; s2_f5;s3_f5; s4_f5]);
phs6= angle([s1_f6; s2_f6;s3_f6; s4_f6]);
phs7= angle([s1_f7; s2_f7;s3_f7; s4_f7]);
phs8= angle([s1_f8; s2_f8;s3_f8; s4_f8]);

%%
rng = linspace(-pi,pi,100);
figure;
hold on;
histogram(phs1,rng)
% histogram(phs2,rng)
% histogram(phs7,rng)
histogram(phs8,rng)
legend('Low','High')
%%

phs_mn_vec = [mean(phs1(:)),mean(phs2(:)),mean(phs3(:)),mean(phs4(:)),...
           mean(phs5(:)),mean(phs6(:)),mean(phs7(:)),mean(phs8(:))];

phs_std_vec = [std(phs1(:)),std(phs2(:)),std(phs3(:)),std(phs4(:)),...
           std(phs5(:)),std(phs6(:)),std(phs7(:)),std(phs8(:))];

%%
figure;
%errorbar(phs_mn_vec,phs_std_vec)
plot(phs_std_vec)
xlabel('SNR (4dB Step)')
ylabel('Phase Std (radian)')


%%

file_loc = pwd;
timestamp = datetime;


% save('snr_4n_2020-07-26','bf_f1','s1_f1','s2_f1','s3_f1','s4_f1',...
%                         'bf_f2','s1_f2','s2_f2','s3_f2','s4_f2',...
%                         'bf_f3','s1_f3','s2_f3','s3_f3','s4_f3',...
%                         'bf_f4','s1_f4','s2_f4','s3_f4','s4_f4',...
%                         'bf_f5','s1_f5','s2_f5','s3_f5','s4_f5',...
%                         'bf_f6','s1_f6','s2_f6','s3_f6','s4_f6',...
%                         'bf_f7','s1_f7','s2_f7','s3_f7','s4_f7',...
%                         'bf_f8','s1_f8','s2_f8','s3_f8','s4_f8',...
%                         'noise_mag','file_loc','timestamp')