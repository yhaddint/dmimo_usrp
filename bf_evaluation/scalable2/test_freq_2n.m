fldr='data_2020-07-26/';

snr1=0;snr2=0;snr3=0;snr4=0;snr5=0;snr6=0;srn7=0;snr8=0;
gn1=0;gn2=0;gn3=0;gn4=0;gn5=0;gn6=0;ng7=0;gn8=0;

noise_mag_1 = mean(abs(read_complex_binary('data_bf_noise2.dat')));
noise_mag_2 = mean(abs(read_complex_binary([fldr 'data_bf_noise_f2400.dat'])));
noise_mag_3 = mean(abs(read_complex_binary([fldr 'data_bf_noise_f5730.dat'])));



fname='data_bf_f905_1msps_2n_mag0p65';
[s1_f1,s2_f1,~,~,bf_f1]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f2400_1msps_2n_mag0p65';
[s1_f2,s2_f2,~,~,bf_f2]=analyze_bf_fun([fldr,fname]);

fname='data_bf_f5730_1msps_2n_mag0p95';
[s1_f3,s2_f3,~,~,bf_f3]=analyze_bf_fun([fldr,fname]);


vld1 = s1_f1 > noise_mag_1 & s2_f1 > noise_mag_1;
vld2 = s1_f2 > noise_mag_2 & s2_f2 > noise_mag_2;
vld3 = s1_f3 > noise_mag_3 & s2_f3 > noise_mag_3;


gn1= (bf_f1)./mean([s1_f1; s2_f1; ]);
gn2= (bf_f2)./mean([s1_f2; s2_f2; ]);
gn3= (bf_f3)./mean([s1_f3; s2_f3; ]);

vld1([317,431])=0;
gn1=gn1(vld1);
gn2 = gn2(vld2);
gn3 = gn3(vld3);

snr1= 20*log10(mean([s1_f1; s2_f1; ])/noise_mag_1);
snr1([317,431])=[];
snr2= 20*log10(mean([s1_f2; s2_f2; ])/noise_mag_2);
snr2([132])=[];
snr3= 20*log10(mean([s1_f3; s2_f3; ])/noise_mag_3);

%%
figure;
hold on
cdfplot(gn1)
cdfplot(gn2)
cdfplot(gn3)
xlabel('BF Gain')
ylabel('Histogram')
title('BF Gain')
box on
legend('1','2','3','4','5','6','7')
%%

snr_vec = [mean(snr1),mean(snr2),mean(snr3),mean(snr4)]
       
gn_vec = [mean(gn1),mean(gn2),mean(gn3),mean(gn4)];
figure(15);
hold on
plot(snr_vec,gn_vec)
xlabel('SNR')
ylabel('BF Gain')
title('BF Gain')
box on



%%

file_loc = pwd;
timestamp = datetime;


% save('freq_2n_2020-07-26','bf_f1','s1_f1','s2_f1',...
%                         'bf_f2','s1_f2','s2_f2',...
%                         'bf_f3','s1_f3','s2_f3',...
%                         'noise_mag_1','noise_mag_2','noise_mag_3',...
%                         'file_loc','timestamp')