[s1_1,s2_1,sn_1,bf_1,bf_gain_ground]=analyze_bf('data_bf_ground_on');
[s1_2,s2_2,sn_2,bf_2,bf_gain_uav]=analyze_bf('data_bf_drone_hover2');
% [s1_3,s2_3,sn_3,bf_3,bf_gain_uav]=analyze_bf('data_lab_test7_on');

%%
figure;
hold on
cdfplot(bf_gain_ground )
cdfplot(bf_gain_uav )
% cdfplot(bf_gain_uav )
legend('Ground','Hovering','Location','best')
xlabel('BF Gain')
ylabel('P(BF Gain < abscissa)')
title('')
box on
%%
% 
% figure;
% hold on
% plot(s1_1)
% plot(s2_1)
% plot(bf_1)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_2)
% plot(s2_2)
% plot(bf_2)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_3)
% plot(s2_3)
% plot(bf_3)
% ylabel('Magnitude')
% xlabel('Time Index')