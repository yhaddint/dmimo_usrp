xf = read_float_binary('bf_data_2019-12-12.dat');

total_data_len = 50000;
downsample = 100;



data_len = 500; total_data_len/downsample;
start_offset = data_len*16;

n_captures =  floor((length(xf)-start_offset)/data_len);

% i=0;
% plot(x(start_offset + 1+i*data_len: start_offset +(i+1)*data_len))


x = xf(start_offset+1:n_captures*data_len);

n_captures =  floor((length(x))/data_len);

x = reshape(x,data_len,n_captures);

seg1 = 15:94;
seg2 = 108:203;
seg3 = 205:304;
seg4 = 316:500;

mn1 = mean(x(seg1,:));
mn2 = mean(x(seg2,:));

mn3 = mean(x(seg3,:));
std3 = std(x(seg3,:));
min3 = min(x(seg3,:));
max3 = max(x(seg3,:));

mn4 = mean(x(seg4,:));
std4 = std(x(seg4,:));
min4 = min(x(seg4,:));
max4 = max(x(seg4,:));
close all;
figure;
hold on
plot(mn1)
plot(mn2)
%plot(mn3)
% plot(min3,'b:')
% plot(max3,'b:')
plot(mn4)
legend('s1','s2','BF')
ylabel('Received Signal Magnitude')
xlabel('Packet Time Index')
box on

figure;
hold on
histogram(mn4-mn1-mn2,linspace(-.005,0.005,50),'Normalization','probability')
xlabel('BF - (s1+s2)  ')
ylabel('Histogram')
box on


