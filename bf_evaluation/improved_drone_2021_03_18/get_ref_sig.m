function [ref]  = get_ref_sig()
node1=read_complex_binary('ref_data.dat');
strt = 16054-2+ 3000;
ref  = node1(strt:strt+6000)/4;
ref = ref(1:floor(numel(ref)/8)*8);

end