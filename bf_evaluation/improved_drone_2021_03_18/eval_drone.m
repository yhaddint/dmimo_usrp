[s1_1,s2_1,sn_1,bf_1,bf_gain_chair1]=analyze_bf('data_drone_chair_off_935_3');
%[s1_2,s2_2,sn_2,bf_2,bf_gain_chair2]=analyze_bf('data_drone_flying_935_2');
[s1_3,s2_3,sn_3,bf_3,bf_gain_uav]=analyze_bf('data_drone_flying_935');

%%
figure;
hold on
cdfplot(bf_gain_chair1 )
%cdfplot(bf_gain_chair2 )
cdfplot(bf_gain_uav )
legend('Chair','Flying','Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
xlim([0,2.5])
title('')
box on
%%
% figure;
% hold on
% plot(s1_1)
% plot(s2_1)
% plot(bf_1)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_2)
% plot(s2_2)
% plot(bf_2)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_3)
% plot(s2_3)
% plot(bf_3)
% ylabel('Magnitude')
% xlabel('Time Index')