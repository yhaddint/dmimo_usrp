xf = read_complex_binary('data_bf_2020-06-30.dat');
xn = read_complex_binary('data_bf_noise.dat');

data_len = 30000;
total_len = data_len + 512;



start_offset =10864 + total_len*16;

n_captures =  floor((length(xf)-start_offset)/total_len);

% i=0;
% plot(x(start_offset + 1+i*data_len: start_offset +(i+1)*data_len))


x = xf(start_offset+1:n_captures*total_len+start_offset);



n_captures =  floor((length(x))/total_len);

x = reshape(x,total_len,n_captures);
x(:,77)=[];
seg1 = 10:2490;
seg2 = 2510:4990;
seg3 = 5010:7490;
seg4 = 7510:9990;

seg5 = 10010:29990;

noise_mag = mean(abs(xn));
s1 = mean(abs(x(seg1,:)));
s2 = mean(abs(x(seg2,:)));
s3 = mean(abs(x(seg3,:)));
s4 = mean(abs(x(seg4,:)));
bf = mean(abs(x(seg5,:)));

%%

figure;
hold on
plot(s1)
plot(s2)
plot(s3)
plot(s4)
plot(bf)
legend('s1','s2','BF')
ylabel('Received Signal Magnitude')
xlabel('Packet Time Index')
box on
% 
% figure;
% hold on
% histogram( (bf-s1-s2-s3-s4)./sum([s1; s2; s3; s4]),linspace(-0.5,0,100),'Normalization','probability')
% xlabel('BF - (s1+s2)  ')
% ylabel('Histogram')
% box on

% figure;
% hold on
% cdfplot( (bf)./sum([s1; s2; s3; s4]))
% xlabel('BF Gain Relative to Ideal')
% ylabel('Histogram')
% box on
%%

figure;
hold on
cdfplot( (bf)./mean([s1; s2; s3; s4]))
xlabel('BF Gain')
ylabel('Histogram')
title('CDF of BF Gain')
box on

