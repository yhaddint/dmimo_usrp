[s1_1,s2_1,sn_1,bf_1,bf_gain_chair1]=analyze_bf('data_bf_1');
[s1_2,s2_2,sn_2,bf_2,bf_gain_chair2]=analyze_bf('data_bf_200');
[s1_3,s2_3,sn_3,bf_3,bf_gain_uav]=analyze_bf('data_bf_500');

%%
figure;
hold on
cdfplot(bf_gain_chair1 )
cdfplot(bf_gain_chair2 )
cdfplot(bf_gain_uav )
legend('1','2','3','Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
xlim([0,2.5])
title('')
box on
%%
figure;
hold on
plot(20*log10(s1_1))
plot(20*log10(s1_2))
plot(20*log10(s1_3))
ylabel('Magnitude (dB)')
xlabel('Time Index')
legend('1','2','3','Location','best')
box on
%%

figure;
hold on
plot(20*log10(sn_1))
plot(20*log10(sn_2))
plot(20*log10(sn_3))
ylabel('Interf. + Noise Power (dB)')
xlabel('Time Index')
legend('1','2','3','Location','best')
box on
%%
figure;
hold on
plot(20*log10(s1_1./sn_1))
plot(20*log10(s1_2./sn_2))
plot(20*log10(s1_3./sn_3))
ylabel('SINR (dB)')
xlabel('Time Index')
legend('1','2','3','Location','best')
box on
% figure;
% hold on
% plot(s1_2)
% plot(s2_2)
% plot(bf_2)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_3)
% plot(s2_3)
% plot(bf_3)
% ylabel('Magnitude')
% xlabel('Time Index')