[s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf('data_bf_lab_0p65');
[s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf('data_bf_lab_0p60');
[s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf('data_bf_lab_0p55');
[s1_4,s2_4,sn_4,bf_4,bf_gain_4,vld4]=analyze_bf('data_bf_lab_0p50');
[s1_5,s2_5,sn_5,bf_5,bf_gain_5,vld5]=analyze_bf('data_bf_lab_0p45');
[s1_6,s2_6,sn_6,bf_6,bf_gain_6,vld6]=analyze_bf('data_bf_lab_0p40');
[s1_7,s2_7,sn_7,bf_7,bf_gain_7,vld7]=analyze_bf('data_bf_lab_0p35');
[s1_8,s2_8,sn_8,bf_8,bf_gain_8,vld8]=analyze_bf('data_bf_lab_0p30');
[s1_9,s2_9,sn_9,bf_9,bf_gain_9,vld9]=analyze_bf('data_bf_lab_0p25');


snr1=mean(20*log10(mean([s1_1;s2_1])./sn_1));
snr2=mean(20*log10(mean([s1_2;s2_2])./sn_2));
snr3=mean(20*log10(mean([s1_3;s2_3])./sn_3));
snr4=mean(20*log10(mean([s1_4;s2_4])./sn_4));
snr5=mean(20*log10(mean([s1_5;s2_5])./sn_5));
snr6=mean(20*log10(mean([s1_6;s2_6])./sn_6));
snr7=mean(20*log10(mean([s1_7;s2_7])./sn_7));
snr8=mean(20*log10(mean([s1_8;s2_8])./sn_8));
snr9=mean(20*log10(mean([s1_9;s2_9])./sn_9));

strt= 4;

snr_vec = [mean(snr1),mean(snr2),mean(snr3),mean(snr4),...
           mean(snr5),mean(snr6),mean(snr7),mean(snr8),mean(snr9)];
snr_vec = snr_vec(strt:end);

%%
set(0, 'DefaultLineLineWidth', 2);
figure;
hold on
% cdfplot(bf_gain_1(vld1) )
% cdfplot(bf_gain_2(vld2) )
% cdfplot(bf_gain_3(vld3) )
cdfplot(bf_gain_4(vld4) )
cdfplot(bf_gain_5(vld5) )
cdfplot(bf_gain_6(vld6) )
cdfplot(bf_gain_7(vld7) )
cdfplot(bf_gain_8(vld8) )
cdfplot(bf_gain_9(vld9) )

legend( cellstr(num2str(round(snr_vec)', 'SNR=%-ddB')),'Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
xlim([0,2.5])
title('')
box on
%%



rnd= mean(abs(exp(1j*2*pi*rand(1000,1))+exp(1j*2*pi*rand(1000,1))));


gn_mn_vec = [mean(bf_gain_1),mean(bf_gain_2),mean(bf_gain_3),mean(bf_gain_4),...
           mean(bf_gain_5),mean(bf_gain_6),mean(bf_gain_7),mean(bf_gain_8),mean(bf_gain_9)];
gn_mn_vec = gn_mn_vec(strt:end);
       
gn_std_vec = [std(bf_gain_1),std(bf_gain_2),std(bf_gain_3),std(bf_gain_4),...
           std(bf_gain_5),std(bf_gain_6),std(bf_gain_7),std(bf_gain_8),std(bf_gain_9)];
gn_std_vec = gn_std_vec(strt:end);

figure;
errorbar(snr_vec,gn_mn_vec,gn_std_vec)
hold on
plot(snr_vec,rnd*ones(numel(snr_vec),1))
ylabel('BF Gain')
xlabel('SNR (dB)')
       
% 
% mx= min([sum(vld1),sum(vld2),sum(vld3),sum(vld4)]);
% figure;
% hold on
% snr1p=snr1(vld1);
% snr2p=snr2(vld2);
% snr3p=snr3(vld3);
% snr4p=snr4(vld4);
% plot(snr1p(1:mx))
% plot(snr2p(1:mx))
% plot(snr3p(1:mx))
% plot(snr4p(1:mx))
% ylabel('SINR (dB)')
% xlabel('Time Index')
% legend('1','2','3','4','Location','best')
% box on

% figure;
% hold on
% plot(s1_1)
% plot(s2_1)
% plot(bf_1)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_2)
% plot(s2_2)
% plot(bf_2)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_3)
% plot(s2_3)
% plot(bf_3)
% ylabel('Magnitude')
% xlabel('Time Index')

% figure;
% hold on
% plot(sn_4)
% plot(s1_4)
% plot(s2_4)
% plot(bf_4)
% 
% ylabel('Magnitude')
% xlabel('Time Index')