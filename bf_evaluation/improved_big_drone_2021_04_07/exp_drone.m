[s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf('data_bf_lab_0p40');
[s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf('data_bf_ground_m100');
[s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf('data_bf_drone');
[s1_4,s2_4,sn_4,bf_4,bf_gain_4,vld4]=analyze_bf('data_bf_line');



%%
set(0, 'DefaultLineLineWidth', 2);
figure;
hold on
cdfplot(bf_gain_1(vld1) )
cdfplot(bf_gain_2(vld2) )
cdfplot(bf_gain_3(vld3) )
cdfplot(bf_gain_4(vld4) )

legend('Lab','Ground','Hover','Line','Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
xlim([0,2.5])
title('')
box on
%%

snr1=20*log10(s2_1./sn_1);
snr2=20*log10(s2_2./sn_2);
snr3=20*log10(s2_3./sn_3);
snr4=20*log10(s2_4./sn_4);

mx= min([sum(vld1),sum(vld2),sum(vld3),sum(vld4)]);
figure;
hold on
snr1p=snr1(vld1);
snr2p=snr2(vld2);
snr3p=snr3(vld3);
snr4p=snr4(vld4);
plot(snr1p(1:mx))
plot(snr2p(1:mx))
plot(snr3p(1:mx))
plot(snr4p(1:mx))
ylabel('SINR (dB)')
xlabel('Time Index')
legend('Lab','Ground','Hover','Line','Location','best')
box on

% figure;
% hold on
% plot(s1_1)
% plot(s2_1)
% plot(bf_1)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_2)
% plot(s2_2)
% plot(bf_2)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_3)
% plot(s2_3)
% plot(bf_3)
% ylabel('Magnitude')
% xlabel('Time Index')

% figure;
% hold on
% plot(sn_4)
% plot(s1_4)
% plot(s2_4)
% plot(bf_4)
% 
% ylabel('Magnitude')
% xlabel('Time Index')