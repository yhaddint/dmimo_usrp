[s1_1,s2_1,sn_1,bf_1,bf_gain_1]=analyze_bf('data_bf_ground_on');
[s1_2,s2_2,sn_2,bf_2,bf_gain_2]=analyze_bf('data_drone_flying_935_2_old_2021_03_18');
[s1_3,s2_3,sn_3,bf_3,bf_gain_3]=analyze_bf('data_bf_drone1');
[s1_4,s2_4,sn_4,bf_4,bf_gain_4]=analyze_bf('data_bf_antenna_sep_2021_03_30');



%%
figure;
hold on
cdfplot(bf_gain_1 )
cdfplot(bf_gain_2 )
cdfplot(bf_gain_3 )
cdfplot(bf_gain_4 )

legend('Chair','Drone 2ant coll','Drone 1ant','Drone 2ant sep','Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
xlim([0,2.5])
title('')
box on
%%
% figure;
% hold on
% plot(s1_1)
% plot(s2_1)
% plot(bf_1)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_2)
% plot(s2_2)
% plot(bf_2)
% ylabel('Magnitude')
% xlabel('Time Index')
% 
% figure;
% hold on
% plot(s1_3)
% plot(s2_3)
% plot(bf_3)
% ylabel('Magnitude')
% xlabel('Time Index')