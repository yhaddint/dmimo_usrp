load('payload')

x = read_complex_binary('data_bf_2020-02-25.dat');

data_len = 30000-1700+512;




zrs_loc = find(x==0);
lst_zrs = diff(zrs_loc)>1;
lst_zrs = zrs_loc(lst_zrs);

x = x(lst_zrs(1):end);
n_pkts = floor(numel(x)/data_len);
xr = reshape(x(1:n_pkts*data_len),[data_len,n_pkts]);

xr = xr(1:floor(size(xr,1)/8)*8,:);

sig = xr(:,208);
plotc(angle(sig))
%hold on
%plotc(payload)

preamble_loc = (313:513)';
data_loc = (540:2830)';
preamble = payload(preamble_loc);
%plot(preamble)

align_payload  = payload(313:end);



h = rcosdesign(0.35,16,8);

f_payload = conv(h,align_payload);
ds_payload  = f_payload(1:8:end);

f_sig = conv(h,sig);
%ds_sig = reshape(f_sig,[8,numel(f_sig)/8]);
ds_sig = f_sig(1:8:end);
plotc(ds_sig)
hold on;
%plot(ds_payload)


x_phase = [preamble_loc;data_loc] ;
sig_phase = angle(ds_sig(x_phase));
%plot(sig_phase)
neg = sig_phase < 0;
sig_phase(neg) = sig_phase(neg) + pi;
plot(x_phase,sig_phase)


x = [ones(numel(sig_phase),1) x_phase];
l = inv(x'*x)*x'*sig_phase;
phs = l(1) + (1:numel(ds_sig))'*l(2);
plotc(phs)
hold off
plotc(ds_sig.*exp(-1j*phs))
hold on
plot(ds_payload)
% plotc(sig)
% sig_phase = angle(sig);
% neg = sig_phase < 0;
% sig_phase(neg) = sig_phase(neg) + pi;
% 
% x_phase = (1:numel(sig_phase))';
% hold on
% plotc((sig_phase))
% 
% x = [ones(numel(sig_phase),1) x_phase];
% l = inv(x'*x)*x'*sig_phase;
% hold on;
% plotc((l(1) + x_phase*l(2)))