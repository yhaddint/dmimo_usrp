load('data_bf_2020-03-12_1.mat')
%%
figure(1)
histogram(1-pkt1_err,linspace(0,0.5,20),'Normalization','probability')
hold on
histogram(1-pkt2_err,linspace(0,0.5,20),'Normalization','probability')
histogram(1-pkt3_err,linspace(0,0.5,20),'Normalization','probability')
hold off
legend('slv1','slv2','BF','location','Best')
xlabel('BER')
ylabel('Probability')
disp([mean(1-pkt1_err),mean(1-pkt2_err),mean(1-pkt3_err) ])
%%
figure(2)
histogram(sig1_pwr,'Normalization','probability')
hold on
histogram(sig2_pwr,'Normalization','probability')
histogram(sig3_pwr,'Normalization','probability')
histogram(noise_var,'Normalization','probability')
hold off
legend('slv1','slv2','BF','Noise','location','Best')
xlabel('Signal Power')
ylabel('Probability')
%%

mn_noise_pwr = mean(noise_var);

sig1_only_pwr = get_sig_only_pwr(sig1_pwr , mn_noise_pwr);
sig2_only_pwr = get_sig_only_pwr(sig2_pwr , mn_noise_pwr);
sig3_only_pwr = get_sig_only_pwr(sig3_pwr , mn_noise_pwr);

snr1 = 10*log10(sig1_only_pwr/mn_noise_pwr);
snr2 = 10*log10(sig2_only_pwr/mn_noise_pwr);
snr3 = 10*log10(sig3_only_pwr/mn_noise_pwr);


%%

figure(3)
x = linspace(-10.5,15.5,25);
histogram(snr1,x,'Normalization','probability')
hold on
histogram(snr2,x,'Normalization','probability')
histogram(snr3,x,'Normalization','probability')
hold off
legend('slv1','slv2','BF','location','Best')
xlabel('SNR (dB)')
ylabel('Probability')
ax = axis();
axis([ -10,15 ax(3:4)])

%%
gn = (sqrt(sig3_only_pwr) )./(sqrt(sig1_only_pwr) +sqrt(sig2_only_pwr));
figure(4)
histogram( gn ,'Normalization','probability')

xlabel('BF Magnitude Compared to Addition')
ylabel('Probability')
%%
figure(5);
scatter(snr1,1-pkt1_err)
set(gca,'yscale','log')
hold on
scatter(snr2,1-pkt2_err)
scatter(snr3,1-pkt3_err)
ax = axis();
axis([ -10,15 ax(3:4)])
hold off
legend('slv1','slv2','BF','location','SouthWest')
xlabel('SNR (dB)')
ylabel('BER')
%%

function sig_only = get_sig_only_pwr(sig,noise_pwr)
sig_only = sig;
sig_only(sig_only<0) = 1e-10;
end