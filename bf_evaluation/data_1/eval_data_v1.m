
ref = get_ref_sig();
global ref_len
ref_len = numel(ref);
x = read_complex_binary('data/data_bf_2020-02-26_2.dat');
data_len = 30000+512;

preamble_loc = 300:430;
payload_loc = 804:29938;

del = 8;
preamble_ds_loc = 47:68;
payload_ds_loc = 111:3751;



pkts = get_packets(x,data_len);

pkt_err = nan(size(pkts,2),1);
for pkt_i = 1: size(pkts,2)
    pkt = pkts(:,pkt_i);
%     plotc(pkt);
%     hold on
%     plot(ref/5)
%     hold off
    ds_pkt = downsample(pkt,del);
    ds_ref = downsample(ref,del);
    % plotc(ds_pkt)
    % hold on;


    %plot(ds_ref)
    %hold on
    rec = correct_phase(ds_pkt,preamble_ds_loc,payload_ds_loc);
    %plot(rec)

    ref_bits = ds_ref(payload_ds_loc)>0;
    pkt_bits = rec(payload_ds_loc)>0;
    corr = mean(ref_bits==pkt_bits);
    pkt_err(pkt_i) =  max([corr 1-corr]);
%     if pkt_err(pkt_i)~=1
%         figure;
%         plot(ds_ref)
%         hold on
%         plot(rec)
%     end
end
mean(pkt_err)

function ref  = get_ref_sig()
load('payload')
ref  = payload(42:30000);
ref = ref(1:floor(numel(ref)/8)*8);
end

function pkts = get_packets(x,data_len)
data_len = 30000+512;
zrs_loc = find(x==0);
lst_zrs = diff(zrs_loc)>10;
lst_zrs = zrs_loc(lst_zrs(2:end));

xt = x(lst_zrs(1):end);
n_pkts = floor(numel(xt)/data_len);
xr = reshape(xt(1:n_pkts*data_len),[data_len,n_pkts]);
global ref_len
pkts = xr(1:ref_len,:);
end


function ds_sig = downsample(pkt,del)
h = rcosdesign(0.35,16,8);
f_sig = conv(pkt,h);
ds_sig = f_sig(del:8:end);
end

function rec = correct_phase(pkt,preamble_loc,payload_loc)
ph = angle(pkt);

x_phase = [preamble_loc';payload_loc'] ;
sig_phase = ph(x_phase);
%plot(sig_phase)
neg = sig_phase < 0;
sig_phase(neg) = sig_phase(neg) + pi;
sig_phase = unwrap(sig_phase*2)/2;



x = [ones(numel(sig_phase),1) x_phase];
l = inv(x'*x)*x'*sig_phase;
phs = l(1) + (1:numel(pkt))'*l(2);
% hold off;
% plot(x_phase,sig_phase)
% hold on
% plot(phs)
%plotc(pkt.*exp(-1j*phs))
rec =real( pkt.*exp(-1j*phs));
end