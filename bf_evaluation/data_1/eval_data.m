
ref = get_ref_sig();
global ref_len
ref_len = numel(ref);
fname = 'data_bf_2020-03-12_1';
x = read_complex_binary(['../../data/' fname '.dat']);
data_len = 30000+512;




sps = 2;
del = 7;

noise1_loc = 9880:9960;
noise2_loc = 19880:19970;
noise_loc = [noise1_loc noise2_loc];
sig1_loc = 500:9500;
sig2_loc = 10500:19500; 
sig3_loc = 20500:29500; 

pkt1_ds_loc = 10:4937;
pkt2_ds_loc = 4999:9937;
pkt3_ds_loc = 9999:14937;


ds_ref = downsample(ref,9,sps);

pkts = get_packets(x,data_len);
pkts = pkts(:,1:end);


%%
pkt1_err = nan(size(pkts,2),1);
pkt2_err = nan(size(pkts,2),1);
pkt3_err = nan(size(pkts,2),1);
noise_var = nan(size(pkts,2),1);

sig1_pwr = nan(size(pkts,2),1);
sig2_pwr = nan(size(pkts,2),1);
sig3_pwr = nan(size(pkts,2),1);
%%
for pkt_i = 1:size(pkts,2)
    pkt = pkts(:,pkt_i);
%     plotc(pkt);
%     hold on
%     plot(ref/5)
%     hold off

    noise_var(pkt_i) = var(pkt(noise_loc));
    
    sig1_pwr(pkt_i) = calc_pwr(pkt(sig1_loc));
    sig2_pwr(pkt_i) = calc_pwr(pkt(sig2_loc));
    sig3_pwr(pkt_i) = calc_pwr(pkt(sig3_loc));

    ds_pkt = downsample(pkt,del,sps);
%     ds_pkt1 = downsample(pkt(sig1_loc),del,sps);
%     ds_pkt2 = downsample(pkt(sig2_loc),del,sps);
%     ds_pkt3 = downsample(pkt(sig3_loc),del,sps);
    % plotc(ds_pkt)
    % hold on;
%     rec1 = correct_phase(ds_pkt1);
%     rec2 = correct_phase(ds_pkt2);
%     rec3 = correct_phase(ds_pkt3);

    %plot(ds_ref)
    %hold on
    rec1 = correct_phase(ds_pkt(pkt1_ds_loc));
    rec2 = correct_phase(ds_pkt(pkt2_ds_loc));
    rec3 = correct_phase(ds_pkt(pkt3_ds_loc));
    %plot(rec)

    ref_bits = ds_ref>0;
    pkt1_bits = rec1>0;
    pkt2_bits = rec2>0;
    pkt3_bits = rec3>0;
    
    corr1 = calc_corr(pkt1_bits,ref_bits(pkt1_ds_loc));
    corr2 = calc_corr(pkt2_bits,ref_bits(pkt2_ds_loc));
    corr3 = calc_corr(pkt3_bits,ref_bits(pkt3_ds_loc));
    [corr1 corr2 corr3];
    pkt1_err(pkt_i) =  corr1;
    pkt2_err(pkt_i) =  corr2;
    pkt3_err(pkt_i) =  corr3;
    
    

end

%%
nanmean(pkt1_err)
nanmean(pkt2_err)
nanmean(pkt3_err)
%%
%save(fname,'pkt1_err','pkt2_err','pkt3_err','sig1_pwr','sig2_pwr','sig3_pwr','noise_var')
%%
figure(1)
hold off
plot(pkt1_err)
hold on;
plot(pkt2_err)
plot(pkt3_err)
hold off
ax = axis;
axis([ax(1:2) 0.49 1.01])
%%
figure(2)
histogram(pkt1_err,linspace(0.5,1.0,20),'Normalization','probability')
hold on
histogram(pkt2_err,linspace(0.5,1.0,20),'Normalization','probability')
histogram(pkt3_err,linspace(0.5,1.0,20),'Normalization','probability')
hold off
legend('pkt1','pkt2','BF','location','North')
xlabel('Correct Bits Ratio')
xlabel('Fraction')
%%
figure(3)
histogram(sig1_pwr,'Normalization','probability')
hold on
histogram(sig2_pwr,'Normalization','probability')
histogram(sig3_pwr,'Normalization','probability')
hold off
legend('pkt1','pkt2','BF','location','North')
xlabel('Signal Power')
xlabel('Fraction')
%%

function pwr = calc_pwr(sig)
pwr = mean(abs(sig).^2);
end

function corr = calc_corr(dec,ref)
%     [cr,idx] = xcorr(2*dec-1,2*ref-1,10);
%     [~,t]=max(abs(cr));
%     lg= idx(t);
    mx = -1;
    for lg = 0 :10
        corrA = mean(dec(lg+1:end )==ref(1:end-lg));
        corrA = max([corrA 1-corrA]);
        if corrA > mx
            mx =corrA;
            %lg
        end
    end
    for lg = -10:0
        corrA = mean(dec(1:end + lg )==ref(1-lg:end));
        corrA = max([corrA 1-corrA]);
        if corrA > mx
            mx =corrA;
            %lg
        end
    end
    corr = mx;
end

% function corr = calc_corr(dec,ref)
%     [cr,idx] = xcorr(2*dec-1,2*ref-2,10);
%     [~,t]=max(abs(cr));
%     lg= idx(t);
%     mx = -1;
%     for lg = 0 :20
%         corrA = mean(dec(lg+1:end )==ref(1:end-lg));
%         corrA = max([corrA 1-corrA]);
%         if corrA > mx
%             mx =corrA;
%             %lg
%         end
%     end
%     corr = mx;
% end
function ref  = get_ref_sig()
load('payload2')
ref  = payload2(42:30000);
ref = ref(1:floor(numel(ref)/8)*8);
end

function pkts = get_packets(x,data_len)
data_len = 30000+512;
zrs_loc = find(x==0);
lst_zrs = diff(zrs_loc)>10;
lst_zrs = zrs_loc(lst_zrs(2:end));

xt = x(lst_zrs(1):end);
n_pkts = floor(numel(xt)/data_len);
xr = reshape(xt(1:n_pkts*data_len),[data_len,n_pkts]);
global ref_len
pkts = xr(1:ref_len,:);
end


function ds_sig = downsample(pkt,del,sps)
interp_fac = 16;
pkt = interp(pkt,interp_fac);
h = rcosdesign(0.35,16,sps*interp_fac);
f_sig = conv(pkt,h);
%del = 9;
f_sig = f_sig(1:floor(numel(f_sig)/(sps*interp_fac))*(sps*interp_fac));
fr_sig = reshape(f_sig,[sps*interp_fac, numel(f_sig)/(sps*interp_fac)])';
f1 = fr_sig(:,1:sps*interp_fac-2);
f2 = fr_sig(:,2:sps*interp_fac-1);
f3 = fr_sig(:,3:sps*interp_fac);
fe = (f3-f1)./(f2+1e-12);
mer = mean(abs(fe),1);
[~,mi] = min(mer);
del = mi+1 ;
ds_sig = f_sig(del:sps*interp_fac:end);
end

function rec = correct_phase(pkt)
ph = angle(pkt);

x_phase =(1:numel(pkt))';
sig_phase = ph(x_phase);
%plot(sig_phase)
neg = sig_phase < 0;
sig_phase(neg) = sig_phase(neg) + pi;
%sig_phase = unwrap(sig_phase*2)/2;

x_phase1 = x_phase(1:500);
sig_phase1 = sig_phase(1:500);
% x = [ones(numel(sig_phase1),1) x_phase1];
% l = inv(x'*x)*x'*sig_phase1;
% phs = l(1) + x_phase*l(2);
phs = median(sig_phase1);

% hold off;
% plot(x_phase,sig_phase)
% hold on
% plot(phs)
% plotc(pkt.*exp(-1j*phs))
rec =real( pkt.*exp(-1j*phs));
end