[s1,s2,s3,s4,bf] = analyze_bf_phase(13);

s1n = s1./abs(s1);
s2n = s2./abs(s2);
s3n = s3./abs(s3);
s4n = s4./abs(s4);

bns = linspace(-pi,pi,50)
figure;
hold on
histogram( angle(conj(s4n).*s2n),bns)
histogram( angle(conj(s4n).*s3n),bns)
histogram( angle(conj(s2n).*s3n),bns)

figure;
hold on
histogram( angle(conj(s4n).*s1n),bns)
histogram( angle(conj(s2n).*s1n),bns)
histogram( angle(conj(s3n).*s1n),bns)

[mean(angle(conj(s4n).*s2n)),std(angle(conj(s4n).*s2n))]
[mean(angle(conj(s4n).*s3n)),std(angle(conj(s4n).*s3n))]
[mean(angle(conj(s4n).*s1n)),std(angle(conj(s4n).*s1n))]