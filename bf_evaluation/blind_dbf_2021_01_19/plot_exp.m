

[f1,x1,ber1,bfsnr1,presnr1] = analyze_bf(1);
[f2,x2,ber2,bfsnr2,presnr2] = analyze_bf(4);
[f3,x3,ber3,bfsnr3,presnr3] = analyze_bf(5);
[f4,x4,ber4,bfsnr4,presnr4] = analyze_bf(3);
[f_np,x_np,ber_np,bfsnr_np,presnrnp_np] = analyze_bf(2);


figure;
hold on
plot(x1,1-f1)
plot(x2,1-f2)
plot(x3,1-f3)
plot(x4,1-f4)
plot(x_np,1-f_np)
xlim([0,4])
xlabel('BF Gain')
ylabel('P(BF Gain>Abcissa)')
title('BF Gain')
box on
legend('2.5','2','1','0','2.5np')







%%


%%

file_loc = pwd;
timestamp = datetime;


% save('outdoor_fake_rx','f1','x1','ber1','bfsnr1','presnr1',...
%                          'f2_circle','x2_circle','ber2_circle','bfsnr2_circle','presnr2_circle',...
%                          'f3_circle','x3_circle','ber3_circle','bfsnr3_circle','presnr3_circle',...
%                          'f2_dist','x2_dist','ber2_dist','bfsnr2_dist','presnr2_dist',...
%                          'f3_dist','x3_dist','ber3_dist','bfsnr3_dist','presnr3_dist',...
%                          'f2_par','x2_par','ber2_par','bfsnr2_par','presnr2_par',...
%                          'f3_par','x3_par','ber3_par','bfsnr3_par','presnr3_par',...
%                          'f4_dist','x4_dist','ber4_dist','bfsnr4_dist','presnr4_dist',...
%                         'file_loc','timestamp')