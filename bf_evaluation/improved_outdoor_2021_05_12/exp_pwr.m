fldr='';
% [s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf_lab_ground_ekfii']);
% [s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf([fldr,'data_bf_lab_ground_ekf']);
% [s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf([fldr,'data_bf_lab_ground_oneshot']);

[s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf_lab_uav_ekfii']);
[s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf([fldr,'data_bf_lab_uav_ekf']);
[s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf([fldr,'data_bf_lab_uav_oneshot']);

%[s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf']);

%[s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'2021-05-12_old/data_bf_lab_uav_ekfii_old']);
% [s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf_outdoor_ground_ekfii']);
% [s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf([fldr,'data_bf_outdoor_ground_ekf']);
% [s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf([fldr,'data_bf_outdoor_ground_oneshot']);
% 
% 
% 
snr1=mean(20*log10(mean([s1_1;s2_1])./sn_1));
snr2=mean(20*log10(mean([s1_2;s2_2])./sn_2));
snr3=mean(20*log10(mean([s1_3;s2_3])./sn_3));


%%
set(0, 'DefaultLineLineWidth', 2);
%figure;
hold on
cdfplot(bf_gain_1.^2/2 )
cdfplot(bf_gain_2.^2/2 )
cdfplot(bf_gain_3.^2/2 )
xlim([0,2.5])

legend('EKF II','EKF','Oneshot','Location','best')
% legend( cellstr(num2str(round(snr_vec_est)', 'SNR=%-ddB')),'Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
%xlim([0,2.5])
title('')
box on
%%





