fldr='';

% [s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf_phs([fldr,'data_bf_ground_ekfii']);
% [s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf_phs([fldr,'data_bf_ground_ekf']);
% [s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf_phs([fldr,'data_bf_ground_oneshot']);
% leg={'Ground EKFII','Ground EKF','Ground Oneshot'}

% [s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf_phs([fldr,'data_bf_ground_ekfii']);
% [s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf_phs([fldr,'data_bf_hover_ekfii']);
% [s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf_phs([fldr,'data_bf_hover_oneshot_old']);
% leg={'Ground EKF','Hover EKF','Hover Oneshot'};

% [s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf_phs([fldr,'data_bf_ground_ekfii']);
% [s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf_phs([fldr,'data_bf_line_ekfii']);
% [s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf_phs([fldr,'data_bf_line_oneshot']);
% leg={'Ground EKF','Line EKF','Line Oneshot'};




% 
[s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf_phs([fldr,'data_bf_ground_ekfii']);
[s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf_phs([fldr,'data_bf_hover_ekfii']);
[s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf_phs([fldr,'data_bf_line_ekfii']);
leg={'Ground EKF','Hover EKF','Line EKF'};

% [s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf_ground_ekfii']);
% [s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf_phs([fldr,'data_bf_ground_ekfii']);
% [s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf([fldr,'data_bf_ground_oneshot']);
% leg={'Measured','Predicted','Oneshot'};

% 
% 
% 
snr1=mean(20*log10(mean([s1_1;s2_1])./sn_1));
snr2=mean(20*log10(mean([s1_2;s2_2])./sn_2));
snr3=mean(20*log10(mean([s1_3;s2_3])./sn_3));


%%
figure;
set(0, 'DefaultLineLineWidth', 2);
%figure;
hold on
cdfplot(bf_gain_1(vld1) )
cdfplot(bf_gain_2(vld2) )
cdfplot(bf_gain_3(vld3) )
%xlim([0,2.5])

legend(leg,'Location','best')
% legend( cellstr(num2str(round(snr_vec_est)', 'SNR=%-ddB')),'Location','best')
ylabel('P(BF Gain < abscissa)')
xlabel('BF Gain')
%xlim([0,2.5])
title('')
box on
set(gcf,'color','white')
%%





