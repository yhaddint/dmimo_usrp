function [vld,snr_all,nm_snr] = calc_snr(sig_mag,noise_std)

snr_all = 20*log10(mean(sig_mag)./noise_std);

nm_snr=mode(snr_all);
vld=abs(snr_all-nm_snr)<1;