fldr='data_2020_09_01/';

snr1=0;snr2=0;snr3=0;snr4=0;snr5=0;snr6=0;srn7=0;snr8=0;
gn1=0;gn2=0;gn3=0;gn4=0;gn5=0;gn6=0;ng7=0;gn8=0;

noise_mag = mean(abs(read_complex_binary('data_bf_noise2.dat')));


fname='data_bf_ground';
[s1_f1,s2_f1,~,~,bf_f1,~]=analyze_bf_fun([fldr,fname]);

fname='data_bf_drone_chair';
[s1_f2,s2_f2,~,~,bf_f2,~]=analyze_bf_fun([fldr,fname]);

fname='data_bf_drone_fly';
[s1_f3,s2_f3,~,~,bf_f3,~]=analyze_bf_fun([fldr,fname]);
s1_f3 =s1_f3(1:930);
s2_f3 =s2_f3(1:930);
bf_f3 =bf_f3(1:930);

gn1= (bf_f1)./mean([s1_f1; s2_f1; ]);
gn2= (bf_f2)./mean([s1_f2; s2_f2; ]);
gn3= (bf_f3)./mean([s1_f3; s2_f3; ]);
%gn4= (bf_f4)./mean([s1_f4; s2_f4; ]);


snr1= 20*log10(mean([s1_f1; s2_f1; ])/noise_mag);
snr1([317,431])=[];
snr2= 20*log10(mean([s1_f2; s2_f2; ])/noise_mag);
snr2([132])=[];
snr3= 20*log10(mean([s1_f3; s2_f3; ])/noise_mag);
%snr4= 20*log10(mean([s1_f4; s2_f4; ])/noise_mag);

%%
set(0, 'DefaultLineLineWidth', 2);
figure;
hold on
cdfplot(gn1)
cdfplot(gn2)
cdfplot(gn3)
%cdfplot(gn4)
xlabel('BF Gain')
ylabel('P(BF Gain < abscissa)')
title('BF Gain')
box on
legend('Table','Drone Fixed','Drone Air','Location','Best')
%%

snr_vec = [mean(snr1),mean(snr2),mean(snr3)];% ,mean(snr4)
       
gn_vec = [mean(gn1),mean(gn2),mean(gn3)];% ,mean(gn4)
figure(15);
hold on
plot(snr_vec,gn_vec)
xlabel('SNR')
ylabel('BF Gain')
title('BF Gain')
box on


%%
figure;
hold on
cdfplot(gn1)
cdfplot(gn3)
xlabel('BF Gain')
ylabel('P(BF Gain < abscissa)')
title('BF Gain')
box on
title('')
legend('Chair','Flying','Location','Best')

%%

file_loc = pwd;
timestamp = datetime;


% save('rot_2n_2020-07-26','bf_f1','s1_f1','s2_f1',...
%                         'bf_f2','s1_f2','s2_f2',...
%                         'bf_f3','s1_f3','s2_f3',...
%                         'bf_f4','s1_f4','s2_f4',...
%                         'noise_mag','file_loc','timestamp')