fldr='var_master2/';
[s1_1,s2_1,sn_1,bf_1,bf_gain_1,vld1]=analyze_bf([fldr,'data_bf_noekf_0p65']);
[s1_2,s2_2,sn_2,bf_2,bf_gain_2,vld2]=analyze_bf([fldr,'data_bf_noekf_0p60']);
[s1_3,s2_3,sn_3,bf_3,bf_gain_3,vld3]=analyze_bf([fldr,'data_bf_noekf_0p55']);
[s1_4,s2_4,sn_4,bf_4,bf_gain_4,vld4]=analyze_bf([fldr,'data_bf_noekf_0p50']);
[s1_5,s2_5,sn_5,bf_5,bf_gain_5,vld5]=analyze_bf([fldr,'data_bf_noekf_0p45']);
[s1_6,s2_6,sn_6,bf_6,bf_gain_6,vld6]=analyze_bf([fldr,'data_bf_noekf_0p40']);
[s1_7,s2_7,sn_7,bf_7,bf_gain_7,vld7]=analyze_bf([fldr,'data_bf_noekf_0p35']);
[s1_8,s2_8,sn_8,bf_8,bf_gain_8,vld8]=analyze_bf([fldr,'data_bf_noekf_0p30']);
[s1_9,s2_9,sn_9,bf_9,bf_gain_9,vld9]=analyze_bf([fldr,'data_bf_noekf_0p25']);


[~,snr1_all,snr1]= calc_snr([s1_1;s2_1],sn_1);
[~,snr2_all,snr2]= calc_snr([s1_2;s2_2],sn_2);
[~,snr3_all,snr3]= calc_snr([s1_3;s2_3],sn_3);
[~,snr4_all,snr4]= calc_snr([s1_4;s2_4],sn_4);
[~,snr5_all,snr5]= calc_snr([s1_5;s2_5],sn_5);
[~,snr6_all,snr6]= calc_snr([s1_6;s2_6],sn_6);
[~,snr7_all,snr7]= calc_snr([s1_7;s2_7],sn_7);
[~,snr8_all,snr8]= calc_snr([s1_8;s2_8],sn_8);
[~,snr9_all,snr9]= calc_snr([s1_9;s2_9],sn_9);



snr_all=[snr1_all(vld1),snr2_all(vld2),snr3_all(vld3),snr4_all(vld4)...
    ,snr5_all(vld5),snr6_all(vld6),snr7_all(vld7),snr8_all(vld8),snr9_all(vld9)];
bf_gain_all=[bf_gain_1(vld1),bf_gain_2(vld2),bf_gain_3(vld3),bf_gain_4(vld4),...
    bf_gain_5(vld5),bf_gain_6(vld6),bf_gain_7(vld7),bf_gain_8(vld8),bf_gain_9(vld9)].^2/2;

snr_stp = 4;
snr_vec_est = 0:snr_stp:35;

n_el = numel(snr_vec_est); 
gn_mn_vec = zeros(n_el,1);
gn_std_vec = zeros(n_el,1);

for snr_i = 1:numel(snr_vec_est)
    ind = snr_all>snr_vec_est(snr_i)-snr_stp/2 & snr_all<snr_vec_est(snr_i)+snr_stp/2;
    gn_mn_vec(snr_i)=mean(bf_gain_all(ind));
    gn_std_vec(snr_i)=std(bf_gain_all(ind));
end

figure;
errorbar(snr_vec_est,gn_mn_vec,gn_std_vec)


       