function ds_sig = my_downsample(pkt,deli,sps)
interp_fac = 16;
pkt = interp(pkt,interp_fac);
h = rcosdesign(0.35,16,sps*interp_fac);
f_sig = conv(pkt,h);
%del = 9;

fr_sig = reshape(f_sig,[sps*interp_fac, numel(f_sig)/(sps*interp_fac)])';
f1 = fr_sig(:,1:sps*interp_fac-2);
f2 = fr_sig(:,2:sps*interp_fac-1);
f3 = fr_sig(:,3:sps*interp_fac);
fe = (f3-f1)./(f2+1e-12);
mer = mean(abs(fe),1);
[~,mi] = min(mer);
del = mi+deli;
ds_sig = f_sig(del:sps*interp_fac:end);
end