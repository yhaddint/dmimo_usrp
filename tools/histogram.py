import matplotlib.pyplot as plt
import numpy as np
import os

def findpeak(xs,ys):
    '''Finds the peak by computing the midpoint between the max and average y,
    and thresholding all of the samples against this midpoint. Xs that
    correspond to Ys above the midpoint are averaged together to find the center
    of the peak.'''
    ys = [10**y for y in ys]
    avg = sum(ys)/len(ys)
    halfway = (max(ys) + avg)/2
    peak_xs = filter(lambda x : x, [x if y > halfway else 0 for x,y in
                                    zip(xs,ys)])
    return sum(peak_xs)/len(peak_xs)

histdata = []

# Across all files beginning with "Trace Capture" in the current directory:
# Read out the data into xs,ys
# Find the peak
# Append the peak position to the histdata
for f in filter(lambda x : x.startswith("Trace Capture"),
                os.listdir(os.curdir)):
    xs,ys = zip(*[[float(y) for y in x.strip().split(',')] for x in
                  open(f).readlines()[1:]])

    peak = findpeak(xs,ys) 

    histdata.append(peak)

# Plot the histogram
plt.hist(histdata, 50)

plt.show()
