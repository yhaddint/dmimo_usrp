scp /home/samer/Documents/distributed_mimo/dmimo_usrp/demo-conix-2019/demo1/* odroid@10.42.0.211:/home/odroid/Documents/distributed_mimo/dmimo_usrp/demo-conix-2019/demo1/

scp /home/samer/Documents/distributed_mimo/dmimo_usrp/demo-conix-2019/demo1/* odroid@10.42.0.63:/home/odroid/Documents/distributed_mimo/dmimo_usrp/demo-conix-2019/demo1/






arp -a



ssh odroid@192.168.1.201
cd Documents/distributed_mimo/dmimo_usrp/demo-conix-2019/demo1/
source ~/prefix/setup_env.sh
taskset 0xF0 ./node1_demo1.py --ip 192.168.1.200 --tx-gain 0.75

ssh odroid@10.42.0.63
cd Documents/distributed_mimo/dmimo_usrp/demo-conix-2019/demo1/
source ~/prefix/setup_env.sh
taskset 0xF0 ./node1_demo1.py --ip 10.42.0.1 --tx-gain 0.75


ssh odroid@192.168.1.202
cd Documents/distributed_mimo/dmimo_usrp/demo-conix-2019/demo1/
source ~/prefix/setup_env.sh
taskset  0xF0 ./node2_demo1.py --tx-gain 0.75


ssh odroid@10.42.0.211
cd Documents/distributed_mimo/dmimo_usrp/demo-conix-2019/demo1/
source ~/prefix/setup_env.sh
taskset  0xF0 ./node2_demo1.py --tx-gain 0.75


192.168.1.201
192.168.1.202


master 192.168.145.38
slave1 192.168.145.172
