#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Node2 Script
# GNU Radio version: 3.7.13.5
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import commpy
import howto
import numpy
import time
import tutorial


class node2_script(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Node2 Script")

        ##################################################
        # Variables
        ##################################################
        self.tx_start_guard = tx_start_guard = 20
        self.second_zeros = second_zeros = 100
        self.phase_det_len = phase_det_len = 500
        self.timing_margin = timing_margin = 60000
        self.my_second_gap = my_second_gap = second_zeros+tx_start_guard+phase_det_len
        self.initial_zeros = initial_zeros = 400
        self.guard_time = guard_time = 100000
        self.data_len = data_len = 50000
        self.zcs = zcs = commpy.sequences.zcsequence(25,63)
        self.silent_period = silent_period = timing_margin + data_len + guard_time
        self.samp_rate = samp_rate = 1e6
        self.pkt_len = pkt_len = initial_zeros+phase_det_len+my_second_gap+data_len
        self.payload = payload = numpy.concatenate(  (  numpy.ones((data_len/2,)) , numpy.array(([0,]*20+[1,]+[0,]*79)*int(data_len/100/2)  ) ))
        self.n_train = n_train = 10
        self.my_start_gap = my_start_gap = initial_zeros
        self.gap = gap = 10000
        self.freq_tx = freq_tx = 900e6
        self.freq_rx = freq_rx = 900e6

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0_0_0 = uhd.usrp_source(
        	",".join(('serial = 316B7E8', "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0_0_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0_0_0.set_center_freq(uhd.tune_request(freq_rx, 10e6), 0)
        self.uhd_usrp_source_0_0_0.set_normalized_gain(0.5, 0)
        self.uhd_usrp_source_0_0_0.set_antenna('RX2', 0)
        self.uhd_usrp_source_0_0_0.set_auto_dc_offset(False, 0)
        self.uhd_usrp_source_0_0_0.set_auto_iq_balance(False, 0)
        self.uhd_usrp_sink_0_0 = uhd.usrp_sink(
        	",".join(('serial = 316B7E8', "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0_0.set_clock_source('external', 0)
        self.uhd_usrp_sink_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)
        self.uhd_usrp_sink_0_0.set_center_freq(uhd.tune_request(freq_tx, 10e6), 0)
        self.uhd_usrp_sink_0_0.set_normalized_gain(0.5, 0)
        self.uhd_usrp_sink_0_0.set_antenna('TX/RX', 0)
        self.tutorial_CFO_EKF_0 = tutorial.CFO_EKF(samp_rate, 63*n_train, 63, 0,
                                 ([0.0001, 0.0013,0.0013,0.2561]), ([0.1,0,0,0,0.1,0,0,0,20]),
                                 (630*2*2)/1e6)
        self.howto_trig_repeat_0 = howto.trig_repeat(10, 0, silent_period)
        self.howto_frac_timing_freq_phase_synch_0 = howto.frac_timing_freq_phase_synch(timing_margin, pkt_len, samp_rate, 1, 16, True)
        self.howto_first_trigger_0 = howto.first_trigger(silent_period)
        self.fir_filter_xxx_0_0 = filter.fir_filter_fff(1, (([1.0/10.0]+[0.0]*62)*10))
        self.fir_filter_xxx_0_0.declare_sample_delay(0)
        self.fft_filter_xxx_0 = filter.fft_filter_ccc(1, (numpy.conj(zcs)), 1)
        self.fft_filter_xxx_0.declare_sample_delay(0)
        self.blocks_vector_source_x_0_1_0 = blocks.vector_source_c(numpy.concatenate((numpy.zeros((initial_zeros,)),numpy.ones((phase_det_len,))*0.8,numpy.zeros((my_second_gap)),payload*0.5)), True, 1, [])
        self.blocks_peak_detector2_fb_0 = blocks.peak_detector2_fb(5, 630, 0.01)
        self.blocks_null_sink_0_1 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_delay_1_0_0 = blocks.delay(gr.sizeof_gr_complex*1, 630-2)
        self.blocks_complex_to_mag_0 = blocks.complex_to_mag(1)
        self.analog_const_source_x_1_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_1_0, 0), (self.howto_frac_timing_freq_phase_synch_0, 3))
        self.connect((self.blocks_complex_to_mag_0, 0), (self.fir_filter_xxx_0_0, 0))
        self.connect((self.blocks_delay_1_0_0, 0), (self.howto_frac_timing_freq_phase_synch_0, 1))
        self.connect((self.blocks_delay_1_0_0, 0), (self.tutorial_CFO_EKF_0, 0))
        self.connect((self.blocks_peak_detector2_fb_0, 1), (self.blocks_null_sink_0_1, 0))
        self.connect((self.blocks_peak_detector2_fb_0, 0), (self.howto_first_trigger_0, 0))
        self.connect((self.blocks_vector_source_x_0_1_0, 0), (self.howto_frac_timing_freq_phase_synch_0, 4))
        self.connect((self.fft_filter_xxx_0, 0), (self.blocks_complex_to_mag_0, 0))
        self.connect((self.fir_filter_xxx_0_0, 0), (self.blocks_peak_detector2_fb_0, 0))
        self.connect((self.howto_first_trigger_0, 0), (self.howto_frac_timing_freq_phase_synch_0, 0))
        self.connect((self.howto_first_trigger_0, 0), (self.howto_trig_repeat_0, 0))
        self.connect((self.howto_frac_timing_freq_phase_synch_0, 0), (self.uhd_usrp_sink_0_0, 0))
        self.connect((self.howto_trig_repeat_0, 0), (self.tutorial_CFO_EKF_0, 1))
        self.connect((self.tutorial_CFO_EKF_0, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.tutorial_CFO_EKF_0, 3), (self.blocks_null_sink_0_0, 0))
        self.connect((self.tutorial_CFO_EKF_0, 2), (self.blocks_null_sink_0_0_0, 0))
        self.connect((self.tutorial_CFO_EKF_0, 1), (self.howto_frac_timing_freq_phase_synch_0, 2))
        self.connect((self.uhd_usrp_source_0_0_0, 0), (self.blocks_delay_1_0_0, 0))
        self.connect((self.uhd_usrp_source_0_0_0, 0), (self.fft_filter_xxx_0, 0))

    def get_tx_start_guard(self):
        return self.tx_start_guard

    def set_tx_start_guard(self, tx_start_guard):
        self.tx_start_guard = tx_start_guard
        self.set_my_second_gap(self.second_zeros+self.tx_start_guard+self.phase_det_len)

    def get_second_zeros(self):
        return self.second_zeros

    def set_second_zeros(self, second_zeros):
        self.second_zeros = second_zeros
        self.set_my_second_gap(self.second_zeros+self.tx_start_guard+self.phase_det_len)

    def get_phase_det_len(self):
        return self.phase_det_len

    def set_phase_det_len(self, phase_det_len):
        self.phase_det_len = phase_det_len
        self.set_pkt_len(self.initial_zeros+self.phase_det_len+self.my_second_gap+self.data_len)
        self.set_my_second_gap(self.second_zeros+self.tx_start_guard+self.phase_det_len)
        self.blocks_vector_source_x_0_1_0.set_data(numpy.concatenate((numpy.zeros((self.initial_zeros,)),numpy.ones((self.phase_det_len,))*0.8,numpy.zeros((self.my_second_gap)),self.payload*0.5)), [])

    def get_timing_margin(self):
        return self.timing_margin

    def set_timing_margin(self, timing_margin):
        self.timing_margin = timing_margin
        self.set_silent_period(self.timing_margin + self.data_len + self.guard_time)

    def get_my_second_gap(self):
        return self.my_second_gap

    def set_my_second_gap(self, my_second_gap):
        self.my_second_gap = my_second_gap
        self.set_pkt_len(self.initial_zeros+self.phase_det_len+self.my_second_gap+self.data_len)
        self.blocks_vector_source_x_0_1_0.set_data(numpy.concatenate((numpy.zeros((self.initial_zeros,)),numpy.ones((self.phase_det_len,))*0.8,numpy.zeros((self.my_second_gap)),self.payload*0.5)), [])

    def get_initial_zeros(self):
        return self.initial_zeros

    def set_initial_zeros(self, initial_zeros):
        self.initial_zeros = initial_zeros
        self.set_pkt_len(self.initial_zeros+self.phase_det_len+self.my_second_gap+self.data_len)
        self.set_my_start_gap(self.initial_zeros)
        self.blocks_vector_source_x_0_1_0.set_data(numpy.concatenate((numpy.zeros((self.initial_zeros,)),numpy.ones((self.phase_det_len,))*0.8,numpy.zeros((self.my_second_gap)),self.payload*0.5)), [])

    def get_guard_time(self):
        return self.guard_time

    def set_guard_time(self, guard_time):
        self.guard_time = guard_time
        self.set_silent_period(self.timing_margin + self.data_len + self.guard_time)

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len
        self.set_silent_period(self.timing_margin + self.data_len + self.guard_time)
        self.set_pkt_len(self.initial_zeros+self.phase_det_len+self.my_second_gap+self.data_len)
        self.set_payload(numpy.concatenate(  (  numpy.ones((self.data_len/2,)) , numpy.array(([0,]*20+[1,]+[0,]*79)*int(self.data_len/100/2)  ) )))

    def get_zcs(self):
        return self.zcs

    def set_zcs(self, zcs):
        self.zcs = zcs
        self.fft_filter_xxx_0.set_taps((numpy.conj(self.zcs)))

    def get_silent_period(self):
        return self.silent_period

    def set_silent_period(self, silent_period):
        self.silent_period = silent_period

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_source_0_0_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_0_0.set_samp_rate(self.samp_rate)

    def get_pkt_len(self):
        return self.pkt_len

    def set_pkt_len(self, pkt_len):
        self.pkt_len = pkt_len

    def get_payload(self):
        return self.payload

    def set_payload(self, payload):
        self.payload = payload
        self.blocks_vector_source_x_0_1_0.set_data(numpy.concatenate((numpy.zeros((self.initial_zeros,)),numpy.ones((self.phase_det_len,))*0.8,numpy.zeros((self.my_second_gap)),self.payload*0.5)), [])

    def get_n_train(self):
        return self.n_train

    def set_n_train(self, n_train):
        self.n_train = n_train

    def get_my_start_gap(self):
        return self.my_start_gap

    def set_my_start_gap(self, my_start_gap):
        self.my_start_gap = my_start_gap

    def get_gap(self):
        return self.gap

    def set_gap(self, gap):
        self.gap = gap

    def get_freq_tx(self):
        return self.freq_tx

    def set_freq_tx(self, freq_tx):
        self.freq_tx = freq_tx
        self.uhd_usrp_sink_0_0.set_center_freq(uhd.tune_request(self.freq_tx, 10e6), 0)

    def get_freq_rx(self):
        return self.freq_rx

    def set_freq_rx(self, freq_rx):
        self.freq_rx = freq_rx
        self.uhd_usrp_source_0_0_0.set_center_freq(uhd.tune_request(self.freq_rx, 10e6), 0)


def main(top_block_cls=node2_script, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
