#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Node1
# Generated: Sun Sep 29 15:59:55 2019
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import commpy
import howto
import numpy
import numpy as np
import sip
import sys
import time
import tutorial
from gnuradio import qtgui


class node1(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Node1")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Node1")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "node1")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.tx_start_guard = tx_start_guard = 0
        self.timing_margin2 = timing_margin2 = 10000
        self.timing_margin = timing_margin = 60000
        self.initial_zeros = initial_zeros = 400
        self.guard_time = guard_time = 100000
        self.data_len = data_len = 50000
        self.zcs = zcs = commpy.sequences.zcsequence(25,63)
        self.silent_period = silent_period = timing_margin + data_len + guard_time
        self.second_zeros_hard_coded = second_zeros_hard_coded = 0
        self.samp_rate = samp_rate = 1e6
        self.phs2 = phs2 = -0.2
        self.phs = phs = 0
        self.phase_det_len = phase_det_len = 200
        self.payload = payload = np.array([1,])
        self.n_train = n_train = 10
        self.my_start_gap = my_start_gap = initial_zeros+tx_start_guard
        self.gap = gap = 10000
        self.freq_tx = freq_tx = 915e6
        self.freq_rx = freq_rx = 915e6
        self.actual_data_len = actual_data_len = data_len-timing_margin2

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0_0_0 = uhd.usrp_source(
        	",".join(('serial = 3170400', "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0_0_0.set_clock_source('internal', 0)
        self.uhd_usrp_source_0_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0_0_0.set_center_freq(uhd.tune_request(freq_rx, 10e6), 0)
        self.uhd_usrp_source_0_0_0.set_normalized_gain(0.5, 0)
        self.uhd_usrp_source_0_0_0.set_antenna('RX2', 0)
        self.uhd_usrp_source_0_0_0.set_auto_dc_offset(True, 0)
        self.uhd_usrp_source_0_0_0.set_auto_iq_balance(True, 0)
        self.uhd_usrp_sink_0_0 = uhd.usrp_sink(
        	",".join(('serial = 3170400', "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0_0.set_clock_source('internal', 0)
        self.uhd_usrp_sink_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)
        self.uhd_usrp_sink_0_0.set_center_freq(uhd.tune_request(freq_tx, 10e6), 0)
        self.uhd_usrp_sink_0_0.set_normalized_gain(0.8, 0)
        self.uhd_usrp_sink_0_0.set_antenna('TX/RX', 0)
        self.tutorial_CFO_EKF_0 = tutorial.CFO_EKF(samp_rate, 63*n_train, 63, 0,
                                 ([0.0001, 0.0013,0.0013,0.2561]), ([0.1,0,0,0,0.1,0,0,0,20]),
                                 (630*2*2)/1e6)
        self.qtgui_time_sink_x_0_1_0 = qtgui.time_sink_f(
        	int(1e6), #size
        	samp_rate, #samp_rate
        	"", #name
        	2 #number of inputs
        )
        self.qtgui_time_sink_x_0_1_0.set_update_time(0.01)
        self.qtgui_time_sink_x_0_1_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0_1_0.set_y_label('ZC Correlation Output', "")

        self.qtgui_time_sink_x_0_1_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_1_0.set_trigger_mode(qtgui.TRIG_MODE_AUTO, qtgui.TRIG_SLOPE_POS, 0.5, 0, 0, "")
        self.qtgui_time_sink_x_0_1_0.enable_autoscale(True)
        self.qtgui_time_sink_x_0_1_0.enable_grid(True)
        self.qtgui_time_sink_x_0_1_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_1_0.enable_control_panel(True)

        if not True:
          self.qtgui_time_sink_x_0_1_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_1_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_1_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_1_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_1_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_1_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_1_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_1_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_1_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_1_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_time_sink_x_0_1_0_win)
        self.qtgui_time_sink_x_0_0 = qtgui.time_sink_f(
        	5000000, #size
        	samp_rate, #samp_rate
        	"", #name
        	2 #number of inputs
        )
        self.qtgui_time_sink_x_0_0.set_update_time(0.1)
        self.qtgui_time_sink_x_0_0.set_y_axis(-1200, -700)

        self.qtgui_time_sink_x_0_0.set_y_label('Estimated CFO', "Hz")

        self.qtgui_time_sink_x_0_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_0.enable_autoscale(True)
        self.qtgui_time_sink_x_0_0.enable_grid(True)
        self.qtgui_time_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0.enable_control_panel(False)

        if not True:
          self.qtgui_time_sink_x_0_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_time_sink_x_0_0_win)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_c(
        	1*int(1e5), #size
        	samp_rate, #samp_rate
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)

        if not True:
          self.qtgui_time_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_0.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_time_sink_x_0_win)
        self._phs2_range = Range(-100, 100, 0.02, -0.2, 200)
        self._phs2_win = RangeWidget(self._phs2_range, self.set_phs2, "phs2", "counter_slider", float)
        self.top_layout.addWidget(self._phs2_win)
        self._phs_range = Range(-4, 4, 0.001, 0, 200)
        self._phs_win = RangeWidget(self._phs_range, self.set_phs, "phs", "counter_slider", float)
        self.top_layout.addWidget(self._phs_win)
        self.howto_trig_repeat_0 = howto.trig_repeat(10, 0, silent_period)
        self.howto_frac_timing_freq_phase_synch_msg2_0 = howto.frac_timing_freq_phase_synch_msg2(timing_margin+my_start_gap, phase_det_len, 0.95, timing_margin2, actual_data_len, samp_rate, 1, 16, True)
        self.howto_first_trigger_0 = howto.first_trigger(silent_period)
        self.fir_filter_xxx_0_0 = filter.fir_filter_fff(1, (([1.0/10.0]+[0.0]*62)*10))
        self.fir_filter_xxx_0_0.declare_sample_delay(0)
        self.fft_filter_xxx_0 = filter.fft_filter_ccc(1, (numpy.conj(zcs)), 1)
        self.fft_filter_xxx_0.declare_sample_delay(0)
        self.blocks_vector_source_x_0_0_2 = blocks.vector_source_c(payload*0.8, True, 1, [])
        self.blocks_vector_source_x_0_0_0 = blocks.vector_source_f((0,), True, 1, [])
        self.blocks_socket_pdu_0 = blocks.socket_pdu("TCP_CLIENT", '192.168.1.111', '52001', 10000, True)
        self.blocks_peak_detector2_fb_0 = blocks.peak_detector2_fb(6.5, int(630*1.5), 0.05)
        self.blocks_null_sink_0_1 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_delay_1_0_0_0 = blocks.delay(gr.sizeof_float*1, 630-63-1)
        self.blocks_delay_1 = blocks.delay(gr.sizeof_gr_complex*1, 628)
        self.blocks_complex_to_mag_0 = blocks.complex_to_mag(1)
        self.blocks_char_to_float_0 = blocks.char_to_float(1, 0.1)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_socket_pdu_0, 'pdus'), (self.howto_frac_timing_freq_phase_synch_msg2_0, 'phase_in'))
        self.connect((self.blocks_char_to_float_0, 0), (self.qtgui_time_sink_x_0_1_0, 0))
        self.connect((self.blocks_complex_to_mag_0, 0), (self.blocks_delay_1_0_0_0, 0))
        self.connect((self.blocks_complex_to_mag_0, 0), (self.fir_filter_xxx_0_0, 0))
        self.connect((self.blocks_delay_1, 0), (self.howto_frac_timing_freq_phase_synch_msg2_0, 1))
        self.connect((self.blocks_delay_1, 0), (self.tutorial_CFO_EKF_0, 0))
        self.connect((self.blocks_delay_1_0_0_0, 0), (self.qtgui_time_sink_x_0_1_0, 1))
        self.connect((self.blocks_peak_detector2_fb_0, 0), (self.howto_first_trigger_0, 0))
        self.connect((self.blocks_vector_source_x_0_0_0, 0), (self.howto_frac_timing_freq_phase_synch_msg2_0, 3))
        self.connect((self.blocks_vector_source_x_0_0_2, 0), (self.howto_frac_timing_freq_phase_synch_msg2_0, 4))
        self.connect((self.fft_filter_xxx_0, 0), (self.blocks_complex_to_mag_0, 0))
        self.connect((self.fir_filter_xxx_0_0, 0), (self.blocks_peak_detector2_fb_0, 0))
        self.connect((self.howto_first_trigger_0, 0), (self.howto_frac_timing_freq_phase_synch_msg2_0, 0))
        self.connect((self.howto_first_trigger_0, 0), (self.howto_trig_repeat_0, 0))
        self.connect((self.howto_frac_timing_freq_phase_synch_msg2_0, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect((self.howto_frac_timing_freq_phase_synch_msg2_0, 0), (self.uhd_usrp_sink_0_0, 0))
        self.connect((self.howto_trig_repeat_0, 0), (self.blocks_char_to_float_0, 0))
        self.connect((self.howto_trig_repeat_0, 0), (self.tutorial_CFO_EKF_0, 1))
        self.connect((self.tutorial_CFO_EKF_0, 2), (self.blocks_null_sink_0, 0))
        self.connect((self.tutorial_CFO_EKF_0, 3), (self.blocks_null_sink_0_0, 0))
        self.connect((self.tutorial_CFO_EKF_0, 1), (self.blocks_null_sink_0_0_0, 0))
        self.connect((self.tutorial_CFO_EKF_0, 0), (self.blocks_null_sink_0_1, 0))
        self.connect((self.tutorial_CFO_EKF_0, 1), (self.howto_frac_timing_freq_phase_synch_msg2_0, 2))
        self.connect((self.tutorial_CFO_EKF_0, 1), (self.qtgui_time_sink_x_0_0, 1))
        self.connect((self.tutorial_CFO_EKF_0, 2), (self.qtgui_time_sink_x_0_0, 0))
        self.connect((self.uhd_usrp_source_0_0_0, 0), (self.blocks_delay_1, 0))
        self.connect((self.uhd_usrp_source_0_0_0, 0), (self.fft_filter_xxx_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "node1")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_tx_start_guard(self):
        return self.tx_start_guard

    def set_tx_start_guard(self, tx_start_guard):
        self.tx_start_guard = tx_start_guard
        self.set_my_start_gap(self.initial_zeros+self.tx_start_guard)

    def get_timing_margin2(self):
        return self.timing_margin2

    def set_timing_margin2(self, timing_margin2):
        self.timing_margin2 = timing_margin2
        self.set_actual_data_len(self.data_len-self.timing_margin2)

    def get_timing_margin(self):
        return self.timing_margin

    def set_timing_margin(self, timing_margin):
        self.timing_margin = timing_margin
        self.set_silent_period(self.timing_margin + self.data_len + self.guard_time)

    def get_initial_zeros(self):
        return self.initial_zeros

    def set_initial_zeros(self, initial_zeros):
        self.initial_zeros = initial_zeros
        self.set_my_start_gap(self.initial_zeros+self.tx_start_guard)

    def get_guard_time(self):
        return self.guard_time

    def set_guard_time(self, guard_time):
        self.guard_time = guard_time
        self.set_silent_period(self.timing_margin + self.data_len + self.guard_time)

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len
        self.set_silent_period(self.timing_margin + self.data_len + self.guard_time)
        self.set_actual_data_len(self.data_len-self.timing_margin2)

    def get_zcs(self):
        return self.zcs

    def set_zcs(self, zcs):
        self.zcs = zcs
        self.fft_filter_xxx_0.set_taps((numpy.conj(self.zcs)))

    def get_silent_period(self):
        return self.silent_period

    def set_silent_period(self, silent_period):
        self.silent_period = silent_period

    def get_second_zeros_hard_coded(self):
        return self.second_zeros_hard_coded

    def set_second_zeros_hard_coded(self, second_zeros_hard_coded):
        self.second_zeros_hard_coded = second_zeros_hard_coded

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_source_0_0_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_0_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_0_1_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_0_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate)

    def get_phs2(self):
        return self.phs2

    def set_phs2(self, phs2):
        self.phs2 = phs2

    def get_phs(self):
        return self.phs

    def set_phs(self, phs):
        self.phs = phs

    def get_phase_det_len(self):
        return self.phase_det_len

    def set_phase_det_len(self, phase_det_len):
        self.phase_det_len = phase_det_len

    def get_payload(self):
        return self.payload

    def set_payload(self, payload):
        self.payload = payload
        self.blocks_vector_source_x_0_0_2.set_data(self.payload*0.8, [])

    def get_n_train(self):
        return self.n_train

    def set_n_train(self, n_train):
        self.n_train = n_train

    def get_my_start_gap(self):
        return self.my_start_gap

    def set_my_start_gap(self, my_start_gap):
        self.my_start_gap = my_start_gap

    def get_gap(self):
        return self.gap

    def set_gap(self, gap):
        self.gap = gap

    def get_freq_tx(self):
        return self.freq_tx

    def set_freq_tx(self, freq_tx):
        self.freq_tx = freq_tx
        self.uhd_usrp_sink_0_0.set_center_freq(uhd.tune_request(self.freq_tx, 10e6), 0)

    def get_freq_rx(self):
        return self.freq_rx

    def set_freq_rx(self, freq_rx):
        self.freq_rx = freq_rx
        self.uhd_usrp_source_0_0_0.set_center_freq(uhd.tune_request(self.freq_rx, 10e6), 0)

    def get_actual_data_len(self):
        return self.actual_data_len

    def set_actual_data_len(self, actual_data_len):
        self.actual_data_len = actual_data_len


def main(top_block_cls=node1, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
