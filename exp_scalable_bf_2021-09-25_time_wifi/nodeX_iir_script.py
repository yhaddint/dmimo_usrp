#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Nodex Iir Script
# GNU Radio version: 3.7.13.5
##################################################

from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import commpy
import howto
import numpy
import numpy as np
import time


class nodeX_iir_script(gr.top_block):

    def __init__(self, freq_rx=900e6, freq_tx=900e6, hdr_const=digital.constellation_calcdist((digital.psk_2()[0]), (digital.psk_2()[1]), 2, 1).base(), indx=0, rx_gain=0.8, samp_rate=1.0e6, tx_gain=0.6):
        gr.top_block.__init__(self, "Nodex Iir Script")

        ##################################################
        # Parameters
        ##################################################
        self.freq_rx = freq_rx
        self.freq_tx = freq_tx
        self.hdr_const = hdr_const
        self.indx = indx
        self.rx_gain = rx_gain
        self.samp_rate = samp_rate
        self.tx_gain = tx_gain

        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 2
        self.eb = eb = 0.35
        self.rxmod = rxmod = digital.generic_mod(hdr_const, False, sps, True, eb, False, False)
        self.preamble = preamble = [0xac, 0xdd, 0xa4, 0xe2, 0xf2, 0x8c, 0x20, 0xfc]
        self.tx_start_guard = tx_start_guard = 0
        self.timing_margin2 = timing_margin2 = int(11000*samp_rate/1e6)
        self.timing_margin = timing_margin = int((5000)*samp_rate/1e6)
        self.payload = payload = np.concatenate((np.zeros((100,)),np.ones((200,))*int(indx==0),np.ones(200)*int(indx==1),np.ones(500) ))*0.8
        self.modulated_sync_word = modulated_sync_word = digital.modulate_vector_bc(rxmod .to_basic_block(), (preamble), ([1]))
        self.initial_zeros = initial_zeros = 400
        self.guard_time = guard_time = int(20000*samp_rate/1e6)
        self.data_bytes = data_bytes = [0x5a,0x5c,0x4a,0xdf,0xcf,0xe1,0xe2,0xb9,0xe0,0x10,0xc7,0xcd,0xd1,0x14,0x32,0xe3,0x8e,0x49,0xb0,0x8d,0xa3,0x06,0xb5,0x8e,0x3a,0x7b,0xa7,0xa9,0x95,0x22,0x42,0xd6,0xd9,0x13,0x6d,0x91,0xfe,0xca,0x03,0x63,0x33,0xd2,0x89,0xa5,0x34,0x81,0x87,0x34,0xc6,0x78,0xce,0xa7,0x57,0xe2,0x3e,0x69,0x8b,0xe6,0x1b,0x9b,0x6e,0xb8,0x52,0xd3,0xa3,0xea,0x6e,0x4a,0xf3,0x38,0xa5,0xee,0x61,0x2f,0xdc,0x56,0x40,0x18,0xe5,0x89,0xad,0xe4,0x17,0x5d,0x97,0x3f,0x5c,0x59,0x8f,0x63,0xe8,0xd7,0x16,0x11,0xb3,0x11,0x6c,0xb3,0xc4,0xbf,0x12,0x71,0x4b,0xb4,0x63,0xb6,0x49,0xb4,0x88,0xf9,0x7b,0xce,0xbd,0x13,0xe6,0xcc,0x4c,0x54,0x4f,0x4d,0xba,0xa3,0x6d,0xa9,0xc3,0x8b,0x3c,0xe4,0x66,0x46,0x05,0x26,0xab,0x64,0xe1,0x46,0x43,0xfb,0x84,0xf6,0x0d,0xb7,0x74,0x00,0xfb,0xf0,0x23,0x1b,0xbf,0x0b,0x25,0x41,0xed,0x9f,0xfe,0x77,0x0c,0xe0,0xda,0x71,0x30,0xf6,0xd5,0x66,0x0c,0x62,0x40,0x2f,0xd5,0xc1,0x01,0x5f,0xfd,0x13,0xfa,0x40,0xc0,0xb9,0xec,0x97,0x35,0xfd,0x9b,0x4b,0xf4,0x3e,0xf0,0x3b,0x1e,0xdc,0x5b,0x7b,0x46,0x7b,0x7b,0x05,0x83,0xea,0x13,0xcd,0xea,0xdb,0x18,0xd1,0x85,0xfa,0x56,0x75,0x51,0x9e,0xee,0xc3,0x7b,0xf8,0x88,0xa4,0x3a,0x23,0x84,0x63,0x3c,0xbe,0x8a,0x29,0xdc,0xa2,0xf2,0x8f,0x07,0x0b,0x96,0x0f,0xac,0xd3,0xb7,0x0f,0x37,0x33,0xbb,0xec,0xe0,0xdd,0x9d,0x16,0xdc,0xa3,0xa1,0x8a,0x27,0x64,0x90,0x45,0xea,0x47,0xd5,0x91,0x00,0x6c,0xe4,0xd9,0x20,0xb9,0xfd,0x6b,0x63,0x3d,0x17,0xd5,0x9e,0x31,0xc5,0x29,0x67,0x11,0x31,0xcc,0xd6,0x2d,0xca,0xd6,0x92,0xe8,0x0b,0x2d,0x33,0xa2,0x70,0x05,0x99,0x2e,0x65,0x8c,0xc9,0xc4,0x79,0x01,0x6b,0xaf,0x74,0x3d,0xd7,0xb4,0xf6,0xc6,0x96,0xa8,0x15,0x1d,0x9f,0x0f,0xe0,0xc7,0x27,0x1b,0x42,0x2c,0x27,0xb2,0x72,0x49,0xe3,0x65,0x52,0xfa,0x47,0xc6,0x74,0x64,0xba,0xfd,0x1d,0xa0,0xe2,0x21,0xb9,0x4c,0xc8,0xce,0x65,0x7b,0x98,0x12,0x70,0xd4,0xb8,0x56,0x8a,0xd4,0x6e,0x81,0xb5,0x0f,0xa9,0x28,0xa7,0x4c,0x53,0xb8,0x62,0x4b,0xfd,0xe7,0xe7,0x99,0xb7,0x93,0x06,0xa6,0x56,0xf6,0x8a,0x9d,0xbc,0xbb,0x7e,0xa0,0xe9,0x7d,0xed,0xb0,0x22,0x5b,0xf8,0xa2,0x9e,0xec,0xc6,0x76,0x01,0x2e,0x3d,0x0e,0x1a,0x4c,0x28,0xc5,0xe0,0x9c,0x0f,0xa1,0xbc,0x7b,0x5c,0xbb,0xc1,0xca,0xc2,0x3f,0x4c,0x47,0xd2,0x75,0x42,0x71,0x2e,0x00,0x48,0xd6,0x6c,0x8b,0x18,0x8e,0x81,0x51,0xe0,0xfa,0x57,0xd1,0xc1,0x77,0x1b,0xd1,0x19,0x46,0x7c,0xff,0x4b,0x30,0x5b,0xc2,0x0e,0x2d,0xbf,0x2b,0x7b,0x28,0x39,0x7d,0x3a,0xea,0xad,0xd1,0x8d,0x9d,0x9e,0xfb,0x8d,0xd2,0x11,0x0f,0xf4,0x29,0x2c,0xcd,0xd7,0x0c,0xf0,0x72,0x7f,0x4d,0x58,0xc2,0x2b,0x50,0x21,0x64,0x9d,0xbb,0x10,0x97,0xff,0xdc,0x98,0x3d,0x64,0x1e,0x73,0x38,0xe9,0x7b,0xef,0x01,0x1c,0x4a,0x38,0x9a,0xb3,0xe8,0x12,0x2e,0xf0,0x9f,0xa1,0x28,0x7a,0x4a,0xf1,0xd3,0xcd,0x87,0xe9,0x7b,0x3f,0x42,0xdd,0x18,0x8a,0xc6,0x2b,0xc6,0xd1,0xac,0xba,0xc9,0xa2,0x6a,0x8c,0x76,0x38,0x6b,0xb9,0x48,0xe3,0xe8,0x5c,0x92,0x45,0xf8,0xb7,0xd3,0x67,0x13,0x8e,0xe8,0xda,0xf0,0x6b,0x69,0x74,0x75,0x58,0xbc,0x3c,0xa6,0xb3,0x22,0x16,0x6b,0x57,0xc5,0x2c,0xde,0x05,0xe9,0x2c,0xbd,0xf4,0x40,0xd3,0xa8,0x14,0x74,0x34,0x08,0x70,0xfe,0x9e,0x18,0x55,0x1f,0xf4,0x25,0xa1,0x93,0x3a,0x11,0x50,0xd8,0xd0,0xb6,0x22,0x2a,0xaf,0xff,0xd4,0x67,0xb0,0xc3,0x5d,0xe7,0x34,0x2f,0x22,0x6b,0x98,0xc9,0x3f,0xa9,0xc0,0x6a,0x60,0xff,0x6a,0x51,0xe2,0x7e,0x39,0xb0,0xcd,0xfd,0xae,0x8b,0x62,0xef,0xcf ]
        self.actual_data_len = actual_data_len = 1000
        self.zcs = zcs = commpy.sequences.zcsequence(25,63)
        self.silent_period = silent_period = timing_margin + actual_data_len+timing_margin2 + guard_time
        self.second_zeros_hard_coded = second_zeros_hard_coded = 0
        self.pld_gap = pld_gap = 100
        self.phase_feedback_len = phase_feedback_len = 100
        self.phase_feedback_delay = phase_feedback_delay = 4000
        self.phase_det_len = phase_det_len = 200
        self.payload_size = payload_size = payload.size
        self.n_train = n_train = 10
        self.my_start_gap = my_start_gap = initial_zeros+tx_start_guard
        self.modulated_data = modulated_data = digital.modulate_vector_bc(rxmod .to_basic_block(), (data_bytes), ([1]))
        self.eb_0 = eb_0 = len(modulated_sync_word)
        self.data_len = data_len = actual_data_len+timing_margin2

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0_0_0 = uhd.usrp_source(
        	",".join(('', "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0_0_0.set_clock_source('internal', 0)
        self.uhd_usrp_source_0_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0_0_0.set_center_freq(uhd.tune_request(freq_rx, 10e6), 0)
        self.uhd_usrp_source_0_0_0.set_normalized_gain(rx_gain, 0)
        self.uhd_usrp_source_0_0_0.set_antenna('RX2', 0)
        self.uhd_usrp_source_0_0_0.set_auto_dc_offset(True, 0)
        self.uhd_usrp_source_0_0_0.set_auto_iq_balance(True, 0)
        self.uhd_usrp_sink_0_0 = uhd.usrp_sink(
        	",".join(('', "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0_0.set_clock_source('internal', 0)
        self.uhd_usrp_sink_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)
        self.uhd_usrp_sink_0_0.set_center_freq(uhd.tune_request(freq_tx, 10e6), 0)
        self.uhd_usrp_sink_0_0.set_normalized_gain(tx_gain, 0)
        self.uhd_usrp_sink_0_0.set_antenna('TX/RX', 0)
        self.howto_trig_repeat_0 = howto.trig_repeat(n_train, 0, silent_period)
        self.howto_freq_corr_0 = howto.freq_corr(samp_rate)
        self.howto_frac_timing_freq_phase_synch_msg3_0 = howto.frac_timing_freq_phase_synch_msg3(timing_margin+my_start_gap+indx*phase_det_len, phase_det_len, 0.95, timing_margin2-indx*phase_det_len, actual_data_len, samp_rate, 1, 16, True)
        self.howto_first_trigger_0 = howto.first_trigger(silent_period)
        self.howto_det_diff_phase_trig_msg_0 = howto.det_diff_phase_trig_msg(timing_margin+phase_feedback_delay, phase_feedback_len, phase_feedback_len*(indx+1))
        self.howto_cfo_filter_0 = howto.cfo_filter(samp_rate, 63*n_train, 63, 0, ([1.0000  ,  -1.911197067426073  ,  0.914975834801434]), ([0.000944691843840,   0.001889383687680,  0.000944691843840]))
        self.fir_filter_xxx_0_0 = filter.fir_filter_fff(1, (([1.0/10.0]+[0.0]*62)*n_train))
        self.fir_filter_xxx_0_0.declare_sample_delay(0)
        self.fft_filter_xxx_0 = filter.fft_filter_ccc(1, (numpy.conj(zcs)), 1)
        self.fft_filter_xxx_0.declare_sample_delay(0)
        self.blocks_vector_source_x_0_0_2 = blocks.vector_source_c(payload*0.8, True, 1, [])
        self.blocks_vector_source_x_0_0_0 = blocks.vector_source_f((0,), True, 1, [])
        self.blocks_peak_detector2_fb_0 = blocks.peak_detector2_fb(2, int(63*n_train*2.0), 0.05)
        self.blocks_null_sink_0_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_delay_1 = blocks.delay(gr.sizeof_gr_complex*1, (n_train)*63-2)
        self.blocks_complex_to_mag_0 = blocks.complex_to_mag(1)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.howto_det_diff_phase_trig_msg_0, 'phase_out'), (self.howto_frac_timing_freq_phase_synch_msg3_0, 'phase_in'))
        self.connect((self.blocks_complex_to_mag_0, 0), (self.fir_filter_xxx_0_0, 0))
        self.connect((self.blocks_delay_1, 0), (self.howto_cfo_filter_0, 0))
        self.connect((self.blocks_delay_1, 0), (self.howto_frac_timing_freq_phase_synch_msg3_0, 1))
        self.connect((self.blocks_delay_1, 0), (self.howto_freq_corr_0, 0))
        self.connect((self.blocks_peak_detector2_fb_0, 0), (self.howto_first_trigger_0, 0))
        self.connect((self.blocks_vector_source_x_0_0_0, 0), (self.howto_frac_timing_freq_phase_synch_msg3_0, 3))
        self.connect((self.blocks_vector_source_x_0_0_2, 0), (self.howto_frac_timing_freq_phase_synch_msg3_0, 4))
        self.connect((self.fft_filter_xxx_0, 0), (self.blocks_complex_to_mag_0, 0))
        self.connect((self.fir_filter_xxx_0_0, 0), (self.blocks_peak_detector2_fb_0, 0))
        self.connect((self.howto_cfo_filter_0, 1), (self.blocks_null_sink_0, 0))
        self.connect((self.howto_cfo_filter_0, 2), (self.blocks_null_sink_0_0, 0))
        self.connect((self.howto_cfo_filter_0, 0), (self.blocks_null_sink_0_0_0, 0))
        self.connect((self.howto_cfo_filter_0, 0), (self.howto_frac_timing_freq_phase_synch_msg3_0, 2))
        self.connect((self.howto_cfo_filter_0, 0), (self.howto_freq_corr_0, 1))
        self.connect((self.howto_first_trigger_0, 0), (self.howto_det_diff_phase_trig_msg_0, 0))
        self.connect((self.howto_first_trigger_0, 0), (self.howto_frac_timing_freq_phase_synch_msg3_0, 0))
        self.connect((self.howto_first_trigger_0, 0), (self.howto_trig_repeat_0, 0))
        self.connect((self.howto_frac_timing_freq_phase_synch_msg3_0, 0), (self.uhd_usrp_sink_0_0, 0))
        self.connect((self.howto_freq_corr_0, 0), (self.howto_det_diff_phase_trig_msg_0, 1))
        self.connect((self.howto_trig_repeat_0, 0), (self.howto_cfo_filter_0, 1))
        self.connect((self.uhd_usrp_source_0_0_0, 0), (self.blocks_delay_1, 0))
        self.connect((self.uhd_usrp_source_0_0_0, 0), (self.fft_filter_xxx_0, 0))

    def get_freq_rx(self):
        return self.freq_rx

    def set_freq_rx(self, freq_rx):
        self.freq_rx = freq_rx
        self.uhd_usrp_source_0_0_0.set_center_freq(uhd.tune_request(self.freq_rx, 10e6), 0)

    def get_freq_tx(self):
        return self.freq_tx

    def set_freq_tx(self, freq_tx):
        self.freq_tx = freq_tx
        self.uhd_usrp_sink_0_0.set_center_freq(uhd.tune_request(self.freq_tx, 10e6), 0)

    def get_hdr_const(self):
        return self.hdr_const

    def set_hdr_const(self, hdr_const):
        self.hdr_const = hdr_const
        self.set_rxmod(digital.generic_mod(self.hdr_const, False, self.sps, True, self.eb, False, False))

    def get_indx(self):
        return self.indx

    def set_indx(self, indx):
        self.indx = indx
        self.set_payload(np.concatenate((np.zeros((100,)),np.ones((200,))*int(self.indx==0),np.ones(200)*int(self.indx==1),np.ones(500) ))*0.8)

    def get_rx_gain(self):
        return self.rx_gain

    def set_rx_gain(self, rx_gain):
        self.rx_gain = rx_gain
        self.uhd_usrp_source_0_0_0.set_normalized_gain(self.rx_gain, 0)


    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_timing_margin2(int(11000*self.samp_rate/1e6))
        self.set_timing_margin(int((5000)*self.samp_rate/1e6))
        self.uhd_usrp_source_0_0_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_0_0.set_samp_rate(self.samp_rate)
        self.set_guard_time(int(20000*self.samp_rate/1e6))

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self.uhd_usrp_sink_0_0.set_normalized_gain(self.tx_gain, 0)


    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.set_rxmod(digital.generic_mod(self.hdr_const, False, self.sps, True, self.eb, False, False))

    def get_eb(self):
        return self.eb

    def set_eb(self, eb):
        self.eb = eb
        self.set_rxmod(digital.generic_mod(self.hdr_const, False, self.sps, True, self.eb, False, False))

    def get_rxmod(self):
        return self.rxmod

    def set_rxmod(self, rxmod):
        self.rxmod = rxmod

    def get_preamble(self):
        return self.preamble

    def set_preamble(self, preamble):
        self.preamble = preamble

    def get_tx_start_guard(self):
        return self.tx_start_guard

    def set_tx_start_guard(self, tx_start_guard):
        self.tx_start_guard = tx_start_guard
        self.set_my_start_gap(self.initial_zeros+self.tx_start_guard)

    def get_timing_margin2(self):
        return self.timing_margin2

    def set_timing_margin2(self, timing_margin2):
        self.timing_margin2 = timing_margin2
        self.set_silent_period(self.timing_margin + self.actual_data_len+self.timing_margin2 + self.guard_time)
        self.set_data_len(self.actual_data_len+self.timing_margin2)

    def get_timing_margin(self):
        return self.timing_margin

    def set_timing_margin(self, timing_margin):
        self.timing_margin = timing_margin
        self.set_silent_period(self.timing_margin + self.actual_data_len+self.timing_margin2 + self.guard_time)

    def get_payload(self):
        return self.payload

    def set_payload(self, payload):
        self.payload = payload
        self.blocks_vector_source_x_0_0_2.set_data(self.payload*0.8, [])

    def get_modulated_sync_word(self):
        return self.modulated_sync_word

    def set_modulated_sync_word(self, modulated_sync_word):
        self.modulated_sync_word = modulated_sync_word
        self.set_eb_0(len(self.modulated_sync_word))

    def get_initial_zeros(self):
        return self.initial_zeros

    def set_initial_zeros(self, initial_zeros):
        self.initial_zeros = initial_zeros
        self.set_my_start_gap(self.initial_zeros+self.tx_start_guard)

    def get_guard_time(self):
        return self.guard_time

    def set_guard_time(self, guard_time):
        self.guard_time = guard_time
        self.set_silent_period(self.timing_margin + self.actual_data_len+self.timing_margin2 + self.guard_time)

    def get_data_bytes(self):
        return self.data_bytes

    def set_data_bytes(self, data_bytes):
        self.data_bytes = data_bytes

    def get_actual_data_len(self):
        return self.actual_data_len

    def set_actual_data_len(self, actual_data_len):
        self.actual_data_len = actual_data_len
        self.set_silent_period(self.timing_margin + self.actual_data_len+self.timing_margin2 + self.guard_time)
        self.set_data_len(self.actual_data_len+self.timing_margin2)

    def get_zcs(self):
        return self.zcs

    def set_zcs(self, zcs):
        self.zcs = zcs
        self.fft_filter_xxx_0.set_taps((numpy.conj(self.zcs)))

    def get_silent_period(self):
        return self.silent_period

    def set_silent_period(self, silent_period):
        self.silent_period = silent_period

    def get_second_zeros_hard_coded(self):
        return self.second_zeros_hard_coded

    def set_second_zeros_hard_coded(self, second_zeros_hard_coded):
        self.second_zeros_hard_coded = second_zeros_hard_coded

    def get_pld_gap(self):
        return self.pld_gap

    def set_pld_gap(self, pld_gap):
        self.pld_gap = pld_gap

    def get_phase_feedback_len(self):
        return self.phase_feedback_len

    def set_phase_feedback_len(self, phase_feedback_len):
        self.phase_feedback_len = phase_feedback_len

    def get_phase_feedback_delay(self):
        return self.phase_feedback_delay

    def set_phase_feedback_delay(self, phase_feedback_delay):
        self.phase_feedback_delay = phase_feedback_delay

    def get_phase_det_len(self):
        return self.phase_det_len

    def set_phase_det_len(self, phase_det_len):
        self.phase_det_len = phase_det_len

    def get_payload_size(self):
        return self.payload_size

    def set_payload_size(self, payload_size):
        self.payload_size = payload_size

    def get_n_train(self):
        return self.n_train

    def set_n_train(self, n_train):
        self.n_train = n_train
        self.fir_filter_xxx_0_0.set_taps((([1.0/10.0]+[0.0]*62)*self.n_train))
        self.blocks_peak_detector2_fb_0.set_look_ahead(int(63*self.n_train*2.0))
        self.blocks_delay_1.set_dly((self.n_train)*63-2)

    def get_my_start_gap(self):
        return self.my_start_gap

    def set_my_start_gap(self, my_start_gap):
        self.my_start_gap = my_start_gap

    def get_modulated_data(self):
        return self.modulated_data

    def set_modulated_data(self, modulated_data):
        self.modulated_data = modulated_data

    def get_eb_0(self):
        return self.eb_0

    def set_eb_0(self, eb_0):
        self.eb_0 = eb_0

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "", "--freq-rx", dest="freq_rx", type="eng_float", default=eng_notation.num_to_str(900e6),
        help="Set freq_tx [default=%default]")
    parser.add_option(
        "", "--freq-tx", dest="freq_tx", type="eng_float", default=eng_notation.num_to_str(900e6),
        help="Set freq_tx [default=%default]")
    parser.add_option(
        "", "--indx", dest="indx", type="intx", default=0,
        help="Set indx [default=%default]")
    parser.add_option(
        "", "--rx-gain", dest="rx_gain", type="eng_float", default=eng_notation.num_to_str(0.8),
        help="Set rx_gain [default=%default]")
    parser.add_option(
        "", "--samp-rate", dest="samp_rate", type="eng_float", default=eng_notation.num_to_str(1.0e6),
        help="Set samp_rate [default=%default]")
    parser.add_option(
        "", "--tx-gain", dest="tx_gain", type="eng_float", default=eng_notation.num_to_str(0.6),
        help="Set tx_gain [default=%default]")
    return parser


def main(top_block_cls=nodeX_iir_script, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    tb = top_block_cls(freq_rx=options.freq_rx, freq_tx=options.freq_tx, indx=options.indx, rx_gain=options.rx_gain, samp_rate=options.samp_rate, tx_gain=options.tx_gain)
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
