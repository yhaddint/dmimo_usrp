#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Transmitter Prot Script
# Generated: Tue Sep 17 14:56:00 2019
##################################################

from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import commpy.sequences
import numpy
import numpy as np
import time


class transmitter_prot_script(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Transmitter Prot Script")

        ##################################################
        # Variables
        ##################################################
        self.timing_margin = timing_margin = 60000
        self.guard_time = guard_time = 100000
        self.data_len = data_len = 50000
        self.zcs = zcs = commpy.sequences.zcsequence(25,63)
        self.samp_rate = samp_rate = 1e6
        self.pkt_len = pkt_len = timing_margin + data_len + guard_time
        self.n_train = n_train = 10
        self.gap = gap = 528000
        self.freq = freq = 915e6

        self.const = const = digital.constellation_qpsk().base()


        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_sink_0_0 = uhd.usrp_sink(
        	",".join(('serial = 317039B', "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0_0.set_clock_source('external', 0)
        self.uhd_usrp_sink_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0_0.set_center_freq(uhd.tune_request(freq, 10e6), 0)
        self.uhd_usrp_sink_0_0.set_normalized_gain(0.8, 0)
        self.uhd_usrp_sink_0_0.set_antenna('TX/RX', 0)
        self.blocks_vector_source_x_0_1_0_0 = blocks.vector_source_c((0.0,), True, 1, [])
        self.blocks_vector_source_x_0_0_0_0 = blocks.vector_source_c(zcs, True, 1, [])
        self.blocks_vector_source_x_0_0 = blocks.vector_source_c((0, ), True, 1, [])
        self.blocks_vector_source_x_0 = blocks.vector_source_c((0, ), True, 1, [])
        self.blocks_stream_mux_0 = blocks.stream_mux(gr.sizeof_gr_complex*1, (zcs.size*n_train, timing_margin - zcs.size*n_train, data_len,guard_time+gap))



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_stream_mux_0, 0), (self.uhd_usrp_sink_0_0, 0))
        self.connect((self.blocks_vector_source_x_0, 0), (self.blocks_stream_mux_0, 1))
        self.connect((self.blocks_vector_source_x_0_0, 0), (self.blocks_stream_mux_0, 3))
        self.connect((self.blocks_vector_source_x_0_0_0_0, 0), (self.blocks_stream_mux_0, 0))
        self.connect((self.blocks_vector_source_x_0_1_0_0, 0), (self.blocks_stream_mux_0, 2))

    def get_timing_margin(self):
        return self.timing_margin

    def set_timing_margin(self, timing_margin):
        self.timing_margin = timing_margin
        self.set_pkt_len(self.timing_margin + self.data_len + self.guard_time)

    def get_guard_time(self):
        return self.guard_time

    def set_guard_time(self, guard_time):
        self.guard_time = guard_time
        self.set_pkt_len(self.timing_margin + self.data_len + self.guard_time)

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len
        self.set_pkt_len(self.timing_margin + self.data_len + self.guard_time)

    def get_zcs(self):
        return self.zcs

    def set_zcs(self, zcs):
        self.zcs = zcs
        self.blocks_vector_source_x_0_0_0_0.set_data(self.zcs, [])

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_0_0.set_samp_rate(self.samp_rate)

    def get_pkt_len(self):
        return self.pkt_len

    def set_pkt_len(self, pkt_len):
        self.pkt_len = pkt_len

    def get_n_train(self):
        return self.n_train

    def set_n_train(self, n_train):
        self.n_train = n_train

    def get_gap(self):
        return self.gap

    def set_gap(self, gap):
        self.gap = gap

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.uhd_usrp_sink_0_0.set_center_freq(uhd.tune_request(self.freq, 10e6), 0)

    def get_const(self):
        return self.const

    def set_const(self, const):
        self.const = const


def main(top_block_cls=transmitter_prot_script, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
