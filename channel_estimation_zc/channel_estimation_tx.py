#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Channel Estimation Tx
# GNU Radio version: 3.7.13.5
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import commpy
import numpy as np
import time


class channel_estimation_tx(gr.top_block):

    def __init__(self, freq=900e6, tx_gain=0.65):
        gr.top_block.__init__(self, "Channel Estimation Tx")

        ##################################################
        # Parameters
        ##################################################
        self.freq = freq
        self.tx_gain = tx_gain

        ##################################################
        # Variables
        ##################################################
        self.zcs = zcs = commpy.sequences.zcsequence(25,63)
        self.samp_rate = samp_rate = 1e6
        self.corr_code = corr_code = zcs*0.8

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join(("", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_center_freq(uhd.tune_request(freq,10e6), 0)
        self.uhd_usrp_sink_0.set_normalized_gain(tx_gain, 0)
        self.blocks_vector_source_x_0 = blocks.vector_source_c(corr_code, True, 1, [])



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_vector_source_x_0, 0), (self.uhd_usrp_sink_0, 0))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.uhd_usrp_sink_0.set_center_freq(uhd.tune_request(self.freq,10e6), 0)

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self.uhd_usrp_sink_0.set_normalized_gain(self.tx_gain, 0)


    def get_zcs(self):
        return self.zcs

    def set_zcs(self, zcs):
        self.zcs = zcs
        self.set_corr_code(self.zcs*0.8)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)

    def get_corr_code(self):
        return self.corr_code

    def set_corr_code(self, corr_code):
        self.corr_code = corr_code
        self.blocks_vector_source_x_0.set_data(self.corr_code, [])


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "", "--freq", dest="freq", type="eng_float", default=eng_notation.num_to_str(900e6),
        help="Set freq [default=%default]")
    parser.add_option(
        "", "--tx-gain", dest="tx_gain", type="eng_float", default=eng_notation.num_to_str(0.65),
        help="Set tx_gain [default=%default]")
    return parser


def main(top_block_cls=channel_estimation_tx, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(freq=options.freq, tx_gain=options.tx_gain)
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
