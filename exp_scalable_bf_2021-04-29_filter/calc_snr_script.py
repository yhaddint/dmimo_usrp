#!/bin/env python


#(same_pkt_len, mod,tx_gain, tx_amplitude)

import paramiko
import subprocess
import time
import itertools
import numpy as np
import sys



freq=900e6

n_pkts=1000
skp_pkts=200

pkt_duration = 182e3*1e-6*1.05;

#slave_ips =['192.168.1.201']

slave_ips =['10.42.0.63','10.42.0.211']

exp_dir='exp_scalable_bf_2021-04-29_filter/'

log_dir='/home/samer/Documents/distributed_mimo/dmimo_usrp/temp/'

slave_dir='/home/odroid/Documents/distributed_mimo/dmimo_usrp/'+exp_dir
master_file='master.py'

slave_file_ekf='nodeX_ekf_script.py'
slave_file_iir='nodeX_iir_script.py'
slave_file_oneshot='nodeX_oneshot_script.py'


master_dir='/home/samer/Documents/distributed_mimo/dmimo_usrp/'+exp_dir

def get_file_name(type_ekf,gain):
    return "{}data_bf_{}_0p{}.dat".format(log_dir,type_ekf,int(round(gain*100)))

def run_slave(slave_id,slave_ip,ekf,gain):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    k = paramiko.RSAKey.from_private_key_file("/home/samer/.ssh/id_rsa")
    client.connect(slave_ip, username='odroid', pkey=k)
    print "Connected to slave"
    
    if ekf==1:
        slave_file=slave_file_ekf
    elif ekf==2:
        slave_file=slave_file_iir
    else:
        slave_file=slave_file_oneshot
    slave_command="bash -lc 'source ~/prefix/setup_env.sh; taskset 0xF0 python {}{} --freq-tx {} --freq-rx {} --tx-gain {} --indx {}'".format(
        slave_dir,slave_file, freq,freq,gain, slave_id)
    print slave_command
    pipes= client.exec_command(slave_command)
    # stdin, stdout, stderr = client.exec_command(slave_command)
    return client,pipes

def close_slave(c):
    client,pipes=c
    ssh_stdin, ssh_stdout, ssh_stderr =pipes
    ssh_stdin.write('\n')
    ssh_stdin.flush()
    output = ssh_stdout.read()

    print "Slave done. Printing Output"
    for line in ssh_stdout:
        print(line.strip('\n'))
    for line in ssh_stderr:
        print(line.strip('\n'))
    client.close()
    

def start_master(ekf,gain,trig_level):
    print "Starting master"
    if ekf==1:
        type_ekf='ekf'
    elif ekf==2:
        type_ekf='iir'
    else:
        type_ekf='oneshot'

    fname=get_file_name(type_ekf,gain)
    tx_command=[master_dir+master_file,'--tx-gain',str(gain),'--freq',str(freq),
    '--n-pkts',str(n_pkts),'--skp-pkts',str(skp_pkts),'--fname',fname,'--trig-level',str(trig_level)]

    print " ".join(tx_command)
    p=subprocess.Popen(tx_command,stdout=subprocess.PIPE,stderr=subprocess.PIPE)   
    time.sleep(1)
    print "Started Master"
    return p
    
def close_master(p):
    p.terminate()
    out, err = p.communicate()
    print out
    print err
    print "Close transmitter"
    
def run_exp(usrp_id,same_pkt_len,mod,tx_gain,tx_amp,freq,rx_oversamp_tx,sps,xlat):
    for el in itertools.product(same_pkt_len,mod,tx_gain,tx_amp,freq,rx_oversamp_tx,sps):
        print(el)
        arg=el+(xlat,)
        p=start_transmitter(*arg)
        arg=(usrp_id,)+el+(xlat,)
        run_receiver(*arg)
        close_transmitter(p)



    
if __name__=="__main__":

    
    # trig_level_list=[1,    1,   1,   1,   1,  0.8,  0.5, 0.3, 0.2]
    # gain_list=      [0.65,0.6,0.55,0.5,0.45,0.4,0.35,0.30,0.25]

    trig_level_list=[1   ,   1,   1,   1,    1,   1,   1, 0.8, 0.8, 0.8, 0.8,  0.5 , 0.5, 0.4, 0.35,  0.3,  0.3, 0.25,0.20]
    gain_list=      [0.65,0.625,0.6,0.575,0.55,0.525, 0.5,0.475,0.45,0.425,0.4,0.375,0.35,0.325,0.30,0.275,0.25,0.225,0.2]
    ekf_list=[2]

    trig_level_list.reverse()
    gain_list.reverse()

    first_run_delay=5


    total_duration = first_run_delay+(pkt_duration*(n_pkts+skp_pkts)+5)*len(gain_list)*len(ekf_list)

    print "Total duration {} mintues".format(total_duration/60 )
    time.sleep(1)
    # 1/0

    for gain,trig_level in zip(gain_list,trig_level_list):
        for ekf in ekf_list:
            c=[]
            for slave_id,slave_ip in enumerate(slave_ips):
                ci = run_slave(slave_id,slave_ip,ekf,gain)
                c.append(ci)
            time.sleep(5+first_run_delay)
            p=start_master(ekf,gain,trig_level)

            dur = pkt_duration*(n_pkts+skp_pkts)+5+first_run_delay
            time.sleep(dur)
            


            close_master(p)
            for ci in c:
                close_slave(ci)

            first_run_delay=0 
    
       


