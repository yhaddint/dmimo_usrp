#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Rx Gui
# GNU Radio version: 3.7.13.5
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import commpy
import numpy
import numpy as np
import sip
import sys
import time
from gnuradio import qtgui


class rx_gui(gr.top_block, Qt.QWidget):

    def __init__(self, freq_rx=900e6, freq_tx=900e6, rx_gain=0.5):
        gr.top_block.__init__(self, "Rx Gui")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Rx Gui")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "rx_gui")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Parameters
        ##################################################
        self.freq_rx = freq_rx
        self.freq_tx = freq_tx
        self.rx_gain = rx_gain

        ##################################################
        # Variables
        ##################################################
        self.zcs = zcs = commpy.sequences.zcsequence(25,63)
        self.samp_rate = samp_rate = 1e6

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0_0_0 = uhd.usrp_source(
        	",".join(('serial = 317039B', "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0_0_0.set_clock_source('internal', 0)
        self.uhd_usrp_source_0_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0_0_0.set_center_freq(uhd.tune_request(freq_rx, 10e6), 0)
        self.uhd_usrp_source_0_0_0.set_normalized_gain(rx_gain, 0)
        self.uhd_usrp_source_0_0_0.set_antenna('RX2', 0)
        self.uhd_usrp_source_0_0_0.set_auto_dc_offset(True, 0)
        self.uhd_usrp_source_0_0_0.set_auto_iq_balance(True, 0)
        self.qtgui_time_sink_x_0_1_2_1 = qtgui.time_sink_c(
        	200000, #size
        	samp_rate, #samp_rate
        	"", #name
        	2 #number of inputs
        )
        self.qtgui_time_sink_x_0_1_2_1.set_update_time(0.10)
        self.qtgui_time_sink_x_0_1_2_1.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0_1_2_1.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_1_2_1.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_1_2_1.set_trigger_mode(qtgui.TRIG_MODE_NORM, qtgui.TRIG_SLOPE_POS, 0.1, 2e-3, 0, "")
        self.qtgui_time_sink_x_0_1_2_1.enable_autoscale(False)
        self.qtgui_time_sink_x_0_1_2_1.enable_grid(True)
        self.qtgui_time_sink_x_0_1_2_1.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_1_2_1.enable_control_panel(True)
        self.qtgui_time_sink_x_0_1_2_1.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_1_2_1.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(4):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_0_1_2_1.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0_1_2_1.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0_1_2_1.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_1_2_1.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_1_2_1_win = sip.wrapinstance(self.qtgui_time_sink_x_0_1_2_1.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_1_2_1_win)
        self.blocks_float_to_complex_0_0 = blocks.float_to_complex(1)
        self.blocks_complex_to_mag_0_0_0 = blocks.complex_to_mag(1)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_complex_to_mag_0_0_0, 0), (self.blocks_float_to_complex_0_0, 0))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.qtgui_time_sink_x_0_1_2_1, 0))
        self.connect((self.uhd_usrp_source_0_0_0, 0), (self.blocks_complex_to_mag_0_0_0, 0))
        self.connect((self.uhd_usrp_source_0_0_0, 0), (self.qtgui_time_sink_x_0_1_2_1, 1))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "rx_gui")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_freq_rx(self):
        return self.freq_rx

    def set_freq_rx(self, freq_rx):
        self.freq_rx = freq_rx
        self.uhd_usrp_source_0_0_0.set_center_freq(uhd.tune_request(self.freq_rx, 10e6), 0)

    def get_freq_tx(self):
        return self.freq_tx

    def set_freq_tx(self, freq_tx):
        self.freq_tx = freq_tx

    def get_rx_gain(self):
        return self.rx_gain

    def set_rx_gain(self, rx_gain):
        self.rx_gain = rx_gain
        self.uhd_usrp_source_0_0_0.set_normalized_gain(self.rx_gain, 0)


    def get_zcs(self):
        return self.zcs

    def set_zcs(self, zcs):
        self.zcs = zcs

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_source_0_0_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_0_1_2_1.set_samp_rate(self.samp_rate)


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "", "--freq-rx", dest="freq_rx", type="eng_float", default=eng_notation.num_to_str(900e6),
        help="Set freq_tx [default=%default]")
    parser.add_option(
        "", "--freq-tx", dest="freq_tx", type="eng_float", default=eng_notation.num_to_str(900e6),
        help="Set freq_tx [default=%default]")
    parser.add_option(
        "", "--rx-gain", dest="rx_gain", type="eng_float", default=eng_notation.num_to_str(0.5),
        help="Set rx_gain [default=%default]")
    return parser


def main(top_block_cls=rx_gui, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(freq_rx=options.freq_rx, freq_tx=options.freq_tx, rx_gain=options.rx_gain)
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
