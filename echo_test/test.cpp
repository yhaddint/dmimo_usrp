#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <boost/circular_buffer.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/thread_priority.hpp>
#include <complex>

using boost::format;
using namespace std;

namespace po = boost::program_options;

uhd::time_spec_t get_packet_time(uhd::time_spec_t first_sample_time,
                                 size_t sample_index,
                                 double rate)
{
    uhd::time_spec_t ret(first_sample_time.get_real_secs() + sample_index / rate);
    return ret;
}

vector<complex<float> > zcseq = {
{1.000000,0.000000}, {-0.992115,0.125333}, {-0.929776,0.368125},
{0.728969,-0.684547}, {0.309017,-0.951057}, {0.309017,0.951057},
{0.876307,0.481754}, {-0.929776,0.368125}, {-0.187381,0.982287},
{-0.809017,-0.587785}, {-0.809017,0.587785}, {-0.425779,-0.904827},
{-0.929776,0.368125}, {-0.425779,-0.904827}, {-0.809017,0.587785},
{-0.809017,-0.587785}, {-0.187381,0.982287}, {-0.929776,0.368125},
{0.876307,0.481754}, {0.309017,0.951057}, {0.309017,-0.951057},
{0.728969,-0.684547}, {-0.929776,0.368125}, {-0.992115,0.125333},
{1.000000,-0.000000}
};

boost::circular_buffer<complex<float> > corrbuf(63);

struct TimestampedBuffer
{
    bool has_time;
    boost::circular_buffer<complex<float> > buffer;
    double first_sample_time;
    double sample_rate;

    TimestampedBuffer(size_t size, double sample_rate) :
        buffer(size), has_time(false), sample_rate(sample_rate),
        first_sample_time(0)
    {

    }

    void push_samples(const vector<complex<float> >& buf, const uhd::time_spec_t& fs_ts)
    {
        double new_buf_time = fs_ts.get_real_secs();
        double front_of_buffer_time = first_sample_time +
                                      ((double)buffer.size()) / sample_rate;

        assert(front_of_buffer_time < new_buf_time);

        long delta_samples = (new_buf_time - front_of_buffer_time) *
                             sample_rate;

        if(delta_samples > buffer.size())
        {
            for(size_t i = 0; i < (delta_samples - buffer.size()); i++)
            {
                buffer.push_back(0);
            }
        }

        for(size_t i = 0; i < buf.size(); i++)
        {
            buffer.push_back(buf[i]);
        }

        first_sample_time = new_buf_time - (((double)buffer.size()) / sample_rate);
    }

    size_t pop_samples(size_t size, vector<complex<float> >& buf, uhd::time_spec_t& fs_ts)
    {
        if(size > buffer.size())
            size = buffer.size();

        buf.clear();

        for(size_t i = 0; i < size; i++)
        {
            buf[i] = buffer.front();
            buffer.pop_front();
        }

        first_sample_time += (((double)size) / sample_rate);

        return size;
    }
};

/**
 * @brief Computes the correlation between the samples in the corrbuf and the
 * cached zcsequence.
 */
complex<float> get_correlation()
{
    complex<float> accumulator(0,0);
    for(size_t index = 0; index < corrbuf.size(); index++)
        accumulator += corrbuf[index] * conj(zcseq[index]);
    return accumulator;
}

/**
 * @brief Returns the timestamp of the sample where the zcsequence begins in the
 * receive stream.
 */
bool get_peak_time(uhd::time_spec_t first_sample_time,
                   vector<complex<float> > samples, float sample_rate,
                   float alpha, float coeff, uhd::time_spec_t& peak)
{
    /* Low-pass filtered correlation */
    static float lpf_corr = 0.0;
    /* Current calculated correlation */
    float correlation;
    /* Peak finder state */
    static bool found_peak = false;
    bool ret = false;

    for(size_t index = 0; index < samples.size(); index++)
    {
        /* Compute the correlation at this filter offset */
        corrbuf.push_back(samples[index]);
        correlation = abs(get_correlation());

        /* Update the low-pass filter */
        lpf_corr = lpf_corr * (1 - alpha) + correlation * alpha;

        /* If the current correlation exceeds the threshold */
        if(correlation > coeff * lpf_corr)
        {
            if(!found_peak)
            {
                /* If we didn't see a peak last offset, mark seen and set the
                 * out parameter for the peak position.
                 */
                found_peak = true;
                ret = true;
                peak = get_packet_time(first_sample_time, index, sample_rate);
            }
        }
        else
        {
            /* Didn't find a peak */
            found_peak = false;
        }
    }

    return ret;
}

complex<float> cos_source(bool reset = false)
{
    static bool first_run = true;

    static double angle;
    static complex<float> rot;
    static complex<float> ret;

    if(first_run || reset)
    {
        angle = M_PI * 2 * 1000 / 1e6;
        rot = complex<float>(cos(angle), sin(angle));
        ret = complex<float>(1, 0);
    }

    if(!reset)
    {
        ret *= rot;
    }

    return ret;
}

int UHD_SAFE_MAIN(int argc, char *argv[])
{
    uhd::set_thread_priority_safe();

    string address;

    po::options_description desc("Allowed options");
    desc.add_options()
        ("address", po::value<string>(&address)->default_value(""), "Single uhd device address")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(address);

    double rate = 1e6;

    TimestampedBuffer tsbuffer(10000, rate);

    /* Zero-fill the correlation buffer */
    for(size_t i = 0; i < corrbuf.size(); i++)
        corrbuf.push_back(0);

    uhd::tune_request_t tx_target_tuning(2.4e9, 100e6), rx_target_tuning(2.2e9, 100e6);
    usrp->set_tx_freq(tx_target_tuning);
    usrp->set_rx_freq(rx_target_tuning);

    usrp->set_tx_rate(rate);
    usrp->set_rx_rate(rate);
    usrp->set_time_now(uhd::time_spec_t(0.0));


    usrp->set_tx_gain(10.0);
    usrp->set_tx_antenna("TX/RX");
    usrp->set_rx_antenna("RX2");

    uhd::tx_metadata_t tx_md;
    uhd::rx_metadata_t rx_md;

    uhd::stream_args_t stream_args("fc32", "sc16");
    uhd::tx_streamer::sptr tx_stream = usrp->get_tx_stream(stream_args);
    uhd::rx_streamer::sptr rx_stream = usrp->get_rx_stream(stream_args);

    uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS);
    stream_cmd.stream_now = true;
    rx_stream->issue_stream_cmd(stream_cmd);

    vector<complex<float> > tx_buff(tx_stream->get_max_num_samps());
    vector<complex<float> > rx_buff(rx_stream->get_max_num_samps());

    cout << "max samps (tx,rx): " << tx_stream->get_max_num_samps() << "," << rx_stream->get_max_num_samps() << endl;

    uhd::time_spec_t timespec;

    timespec = usrp->get_time_now();
    cout << "Current time: " << timespec.get_real_secs() << endl;
    cout << "Master clock rate: " << usrp->get_master_clock_rate() << endl;

    long total_signal_length = 10000;

    while(1)
    {
        size_t rx_size = rx_stream->recv(&rx_buff.front(), rx_buff.capacity(), rx_md);
        rx_buff.resize(rx_size);

        //tsbuffer.push_samples(rx_buff, rx_md.time_spec);

        uhd::time_spec_t i_time, peak_time, scheduled_time;

        //tsbuffer.pop_samples(100, rx_buff, i_time);

        if(get_peak_time(i_time, rx_buff, rate, 0.01, 3, peak_time))
        {
            scheduled_time = uhd::time_spec_t(peak_time.get_real_secs() + 0.001);

            long signal_tx_counter;
            tx_md.time_spec = scheduled_time;
            tx_md.has_time_spec = true;
            tx_md.start_of_burst = true;
            tx_buff.clear();
            cos_source(true);
            
            signal_tx_counter = 0;
            while(signal_tx_counter < total_signal_length)
            {
                for(size_t i = 0; i < tx_buff.size(); i++)
                {
                    tx_buff[i] = cos_source();
                }
                size_t transmit_size = tx_stream->send(&tx_buff.front(), tx_buff.size(), tx_md);

                if(transmit_size == tx_buff.size())
                    tx_buff.clear();
                else
                {
                    copy(tx_buff.begin() + transmit_size, tx_buff.end(), tx_buff.begin());
                    tx_buff.erase(tx_buff.begin() + (tx_buff.capacity() - transmit_size), tx_buff.end());
                }

                tx_md.has_time_spec = false;
                tx_md.start_of_burst = false;
                
                signal_tx_counter++;
            }

            tx_md.end_of_burst = true;
            tx_stream->send(&tx_buff.front(), 0, tx_md);
            cout << "Packet done" << signal_tx_counter << endl;
        }
    }

}
