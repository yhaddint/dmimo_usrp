u1=read_complex_binary('usrp2.dat');
u2=read_complex_binary('usrp3.dat');
samp_rate = 0.25e6;
%figure;hold on; plot(real(u1));plot(imag(u1));
%figure;hold on; plot(real(u2));plot(imag(u2));
dsmpl=250;
uu1=downsample(u1,dsmpl);
uu2=downsample(u2,dsmpl);
fft_size = 256;
ln=floor(length(uu1)/fft_size);
uuu1=uu1(1:ln*fft_size);
uuu2=uu2(1:ln*fft_size);
uuuu1=reshape(uuu1,[ fft_size,ln]);
uuuu2=reshape(uuu2,[ fft_size, ln]);
s1=fftshift(fft(uuuu1,fft_size))/fft_size;
s2=fftshift(fft(uuuu2),fft_size)/fft_size;
%s1=spectrogram(uu1,fft_size,0);
%s2=spectrogram(uu2,fft_size,0);

figure;
imagesc(linspace(-samp_rate/dsmpl/2,samp_rate/dsmpl/2,fft_size),1:ln,abs(fftshift(fft(uuuu1)))')

figure;
imagesc(linspace(-samp_rate/dsmpl/2,samp_rate/dsmpl/2,fft_size),1:ln,abs(fftshift(fft(uuuu2)))')


[~,ff1]=max(abs(s1));
[~,ff2]=max(abs(s2));

f1=ff1*samp_rate/dsmpl/fft_size;
f2=ff2*samp_rate/dsmpl/fft_size;

figure;histogram(f1)
figure;histogram(f2)