u1=read_complex_binary('usrp2.dat');
u2=read_complex_binary('usrp3.dat');

u3= u1.*conj(u2);

samp_rate = 0.25e6;
%figure;hold on; plot(real(u1));plot(imag(u1));
%figure;hold on; plot(real(u2));plot(imag(u2));
dsmpl=250;
uu1=downsample(u3,dsmpl);
fft_size = 1024;
ln=floor(length(uu1)/fft_size);
uuu1=uu1(1:ln*fft_size);
uuuu1=reshape(uuu1,[ fft_size,ln]);
s1=fftshift(fft(uuuu1,fft_size))/fft_size;
%s1=spectrogram(uu1,fft_size,0);
%s2=spectrogram(uu2,fft_size,0);

figure;
imagesc(linspace(-samp_rate/dsmpl/2,samp_rate/dsmpl/2,fft_size),1:ln,abs(fftshift(fft(uuuu1)))')


[~,ff1]=max(abs(s1));

f1=(ff1-fft_size/2)*samp_rate/dsmpl/fft_size;

figure;histogram(f1,10)
xlabel('Frequency (Hz)')
ylabel('Number of Measurements')
