samp1=8e-10;
samp2=1e-10;

%load('data2_1Mhz')
load('data22_1Mhz')
plot_hist(d1,samp1,1)

% load('data22_10Mhz')
% plot_hist(d2,samp2,2)


function plot_hist(d,samp,cs)
    bd=[];
    dly = nan(length(d),1);

    for fi = 1 : length(d)
       c= d{fi};
       c1=c(:,1);
       c2=c(:,2);
       c1=c1/rms(c1);
       c2=c2/rms(c2);

        [c12,lags]=xcorr(abs(c1),abs(c2));

        %plot(lags,abs(c12))

        [~,mxind]=max(abs(c12));
        dly(fi)=lags(mxind)*samp;
        if cs ==1
            if lags(mxind)<-1e-5
                bd = [bd fi];
            end
        elseif cs ==2
            %if lags(mxind)>1e4
            if lags(mxind)*samp<-2e-6 || lags(mxind)*samp>2e-6
                bd = [bd fi];
            end
        end
        
    end
    dly(bd)=[];
    figure;
    histogram(dly/1e-5,20)
    xlabel('Delay as fraction of Td')
    ylabel('Number of Measurements')
end