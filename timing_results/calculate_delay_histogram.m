samp_time = 2e-010;


indx = [0.9e6:1.5e6];
nfiles = 38;
dly = nan(nfiles,1);
for fi = 0:nfiles
   fname = sprintf('01Mhz/cap_%03d',fi);
   c1=wfm2read([fname,'_Ch1.wfm']);
   c1=c1(indx);
   c1=c1/rms(c1);
   c2=wfm2read([fname,'_Ch2.wfm']);
   c2=c2(indx);
   c2=c2/rms(c2);

    [c12,lags]=xcorr(c1,c2);

    %plot(lags,abs(c12))

    [~,mxind]=max(abs(c12));
    dly(fi+1)=lags(mxind)*samp_time;
    
    % Do some stuff
end
figure;
histogram(dly)

