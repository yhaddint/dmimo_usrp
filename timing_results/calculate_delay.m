samp_time = 2e-010;

load data_2018_08_20

indx = 0.9e6:1.5e6;

c1= ch1_1(indx);
c2= ch2_1(indx);

c2=c2*rms(c1)/rms(c2);

plot([c1,c2])

[c12,lags]=xcorr(c1,c2);

plot(lags,abs(c12))

[~,indx]=max(abs(c12));
lags(indx)*samp_time