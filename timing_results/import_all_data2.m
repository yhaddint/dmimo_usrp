

fls = dir('data2/data2/cap2-10_*.csv');
d = cell(length(fls),1);
idx = 1;
for fni = 1: length(fls)
    x = import_data2(sprintf('data2/data2/%s',fls(fni).name));
    if rms(x(:,1))>1e-3
        d{idx}=x;
        idx
        idx=idx+1;
    else
        0;
    end
    fni
end

d2 = d(1:idx-1);
save('data22_10Mhz','d2')
