/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_HOWTO_FRAC_TIMING_FREQ_PHASE_SYNCH_MSG2_DEMO_H
#define INCLUDED_HOWTO_FRAC_TIMING_FREQ_PHASE_SYNCH_MSG2_DEMO_H

#include <howto/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace howto {

    /*!
     * \brief <+description of block+>
     * \ingroup howto
     *
     */
    class HOWTO_API frac_timing_freq_phase_synch_msg2_demo : virtual public gr::block
    {
     public:
      typedef boost::shared_ptr<frac_timing_freq_phase_synch_msg2_demo> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of howto::frac_timing_freq_phase_synch_msg2_demo.
       *
       * To avoid accidental use of raw pointers, howto::frac_timing_freq_phase_synch_msg2_demo's
       * constructor is in a private implementation
       * class. howto::frac_timing_freq_phase_synch_msg2_demo::make is the public interface for
       * creating new instances.
       */
      static sptr make(size_t timing_margin,size_t phase_det_length, float phase_det_mag, size_t timing_margin2 , size_t data_length, double sample_rate, float freq_scaler, uint32_t oversample, bool usrp);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_FRAC_TIMING_FREQ_PHASE_SYNCH_MSG2_DEMO_H */

