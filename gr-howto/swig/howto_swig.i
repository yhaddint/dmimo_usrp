/* -*- c++ -*- */

#define HOWTO_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "howto_swig_doc.i"

%{
#include "howto/CFOest.h"
#include "howto/CFOestsync.h"
#include "howto/CFOcomp.h"
#include "howto/CFO_EKF.h"
#include "howto/CFOcompv2.h"
#include "howto/DCOcomp.h"
#include "howto/trig_vector_source.h"
#include "howto/trig_vector_source_tag.h"
#include "howto/timing_alignment.h"
#include "howto/first_trigger.h"
#include "howto/priority_multiply.h"
#include "howto/timing_freq_phase_synch.h"
#include "howto/ml_frac_timing_est.h"
#include "howto/frac_timing_freq_phase_synch.h"
#include "howto/my_accum.h"
#include "howto/detect_phase.h"
#include "howto/detect_phase_msg.h"
#include "howto/frac_timing_freq_phase_synch_msg.h"
#include "howto/frac_timing_freq_phase_synch_msg2.h"
#include "howto/my_peak_detector.h"
#include "howto/trig_multiple.h"
#include "howto/trig_repeat.h"
#include "howto/vector_burst_source.h"
#include "howto/frac_timing_freq_phase_synch_msg2_demo.h"
#include "howto/det_abs_phase_msg.h"
#include "howto/det_abs_phase.h"
#include "howto/det_diff_phase_trig.h"
#include "howto/det_diff_phase_trig_msg.h"
#include "howto/msg_subtract.h"
#include "howto/level_trigger.h"
#include "howto/log_bf.h"
#include "howto/log_mimo.h"
#include "howto/recip_source.h"
#include "howto/freq_corr.h"
#include "howto/gen_phase_msg.h"
#include "howto/frac_timing_freq_phase_synch_msg3.h"
#include "howto/cfo_filter.h"
%}


%include "howto/CFOest.h"
GR_SWIG_BLOCK_MAGIC2(howto, CFOest);
%include "howto/CFOestsync.h"
GR_SWIG_BLOCK_MAGIC2(howto, CFOestsync);
%include "howto/CFOcomp.h"
GR_SWIG_BLOCK_MAGIC2(howto, CFOcomp);

%include "howto/CFO_EKF.h"
GR_SWIG_BLOCK_MAGIC2(howto, CFO_EKF);
%include "howto/CFOcompv2.h"
GR_SWIG_BLOCK_MAGIC2(howto, CFOcompv2);
%include "howto/DCOcomp.h"
GR_SWIG_BLOCK_MAGIC2(howto, DCOcomp);
%include "howto/trig_vector_source.h"
GR_SWIG_BLOCK_MAGIC2(howto, trig_vector_source);
%include "howto/trig_vector_source_tag.h"
GR_SWIG_BLOCK_MAGIC2(howto, trig_vector_source_tag);


%include "howto/timing_alignment.h"
GR_SWIG_BLOCK_MAGIC2(howto, timing_alignment);
%include "howto/first_trigger.h"
GR_SWIG_BLOCK_MAGIC2(howto, first_trigger);
%include "howto/priority_multiply.h"
GR_SWIG_BLOCK_MAGIC2(howto, priority_multiply);

%include "howto/timing_freq_phase_synch.h"
GR_SWIG_BLOCK_MAGIC2(howto, timing_freq_phase_synch);
%include "howto/ml_frac_timing_est.h"
GR_SWIG_BLOCK_MAGIC2(howto, ml_frac_timing_est);
%include "howto/frac_timing_freq_phase_synch.h"
GR_SWIG_BLOCK_MAGIC2(howto, frac_timing_freq_phase_synch);
%include "howto/my_accum.h"
GR_SWIG_BLOCK_MAGIC2(howto, my_accum);
%include "howto/detect_phase.h"
GR_SWIG_BLOCK_MAGIC2(howto, detect_phase);
%include "howto/detect_phase_msg.h"
GR_SWIG_BLOCK_MAGIC2(howto, detect_phase_msg);
%include "howto/frac_timing_freq_phase_synch_msg.h"
GR_SWIG_BLOCK_MAGIC2(howto, frac_timing_freq_phase_synch_msg);
%include "howto/frac_timing_freq_phase_synch_msg2.h"
GR_SWIG_BLOCK_MAGIC2(howto, frac_timing_freq_phase_synch_msg2);

%include "howto/my_peak_detector.h"
GR_SWIG_BLOCK_MAGIC2(howto, my_peak_detector);
%include "howto/trig_multiple.h"
GR_SWIG_BLOCK_MAGIC2(howto, trig_multiple);
%include "howto/trig_repeat.h"
GR_SWIG_BLOCK_MAGIC2(howto, trig_repeat);
%include "howto/vector_burst_source.h"
GR_SWIG_BLOCK_MAGIC2(howto, vector_burst_source);
%include "howto/frac_timing_freq_phase_synch_msg2_demo.h"
GR_SWIG_BLOCK_MAGIC2(howto, frac_timing_freq_phase_synch_msg2_demo);
%include "howto/det_abs_phase_msg.h"
GR_SWIG_BLOCK_MAGIC2(howto, det_abs_phase_msg);
%include "howto/det_abs_phase.h"
GR_SWIG_BLOCK_MAGIC2(howto, det_abs_phase);
%include "howto/det_diff_phase_trig.h"
GR_SWIG_BLOCK_MAGIC2(howto, det_diff_phase_trig);
%include "howto/det_diff_phase_trig_msg.h"
GR_SWIG_BLOCK_MAGIC2(howto, det_diff_phase_trig_msg);
%include "howto/msg_subtract.h"
GR_SWIG_BLOCK_MAGIC2(howto, msg_subtract);
%include "howto/level_trigger.h"
GR_SWIG_BLOCK_MAGIC2(howto, level_trigger);
%include "howto/log_bf.h"
GR_SWIG_BLOCK_MAGIC2(howto, log_bf);
%include "howto/log_mimo.h"
GR_SWIG_BLOCK_MAGIC2(howto, log_mimo);
%include "howto/recip_source.h"
GR_SWIG_BLOCK_MAGIC2(howto, recip_source);
%include "howto/freq_corr.h"
GR_SWIG_BLOCK_MAGIC2(howto, freq_corr);
%include "howto/gen_phase_msg.h"
GR_SWIG_BLOCK_MAGIC2(howto, gen_phase_msg);
%include "howto/frac_timing_freq_phase_synch_msg3.h"
GR_SWIG_BLOCK_MAGIC2(howto, frac_timing_freq_phase_synch_msg3);
%include "howto/cfo_filter.h"
GR_SWIG_BLOCK_MAGIC2(howto, cfo_filter);
