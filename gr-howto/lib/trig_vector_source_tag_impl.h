/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_TRIG_VECTOR_SOURCE_TAG_IMPL_H
#define INCLUDED_HOWTO_TRIG_VECTOR_SOURCE_TAG_IMPL_H

#include <howto/trig_vector_source_tag.h>

namespace gr {
  namespace howto {

    class trig_vector_source_tag_impl : public trig_vector_source_tag
    {
     private:
      // Nothing to declare in this block.
      std::vector<gr_complex> d_data;
      double d_sample_rate;
      double d_real_time_seconds;
      uint64_t d_whole_seconds_strt;
      double d_frac_seconds_strt;
      uint64_t d_whole_seconds_tmp;
      double d_frac_seconds_tmp;
      long d_tmp;
      bool d_tx;
      uint32_t d_offset;

     public:
      trig_vector_source_tag_impl(const std::vector<gr_complex> &data,double sample_rate);
      ~trig_vector_source_tag_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_TRIG_VECTOR_SOURCE_TAG_IMPL_H */

