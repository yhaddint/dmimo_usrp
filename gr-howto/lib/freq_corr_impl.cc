/* -*- c++ -*- */
/* 
 * Copyright 2020 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define F_PI ((float)(M_PI))
#include <gnuradio/fxpt.h>
#include <gnuradio/io_signature.h>
#include "freq_corr_impl.h"

namespace gr {
  namespace howto {

    freq_corr::sptr
    freq_corr::make(float samp_rate)
    {
      return gnuradio::get_initial_sptr
        (new freq_corr_impl(samp_rate));
    }

    /*
     * The private constructor
     */
    freq_corr_impl::freq_corr_impl(float samp_rate)
      : gr::sync_block("freq_corr",
              gr::io_signature::make2(2, 2, sizeof(gr_complex),sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_samp_rate(samp_rate),
      d_phase(0)
    {}

    /*
     * Our virtual destructor.
     */
    freq_corr_impl::~freq_corr_impl()
    {
    }

    int
    freq_corr_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *sig_in = (const gr_complex *) input_items[0];
      const float *freq_in = (const float *) input_items[1];
      gr_complex *sig_out = (gr_complex *) output_items[0];

      // Do <+signal processing+>
      float phase=d_phase;
      float oi,oq;
      for (int i=0;i<noutput_items;i++){
        phase = phase + 1/d_samp_rate*2*F_PI*freq_in[i];
        phase   = std::fmod(phase + F_PI, 2.0f * F_PI) - F_PI;
        int32_t angle = gr::fxpt::float_to_fixed (phase);
        gr::fxpt::sincos(angle, &oq, &oi);

        sig_out[i] = sig_in[i]*gr_complex(oi, -oq);
      }
      d_phase = std::fmod(phase + F_PI, 2.0f * F_PI) - F_PI;

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

