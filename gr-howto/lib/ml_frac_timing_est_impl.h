/* -*- c++ -*- */
/* 
 * Copyright 2018 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_ML_FRAC_TIMING_EST_IMPL_H
#define INCLUDED_HOWTO_ML_FRAC_TIMING_EST_IMPL_H

#include <howto/ml_frac_timing_est.h>

namespace gr {
  namespace howto {

    class ml_frac_timing_est_impl : public ml_frac_timing_est
    {
     private:
      // Nothing to declare in this block.
        uint32_t d_oversample;
        std::vector<gr_complex> d_zc_iq;;
     public:
      ml_frac_timing_est_impl(uint32_t oversample);
      ~ml_frac_timing_est_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_ML_FRAC_TIMING_EST_IMPL_H */

