/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define F_PI ((float)(M_PI))


#define STATE_WATCH 0
#define STATE_DELAY 1
#define STATE_POSTPONE 2
#define STATE_ESTIMATE 3


#include <gnuradio/io_signature.h>
#include "det_abs_phase_msg_impl.h"

namespace gr {
  namespace howto {

    det_abs_phase_msg::sptr
    det_abs_phase_msg::make(uint32_t delay,uint32_t duration)
    {
      return gnuradio::get_initial_sptr
        (new det_abs_phase_msg_impl(delay, duration));
    }

    /*
     * The private constructor
     */
    det_abs_phase_msg_impl::det_abs_phase_msg_impl(uint32_t delay,uint32_t duration)
      : gr::sync_block("det_abs_phase_msg",
              gr::io_signature::make2(2,2, sizeof(char),sizeof(gr_complex)),
              gr::io_signature::make(0, 0, 0)),
    d_delay(delay),
    d_duration(duration),
    d_remain(0),
    d_state(STATE_WATCH)
    {
      message_port_register_out(pmt::mp("phase_out"));
      set_output_multiple(d_duration);
      set_output_multiple(10);
    }

    /*
     * Our virtual destructor.
     */
    det_abs_phase_msg_impl::~det_abs_phase_msg_impl()
    {
    }

    int
    det_abs_phase_msg_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const char *trig_in = (const char *) input_items[0];
      const gr_complex *iq_in = (const gr_complex *) input_items[1];

      // Do <+signal processing+>
      float phase;
      float out;
      int trig_loc=0;
      int phase_loc = 0;

      int ninput_items = noutput_items;



      if (d_state == STATE_WATCH){
        for (int i =0;i<noutput_items;i++){
          if (trig_in[i] == 1){
            trig_loc = i;
            d_remain = d_delay;
            d_state = STATE_DELAY;
            //std::cout <<  std::endl << "Trigger "   << std::endl;
            break;
          }
        }
      }

      if (d_state == STATE_DELAY){
        //std::cout << "first " << iq_in[trig_loc] << "last " << iq_in[noutput_items-1] << std::endl;
        //std::cout << "d_remain " << d_remain<< " n op " << noutput_items << " trig_loc "<< trig_loc << std::endl;
        if (noutput_items-trig_loc < d_remain){
          d_remain = d_remain -  (noutput_items-trig_loc);
        }
        else{
          //std::cout << " Terminating" << std::endl;
          if (noutput_items-trig_loc >= d_remain+d_duration){
            phase_loc = trig_loc + d_remain;
            noutput_items = trig_loc +d_remain + d_duration;
            d_remain = 0;
            d_state = STATE_ESTIMATE;
            }
          else{
            noutput_items = trig_loc + d_remain;
            d_remain = 0;
            d_state = STATE_POSTPONE;
          }
        } 
      }
      else if (d_state == STATE_POSTPONE){
        //std::cout << "Postpone " <<  std::endl;
        phase_loc = 0;
        d_state = STATE_ESTIMATE;
      }
          


      if (d_state == STATE_ESTIMATE){
        float si = 0 ,sq =0;
        #define START_BOUNDARY (30)
        #define END_BOUNDARY (5)

        for (int j = START_BOUNDARY ; j< d_duration-END_BOUNDARY;j++){
          si += std::real(iq_in[phase_loc+j]);
          sq += std::imag(iq_in[phase_loc+j]);
           //std::cout << std::real(iq_in[phase_loc+j])<< " ";
         }

         si=si/(d_duration-(START_BOUNDARY+END_BOUNDARY));
         sq=sq/(d_duration-(START_BOUNDARY+END_BOUNDARY));
         


        //std::cout << std::endl;
        // std::cout << "IQ " << si  << " " << sq  << std::endl;

        phase = atan2(sq , si);
        d_out =  -phase ;
        d_out = std::fmod(d_out + F_PI, 2.0f * F_PI) - F_PI;
        
        message_port_pub(pmt::mp("phase_out"),  pmt::cons(pmt::PMT_NIL,pmt::make_blob(&d_out,sizeof(float))));
        d_state = STATE_WATCH;
        d_remain = 0;
      }

      // Tell runtime system how many output items we produced.
      //std::cout << "nip " << ninput_items<< " nop " << noutput_items  << std::endl;
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

