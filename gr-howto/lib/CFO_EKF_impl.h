/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_CFO_EKF_IMPL_H
#define INCLUDED_HOWTO_CFO_EKF_IMPL_H

#include <howto/CFO_EKF.h>
#include <fstream>

namespace gr {
  namespace howto {

    class CFO_EKF_impl : public CFO_EKF
    {
     private:
      // Nothing to declare in this block.
      //
      std::ofstream op_file;
      float RSS;
      float RSS_sum;
      int cnt_write;
      int print_num;
      int cnt_samp;
      int cnt_train_samp;
      float oneshot_cfo_est;
      gr_complex corr_sum;
      int d_Ncycle;
      int d_Ntrain;
      float d_samp_rate;
      float Ts;
      float pi;
      float Tcycle;
      float mag_iq;


      struct state
      {
        // 2x1
        float x_hat[2][1];
        float x_update[2][1];

        // 3x1
        float z[3][1];

        // 3x1
        float y_tilde[3][1];

        float H[3][2];
        float P[2][2];
        float Q[2][2];
        float F[2][2];
        float F_inv[2][2];
        float F_t[2][2];
        float R[3][3];
        float K[2][3];
        float S[3][3];
      } state;

     public:
      CFO_EKF_impl(float samp_rate,
                    int Ncycle,
                    int Ntrain,
                    float initial_frequency);
                   // std::vector< std::vector<float> > initial_P_mtx,
                   // std::vector< std::vector<float> > state_update_cov_matrix,
                   // std::vector< std::vector<float> > state_update_matrix,
                   // std::vector< std::vector<float> > one_shot_matrix);
      ~CFO_EKF_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_CFO_EKF_IMPL_H */

