/* -*- c++ -*- */
/* 
 * Copyright 2019 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "trig_repeat_impl.h"

#include <algorithm>

#define STATE_LOOKING 0
#define STATE_COUNTING 1
#define STATE_SILENT 2

#define ZC_LEN 63


namespace gr {
  namespace howto {

    trig_repeat::sptr
    trig_repeat::make(uint32_t ntrig, uint32_t duration, uint32_t packet_len)
    {
      return gnuradio::get_initial_sptr
        (new trig_repeat_impl(ntrig, duration,packet_len));
    }

    /*
     * The private constructor
     */
    trig_repeat_impl::trig_repeat_impl(uint32_t ntrig, uint32_t duration, uint32_t packet_len)
      : gr::sync_block("trig_repeat",
              gr::io_signature::make(1, 1, sizeof(char)),
              gr::io_signature::make(1, 1, sizeof(char))),
      d_ntrig(ntrig),
      d_packet_len(packet_len),
      d_remaining(0),
      d_state(STATE_LOOKING)
    {}

    /*
     * Our virtual destructor.
     */
    trig_repeat_impl::~trig_repeat_impl()
    {
    }

    int
    trig_repeat_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const char *in = (const char *) input_items[0];
      char *out = (char *) output_items[0];

      int trig_loc = 0;

      std::fill(out, out+noutput_items, 0);

      if (d_state == STATE_LOOKING){
        for (int i=0;i<noutput_items;i++){
          if (in[i]==1){
            // std::cout<<"Trigger found" << std::endl;
            d_state = STATE_COUNTING;
            d_remaining = d_ntrig*ZC_LEN;
            trig_loc = i;
            break;
          }
        }
      }
      if (d_state == STATE_COUNTING){
        for (int i=trig_loc;i<noutput_items;i++){
          if (d_remaining% ZC_LEN==0){
            out[i]=1;
          }
          d_remaining -- ;
          if (d_remaining <= 0){
            d_state = STATE_SILENT;
            d_remaining = d_packet_len;
            trig_loc = i;
            break;
          }
        }
      }
      if (d_state == STATE_SILENT){
       
          if (noutput_items - trig_loc >=d_remaining){
            noutput_items = noutput_items - d_remaining;
            d_state = STATE_LOOKING;
          }
          else{
          d_remaining -= noutput_items - trig_loc;
          }
      }
      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

