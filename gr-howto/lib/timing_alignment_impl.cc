/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "timing_alignment_impl.h"

namespace gr {
  namespace howto {

    timing_alignment::sptr
    timing_alignment::make(size_t timing_margin, size_t data_length, double sample_rate)
    {
      return gnuradio::get_initial_sptr
        (new timing_alignment_impl(timing_margin, data_length, sample_rate));
    }

    /*
     * The private constructor
     */
    timing_alignment_impl::timing_alignment_impl(size_t timing_margin, size_t data_length, double sample_rate)
      : gr::block("timing_alignment",
              gr::io_signature::make2(2, 2, sizeof(char),sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_sample_rate(sample_rate),
      d_offset(0),
      d_timing_margin(timing_margin),
      d_data_length(data_length),
      d_remain_data(0)
    {

        set_output_multiple(d_data_length);
    }

    /*
     * Our virtual destructor.
     */
    timing_alignment_impl::~timing_alignment_impl()
    {
    }

    void
    timing_alignment_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */
       ninput_items_required[0] = d_data_length;
       ninput_items_required[1] = d_data_length;
    }

    int
    timing_alignment_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {



      const char *in_trig = (const char *) input_items[0];
      const gr_complex *in_iq  = (const gr_complex *) input_items[1];
      gr_complex *out = (gr_complex *) output_items[0];

      std::vector<tag_t> tags;
      // Read timing tag from USRP
      // https://gnuradio.blogspot.com/2015/12/re-discuss-gnuradio-working-with-tags.html
        static const pmt::pmt_t TIME_KEY = pmt::string_to_symbol("rx_time");
          get_tags_in_range(
              tags, 0, nitems_read(0), nitems_read(0) + noutput_items);
          std::vector<tag_t>::iterator itr;
          size_t j = 0;
          for (itr = tags.begin(); itr != tags.end(); itr++, j++)
          {
            if (pmt::eq((*itr).key, TIME_KEY))
            {
              d_whole_seconds_strt = pmt::to_uint64(pmt::tuple_ref((*itr).value, 0));
              d_frac_seconds_strt = pmt::to_double(pmt::tuple_ref((*itr).value, 1));
              d_real_time_seconds = 
                  static_cast<double>(d_whole_seconds_strt) +
                  static_cast<double>(d_frac_seconds_strt);
              std::cerr << " tag_offset=" << (*itr).offset
                        << " noutput_items=" << noutput_items
                        << std::setprecision(std::numeric_limits<double>::digits10 + 1)
                        << " time_seconds=" << d_real_time_seconds << std::endl;

              
            }
          }

          //noutput_items = 10; //ninput_items[0];


      
      
      noutput_items = 0;   
      bool op_bl = false; // We have output
      uint32_t i;
      //std::cout << "first " << d_offset<< " " << ninput_items[0] << std::endl;

      // If no data remaining from previous call
      if (d_remain_data == 0){
        // Look for trigger
        for ( i = d_offset ; i < ninput_items[0];i++){
            if (in_trig[i]==1){
              // If trigger found

              //std::cout << i << std::endl;
              // We are outputting data_length
              noutput_items = d_data_length;

              // Calculate trigger time plus margin - > time of next transmission
              d_frac_seconds_tmp = d_frac_seconds_strt + ((double)(nitems_read(0) + i + d_timing_margin))/d_sample_rate; // + 43937
              d_whole_seconds_tmp = d_whole_seconds_strt + (uint64_t)floor(d_frac_seconds_tmp);
              d_frac_seconds_tmp = d_frac_seconds_tmp - floor(d_frac_seconds_tmp);

              

              // Write timing tag
              tag_t tag_ts;
              tag_ts.offset = nitems_written(0) ;
              tag_ts.key = pmt::mp("tx_time");
              tag_ts.value = pmt::make_tuple(
                  pmt::from_uint64( d_whole_seconds_tmp  ),
                  pmt::from_double(d_frac_seconds_tmp));
              add_item_tag(0, tag_ts);

              // Write start of burst               
              tag_t tag_sob;
              tag_sob.offset = nitems_written(0) ;
              tag_sob.key = pmt::mp("tx_sob");
              tag_sob.value = pmt::PMT_T;
              add_item_tag(0, tag_sob);

              d_remain_data = d_data_length;

              // Here we are only estimating how much to consume from trigger port
              // Actual transmission is and consumption of data in next if
              // I we can't send th whole data length this  time
              if (i+d_data_length>ninput_items[0]){
                // Consume as much as you can and leave the rest for the next call
                consume(0,ninput_items[0]);
                d_offset =  (i+(uint32_t)d_data_length) - (uint32_t)ninput_items[0] ;
              }
              else{
                // Else we will send data length and return
                d_offset = 0;
                consume(0, i+d_data_length);
              }
              // We have data stop looking for trigger
              op_bl=true;
              break;
            }
          }
          // We did not find a trigger, just read all input on trigger
          if (op_bl == false){
            d_offset = 0;
            consume (0,ninput_items[0]);
          }
        }
        // If there is data remaining from this or previous call try to send it
        if (d_remain_data > 0) {

          // If we have enough input on input 1
          if (ninput_items[1] >= d_data_length){
                // send the whole thing 
                for (int j = 0; j< d_data_length;j++){
                  out[j] = in_iq[j];
                }
                d_remain_data = 0;
              }
              else{
                // send as much as you can
                for (int j = 0; j< ninput_items[1];j++){
                  out[j] = in_iq[j];
                }
                d_remain_data = d_data_length - ninput_items[1];
              }
              // consume data
              consume(1, d_data_length - d_remain_data);
              noutput_items = d_data_length - d_remain_data;
        }
        // if no data remaining and there is an output end burst
        if (d_remain_data == 0 && noutput_items>0){
                tag_t tag_eob;
                tag_eob.offset = nitems_written(0)+ noutput_items -1;
                tag_eob.key = pmt::mp("tx_eob");
                tag_eob.value = pmt::PMT_T;              
                add_item_tag(0, tag_eob);
          }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

