/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_TIMING_ALIGNMENT_IMPL_H
#define INCLUDED_HOWTO_TIMING_ALIGNMENT_IMPL_H

#include <howto/timing_alignment.h>

namespace gr {
  namespace howto {

    class timing_alignment_impl : public timing_alignment
    {
     private:
      // Nothing to declare in this block.
      double d_sample_rate;
      double d_real_time_seconds;
      uint64_t d_whole_seconds_strt;
      double d_frac_seconds_strt;
      uint64_t d_whole_seconds_tmp;
      double d_frac_seconds_tmp;
      uint32_t d_offset;
      size_t d_timing_margin;
      size_t d_data_length;
      size_t d_remain_data;

     public:
      timing_alignment_impl(size_t timing_margin, size_t data_length, double sample_rate);
      ~timing_alignment_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_TIMING_ALIGNMENT_IMPL_H */

