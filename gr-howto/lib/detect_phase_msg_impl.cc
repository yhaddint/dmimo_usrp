/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "detect_phase_msg_impl.h"

#define F_PI ((float)(M_PI))

#define STATE_WATCH 0
#define STATE_POSTPONE 1
#define STATE_ESTIMATE 2
#define STATE_HOLD 3

namespace gr {
  namespace howto {

    detect_phase_msg::sptr
    detect_phase_msg::make(float thresh, uint32_t delay,uint32_t duration, uint32_t hold)
    {
      return gnuradio::get_initial_sptr
        (new detect_phase_msg_impl(thresh, delay,duration, hold));
    }

    /*
     * The private constructor
     */
    detect_phase_msg_impl::detect_phase_msg_impl(float thresh, uint32_t delay,uint32_t duration, uint32_t hold)
      : gr::sync_block("detect_phase_msg",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(0, 0, sizeof(float))),
    d_thresh(thresh),
    d_delay(delay),
    d_duration(duration),
    d_hold(hold),
    d_remain(0),
    d_state(STATE_WATCH)
    {
       message_port_register_out(pmt::mp("phase_out"));
       set_output_multiple(2*d_duration + d_delay);
    }

    /*
     * Our virtual destructor.
     */
    detect_phase_msg_impl::~detect_phase_msg_impl()
    {
    }

    int
    detect_phase_msg_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];

      float phase1,phase2;
      float out;
      int trig_loc=0;

      int ninput_items = noutput_items;
      noutput_items = 0;

      if (d_state == STATE_WATCH || d_state == STATE_POSTPONE){
        for (int i =0;i<ninput_items;i++){
          if (std::abs(in[i])>d_thresh){
            trig_loc = i;
            if (i+2*d_duration + d_delay>ninput_items){
              // if not enough input
              // Do nothing leave it for next time
              d_state = STATE_POSTPONE;
            }
            else{
              d_remain = d_hold;
              d_state = STATE_ESTIMATE;
            }
            break;
          }
        }
      }

      if (d_state == STATE_WATCH){
        noutput_items = ninput_items;
      }
      else if (d_state == STATE_POSTPONE){
        noutput_items = trig_loc -1;
      }
      else if (d_state == STATE_ESTIMATE){
        float si1 = 0 ,si2 =0;
        float sq1 = 0 ,sq2 =0;
        #define START_BOUNDARY (50)
        #define END_BOUNDARY (10)

        for (int j = START_BOUNDARY ; j< d_duration-END_BOUNDARY;j++){
          si1 += std::real(in[trig_loc+j]);
          sq1 += std::imag(in[trig_loc+j]);
          si2 += std::real(in[trig_loc+j+d_delay]);
          sq2 += std::imag(in[trig_loc+j+d_delay]);
           // std::cout << si1  << " " << sq1 << " " << si2  << " " << sq2 << std::endl;
         }
         si1=si1/(d_duration-(START_BOUNDARY+END_BOUNDARY));
         sq1=sq1/(d_duration-(START_BOUNDARY+END_BOUNDARY));
         si2=si2/(d_duration-(START_BOUNDARY+END_BOUNDARY));
         sq2=sq2/(d_duration-(START_BOUNDARY+END_BOUNDARY));


        // std::cout << std::endl;
        // std::cout << si1  << " " << sq1 << " " << si2  << " " << sq2 << std::endl;

        phase1 = atan2(sq1 , si1);
        phase2 = atan2(sq2,si2);

        d_out =  (phase1-phase2) ;
        d_out = std::fmod(d_out + F_PI, 2.0f * F_PI) - F_PI;
        message_port_pub(pmt::mp("phase_out"),  pmt::cons(pmt::PMT_NIL,pmt::make_blob(&d_out,sizeof(float))));
        // std::cout << "Phase sent" << std::endl;
        d_remain = d_hold;

        noutput_items = trig_loc + d_delay;
        d_state = STATE_HOLD;

      }

      if (d_state == STATE_HOLD){
        if (noutput_items + d_remain < ninput_items){
          noutput_items = noutput_items + d_remain;
          d_state = STATE_WATCH;
        }
        else {
          d_remain = d_remain - (ninput_items - noutput_items);
          noutput_items = ninput_items;
          d_state = STATE_HOLD;
        }

      }

       // TODO: Need to handle when threshold goes low
       // in case there are many windows inside same sample


      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

