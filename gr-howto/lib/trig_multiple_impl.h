/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_TRIG_MULTIPLE_IMPL_H
#define INCLUDED_HOWTO_TRIG_MULTIPLE_IMPL_H

#include <howto/trig_multiple.h>

namespace gr {
  namespace howto {

    class trig_multiple_impl : public trig_multiple
    {
     private:
      // Nothing to declare in this block.
      uint32_t d_ntrig;
      uint32_t d_duration;
      char d_state;
      uint32_t d_remain;
      uint32_t d_packet_len;
      uint32_t d_elapsed;
      uint32_t d_counter;

     public:
      trig_multiple_impl(uint32_t ntrig, uint32_t duration);
      ~trig_multiple_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_TRIG_MULTIPLE_IMPL_H */

