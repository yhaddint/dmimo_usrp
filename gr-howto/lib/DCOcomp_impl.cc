/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "DCOcomp_impl.h"

namespace gr {
  namespace howto {

    DCOcomp::sptr
    DCOcomp::make(float samp_rate, float update_samp)
    {
      return gnuradio::get_initial_sptr
        (new DCOcomp_impl(samp_rate, update_samp));
    }

    /*
     * The private constructor
     */
    DCOcomp_impl::DCOcomp_impl(float samp_rate, float update_samp)
      : gr::sync_block("DCOcomp",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_update_samp(update_samp)
    {
      DCO_acc = 0;
      DCO_est = 0;
      samp_cnt = 0;
    }

    /*
     * Our virtual destructor.
     */
    DCOcomp_impl::~DCOcomp_impl()
    {
    }

    int
    DCOcomp_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];

      for (int i = 0; i < noutput_items; i++){
        samp_cnt++;
        DCO_acc = DCO_acc + in[i];
        if (samp_cnt==d_update_samp){
          samp_cnt=0;
          DCO_est = DCO_acc/d_update_samp;
          DCO_acc = 0;
        }
        out[i] = in[i]-DCO_est;
      }

      
      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

