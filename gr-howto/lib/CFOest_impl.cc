/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "CFOest_impl.h"

namespace gr {
  namespace howto {

    CFOest::sptr
    CFOest::make()
    {
      return gnuradio::get_initial_sptr
        (new CFOest_impl());
    }

    /*
     * The private constructor
     */
    CFOest_impl::CFOest_impl()
      : gr::sync_decimator("CFOest",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(float)), 500)
    {}

    /*
     * Our virtual destructor.
     */
    CFOest_impl::~CFOest_impl()
    {
    }

    int
    CFOest_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      float *out = (float *) output_items[0];

      for (int n = 0; n<noutput_items; n++){
        gr_complex autocorr = 0;
        gr_complex temp = 0;
        for(int i = 0; i < 400; i++){
          temp = std::conj(in[n*500+i]) * in[n*500+i+100];
          autocorr = autocorr + temp;
        }
        out[n] = arg(autocorr)/100;
      }
      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

