/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "priority_multiply_impl.h"

namespace gr {
  namespace howto {

    priority_multiply::sptr
    priority_multiply::make()
    {
      return gnuradio::get_initial_sptr
        (new priority_multiply_impl());
    }

    /*
     * The private constructor
     */
    priority_multiply_impl::priority_multiply_impl()
      : gr::block("priority_multiply",
              gr::io_signature::make(2, 2, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex)))
    {
      set_output_multiple(1000);
    }

    /*
     * Our virtual destructor.
     */
    priority_multiply_impl::~priority_multiply_impl()
    {
    }

    void
    priority_multiply_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */
      ninput_items_required[1] = 0;
      ninput_items_required[0] = noutput_items;
    }

    int
    priority_multiply_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {

      //std::cout << "Work called" << std::endl;
      const gr_complex *in_lp = (const gr_complex *) input_items[0];
      const gr_complex *in_hp = (const gr_complex *) input_items[1];
      gr_complex *out = (gr_complex *) output_items[0];

      // Do <+signal processing+>
      // Tell runtime system how many input items we consumed on
      // each input stream.
      noutput_items = 0;
      if (ninput_items[1]>0){
        if (ninput_items[1]<ninput_items[0]){
          noutput_items = ninput_items[1];
        }
        else{
          noutput_items = ninput_items[0];
        }
        for (int i = 0 ; i < noutput_items ; i ++){
            out[i] = in_lp[i] * in_hp[i];
        }
        consume_each(noutput_items);
        //std::cout << "consumed each " <<  noutput_items << std::endl;
      }
      else{
        //std::cout << "consumed input 0" << std::endl;
        consume(0,ninput_items[0]);
      }
      //std::cout << ninput_items[0] << " " << ninput_items[1] << " " <<  noutput_items << std::endl; 

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

