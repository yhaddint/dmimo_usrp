/* -*- c++ -*- */
/* 
 * Copyright 2021 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "gen_phase_msg_impl.h"

#include <gnuradio/fxpt.h>

#define F_PI ((float)(M_PI))
#define STATE_WAIT 0
#define STATE_SEND 1

#define SLEEP_WAITING_TIME_uS 10



namespace gr {
  namespace howto {

    gen_phase_msg::sptr
    gen_phase_msg::make(uint32_t duration)
    {
      return gnuradio::get_initial_sptr
        (new gen_phase_msg_impl(duration));
    }

    void gen_phase_msg_impl::read_phase(pmt::pmt_t pdu){
          // d_phase_prev = d_phase_in;
          float phase_in =  *((float*)pmt::blob_data(pmt::cdr(pdu))); // +0.0*d_phase_prev
          if (d_state == STATE_WAIT){
          d_phase = std::fmod(phase_in + F_PI, 2.0f * F_PI) - F_PI;
            // std::cout << "Received phase " << d_phase_in<<std::endl;
            // std::cout << "Phase received " <<  nitems_written(0)  << std::endl;
            d_state = STATE_SEND;
          }
    }

    /*
     * The private constructor
     */
    gen_phase_msg_impl::gen_phase_msg_impl(uint32_t duration)
      : gr::sync_block("gen_phase_msg",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_duration(duration),
      d_phase_det_mag(0.8),
      d_state(STATE_WAIT)
    {
      message_port_register_in(pmt::mp("phase_in"));
      set_msg_handler(pmt::mp("phase_in"), boost::bind(&gen_phase_msg_impl::read_phase, this, _1));
      set_output_multiple(d_duration);
    }


    /*
     * Our virtual destructor.
     */
    gen_phase_msg_impl::~gen_phase_msg_impl()
    {
    }

    int
    gen_phase_msg_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      gr_complex *out = (gr_complex *) output_items[0];

      // Do <+signal processing+>
      float oi, oq;
      if (d_state == STATE_SEND){
        //d_out = 1.0; //////////////////////////// delete
        // std::cout << std::endl <<  "Phase Tx " << d_phase << " ; ";

        int32_t angle = gr::fxpt::float_to_fixed (d_phase);
        gr::fxpt::sincos(angle, &oq, &oi);
        for (int i=0;i<d_duration;i++){
          out[i] = d_phase_det_mag*gr_complex(oi, oq);

        }
        d_state = STATE_WAIT;
        noutput_items = d_duration;


      }
      else{
        noutput_items = 0;
        usleep(SLEEP_WAITING_TIME_uS);// microseconds
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

