/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "first_trigger_impl.h"

namespace gr {
  namespace howto {

    first_trigger::sptr
    first_trigger::make(size_t pkt_len)
    {
      return gnuradio::get_initial_sptr
        (new first_trigger_impl(pkt_len));
    }

    /*
     * The private constructor
     */
    first_trigger_impl::first_trigger_impl(size_t pkt_len)
      : gr::sync_block("first_trigger",
              gr::io_signature::make(1, 1, sizeof(char)),
              gr::io_signature::make(1, 1, sizeof(char))),
      d_pkt_len(pkt_len),
      d_remain(0)
    {}

    /*
     * Our virtual destructor.
     */
    first_trigger_impl::~first_trigger_impl()
    {
    }

    int
    first_trigger_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const char *in = (const char *) input_items[0];
      char *out = (char *) output_items[0];

      for (int i = 0; i< noutput_items; i++){
        if (d_remain==0){
          if (in[i]==1){
            d_remain = d_pkt_len-1;
            out[i] = 1;
          }
          else{
            out[i]=0;
          }
        }
        else{
          out[i]=0;
          d_remain-- ;
        }

      }

      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

