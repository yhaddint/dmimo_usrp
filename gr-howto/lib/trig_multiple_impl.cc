/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "trig_multiple_impl.h"

#define STATE_LOOKING 0
#define STATE_COUNTING 1
#define STATE_SILENT 2

namespace gr {
  namespace howto {

    trig_multiple::sptr
    trig_multiple::make(uint32_t ntrig, uint32_t duration)
    {
      return gnuradio::get_initial_sptr
        (new trig_multiple_impl(ntrig, duration));
    }

    /*
     * The private constructor
     */
    trig_multiple_impl::trig_multiple_impl(uint32_t ntrig, uint32_t duration)
      : gr::sync_block("trig_multiple",
              gr::io_signature::make(1, 1, sizeof(char)),
              gr::io_signature::make(1, 1, sizeof(char))),
      d_duration(duration),
      d_ntrig(ntrig),
      d_state(STATE_LOOKING),
      d_packet_len(210000),
      d_remain(0),
      d_counter(0)
    {
      set_output_multiple(d_duration);
    }

    /*
     * Our virtual destructor.
     */
    trig_multiple_impl::~trig_multiple_impl()
    {
    }

    int
    trig_multiple_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const char *in = (const char *) input_items[0];
      char *out = (char *) output_items[0];
      uint32_t counter =0;
      uint32_t strt_i =0;
 

      memset(out, 0, noutput_items*sizeof(char));
      // std::cout << (int)d_state;

      if (d_state ==STATE_LOOKING || d_state == STATE_COUNTING){
        for (int i=0;i<noutput_items;i++){
            if (d_state == STATE_LOOKING){
              if (in[i]>0){    
                // std::cout << "Started Counting" <<std::endl;
                  d_state = STATE_COUNTING;
                  strt_i = i;
                  d_counter=1;
                  d_elapsed =1;
                }
              }
            else if (d_state==STATE_COUNTING){
                d_elapsed ++;
                if (in[i]>0){  
                // std::cout << "Incremneting counter" <<std::endl;  
                  d_counter+=1;
                  d_elapsed +=1;
                }
                if (d_counter>=d_ntrig){
                  for (int j = strt_i; j<=i;j++){
                    out[j] = in[j];
                  }
                  // std::cout << "Putting output" <<std::endl;  
                  d_counter =0;
                  d_elapsed = 0;
                  d_state = STATE_SILENT;
                  d_remain = d_packet_len;
                }
                else if (d_elapsed>d_duration){
                  d_counter--;
                  if (d_counter==0){
                    d_state = STATE_LOOKING;
                  }
                  noutput_items = strt_i +1;
                  // std::cout << "Timed out" <<std::endl; 
                  return  noutput_items;
                  
                }
              
            } 
        }
      }
     if (d_state==STATE_SILENT){
        // std::cout << "In silent" <<std::endl;
        if (noutput_items>=d_remain){
          noutput_items = d_remain;
          d_state = STATE_LOOKING;
          // std::cout << "Back to looking" <<std::endl;
        }
        d_remain = d_remain - noutput_items;
        // std::cout<<d_remain;

      }



      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }


  } /* namespace howto */
} /* namespace gr */

