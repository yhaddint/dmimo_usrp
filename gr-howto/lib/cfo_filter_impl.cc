/* -*- c++ -*- */
/* 
 * Copyright 2021 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "cfo_filter_impl.h"

namespace gr {
  namespace howto {

    cfo_filter::sptr
    cfo_filter::make(float samp_rate, int Ntrain, int Ndelay, float initial_frequency, std::vector<float> a, std::vector<float> b)
    {
      return gnuradio::get_initial_sptr
        (new cfo_filter_impl(samp_rate, Ntrain, Ndelay, initial_frequency, a, b));
    }

    /*
     * The private constructor
     */
    cfo_filter_impl::cfo_filter_impl(float samp_rate, int Ntrain, int Ndelay, float initial_frequency, std::vector<float> a, std::vector<float> b)
      : gr::sync_block("cfo_filter",
              gr::io_signature::make2(2, 2, sizeof(gr_complex),
                                      sizeof(uint8_t)),
              gr::io_signature::make(3, 3, sizeof(float))),
      d_samp_rate(samp_rate),
      d_Ntrain(Ntrain),
      d_Ndelay(Ndelay),
      //holdoff(t_holdoff * samp_rate),
      d_Tcycle(0),
      d_cycle_active(false),
      d_first_samp(true),
      d_a(a),
      d_b(b),
      d_v1m1(0), 
      d_v2m1(0)
    {

      perform_oneshot = false;
        cycle_samples = 0;


        Ts = 1/d_samp_rate;
        mag_iq = 1;

        cnt_samp = 0;
        cnt_train_samp = 0;
        corr_sum = 0;
        oneshot_cfo_est = 0;

        set_history(d_Ndelay + 1);

    }
    // https://stackoverflow.com/a/32534821
    double cfo_filter_impl::iirfilter(float x1,float y1) {
    y1 = (d_b[0] * x1 + d_v1m1) / d_a[0];
    d_v1m = (d_b[1] * x1 + d_v2m1) - d_a[1] * y1;
    d_v2m = d_b[2] * x1 - d_a[2] * y1;
    d_v1m1 = d_v1m;
    d_v2m1 = d_v2m;
    return y1;
    }


    /*
     * Our virtual destructor.
     */
    cfo_filter_impl::~cfo_filter_impl()
    {
    }

    int
    cfo_filter_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in_iq = (const gr_complex *) input_items[0];
      const uint8_t *in_trigger = (const uint8_t *) input_items[1];
      float *out_cfo_est = (float *) output_items[0];
      float *out_oneshotcfo_est = (float *) output_items[1];
      float *out_tcycle = (float *) output_items[2];
      // Do <+signal processing+>


      for (int i = 0; i < noutput_items; i++)
      {
          // one shot estimation

          //holdoff.tick();

          cycle_samples++;

          //if (in_trigger[i] && !last_trigger_level && holdoff.trigger())
          if (in_trigger[i])
          {  
             if (d_cycle_active == false){
                //std::cout << "New cycle started " << std::endl;
                d_cycle_active = true;

                perform_oneshot = true;
                cnt_samp = 0;
                cnt_train_samp = 0;
                corr_sum = 0;

                /* Update F with the delta in samples since the last oneshot
                 * estimation
                 */
                d_Tcycle = cycle_samples;
                cycle_samples = 0;
              }
          }

          out_tcycle[i] = d_Tcycle;


          //last_trigger_level = in_trigger[i];

          if(perform_oneshot)
          {
            // during T_train
            if ((cnt_samp > d_Ndelay) && (cnt_samp < d_Ntrain))
            {
              corr_sum = corr_sum + in_iq[i + d_Ndelay] * std::conj(in_iq[i]);
            } // end of T_train

            // trigger one-shot estimation and EKF updates
            if (cnt_samp == d_Ntrain - 1)
            {
              perform_oneshot = false;

              oneshot_cfo_est = std::arg(corr_sum) / (d_Ndelay * Ts * 2 * M_PI);


              // z is 3x1
              mag_iq = sqrt(real(in_iq[i]) * real(in_iq[i]) + imag(in_iq[i]) *
                            imag(in_iq[i]));
              if (mag_iq==0.){
                mag_iq = 1.;
              }

              // if (d_first_samp && d_cycle_active ){

              //   state.x_hat[1][0] = oneshot_cfo_est;
              //   d_first_samp = false;
              //   std::cout << "Set initial value " << oneshot_cfo_est << std::endl;

              // }

              filter_cfo_est=iirfilter(oneshot_cfo_est,filter_cfo_est);
              d_cycle_active = false;
              cnt_samp = 0;
              }
            }

          out_cfo_est[i] = filter_cfo_est;
          out_oneshotcfo_est[i] = oneshot_cfo_est;
          
          if (d_cycle_active){
            cnt_samp++;
          }
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

