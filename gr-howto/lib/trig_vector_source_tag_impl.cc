/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "trig_vector_source_tag_impl.h"
#include <stdio.h>

namespace gr {
  namespace howto {

    trig_vector_source_tag::sptr
    trig_vector_source_tag::make(const std::vector<gr_complex> &data,double sample_rate)
    {
      return gnuradio::get_initial_sptr
        (new trig_vector_source_tag_impl(data, sample_rate));
    }

    /*
     * The private constructor
     */
    trig_vector_source_tag_impl::trig_vector_source_tag_impl(const std::vector<gr_complex> &data,double sample_rate)
      : gr::block("trig_vector_source_tag",
              gr::io_signature::make(1, 1, sizeof(char)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_data(data),
      d_sample_rate(sample_rate),
      d_tmp(0),
      d_tx(0),
      d_offset(0)
    {
      d_offset = 0;

    }

    /*
     * Our virtual destructor.
     */
    trig_vector_source_tag_impl::~trig_vector_source_tag_impl()
    {
    }

    void
    trig_vector_source_tag_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */

     ninput_items_required[0] = 1000*noutput_items;
    }

    int
    trig_vector_source_tag_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const char *in = (const char *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];

      std::vector<tag_t> tags;

      // https://gnuradio.blogspot.com/2015/12/re-discuss-gnuradio-working-with-tags.html
        static const pmt::pmt_t TIME_KEY = pmt::string_to_symbol("rx_time");
          get_tags_in_range(
              tags, 0, nitems_read(0), nitems_read(0) + noutput_items);
          std::vector<tag_t>::iterator itr;
          size_t j = 0;
          for (itr = tags.begin(); itr != tags.end(); itr++, j++)
          {
            if (pmt::eq((*itr).key, TIME_KEY))
            {
              d_whole_seconds_strt = pmt::to_uint64(pmt::tuple_ref((*itr).value, 0));
              d_frac_seconds_strt = pmt::to_double(pmt::tuple_ref((*itr).value, 1));
              d_real_time_seconds = 
                  static_cast<double>(d_whole_seconds_strt) +
                  static_cast<double>(d_frac_seconds_strt);
              std::cerr << " tag_offset=" << (*itr).offset
                        << " noutput_items=" << noutput_items
                        << std::setprecision(std::numeric_limits<double>::digits10 + 1)
                        << " time_seconds=" << d_real_time_seconds << std::endl;

              
            }
          }

          //noutput_items = 10; //ninput_items[0];


      
      
      noutput_items = 0;   
      bool op_bl = false;
      uint32_t i;
      //std::cout << "first " << d_offset<< " " << ninput_items[0] << std::endl;

      for ( i = d_offset ; i < ninput_items[0];i++){
          if (in[i]==1){
            //std::cout << i << std::endl;
            noutput_items = d_data.size();

            d_frac_seconds_tmp = d_frac_seconds_strt + ((double)(nitems_read(0) + i + 40000 + 36))/d_sample_rate; // + 43937
            d_whole_seconds_tmp = d_whole_seconds_strt + (uint64_t)floor(d_frac_seconds_tmp);
            d_frac_seconds_tmp = d_frac_seconds_tmp - floor(d_frac_seconds_tmp);

            


            tag_t tag_ts;
            tag_ts.offset = nitems_written(0) ;
            tag_ts.key = pmt::mp("tx_time");
            tag_ts.value = pmt::make_tuple(
                pmt::from_uint64( d_whole_seconds_tmp  ),
                pmt::from_double(d_frac_seconds_tmp));
            add_item_tag(0, tag_ts);

                  
            tag_t tag_sob;
            tag_sob.offset = nitems_written(0) ;
            tag_sob.key = pmt::mp("tx_sob");
            tag_sob.value = pmt::PMT_T;
            add_item_tag(0, tag_sob);

            

            for (int j = 0; j< d_data.size();j++){
              out[j] = d_data[j];
            }


            tag_t tag_eob;
            tag_eob.offset = nitems_written(0)+ d_data.size() -1;
            tag_eob.key = pmt::mp("tx_eob");
            tag_eob.value = pmt::PMT_T;
            add_item_tag(0, tag_eob);
            noutput_items = d_data.size();


            if (i+d_data.size()>ninput_items[0]){
              consume_each (ninput_items[0]);
              d_offset =  (i+(uint32_t)d_data.size()) - (uint32_t)ninput_items[0] ;
            }
            else{
              d_offset = 0;
              consume_each (i+d_data.size());
            }
            op_bl=true;
            break;

          }

        }
        if (op_bl == false){
          d_offset = 0;
          consume_each (ninput_items[0]);
        }

        //std::cout << "Second " << d_offset<< " " << ninput_items[0] << std::endl;
          //d_frac_seconds_strt = d_frac_seconds_strt + ninput_items[0]/d_sample_rate;
          //d_whole_seconds_strt = d_whole_seconds_strt + (uint64_t)floor(d_frac_seconds_strt);
          //d_frac_seconds_strt = d_frac_seconds_strt - floor(d_frac_seconds_strt);
          
          
          //printf("%ld ",nitems_written(0));
          //printf("%ld %d %d %ld %ld\n", nitems_read(0),ninput_items[0], noutput_items,d_tmp,nitems_read(0) - d_tmp);
        /*
        for (int i=0;i<noutput_items;i++){
          out[i]=(gr_complex)0;
        }
        noutput_items = ninput_items[0];
      // Do <+signal processing+>
      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume_each (noutput_items);
      */

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

