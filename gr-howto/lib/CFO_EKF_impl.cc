/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "CFO_EKF_impl.h"
#include <vector>
#include <cassert>
#include <math.h>


#define ELEM(type,ptr,sr,sc,r,c) (*(((type*)(ptr)) + ((sc) * (r) + (c))))

namespace gr {
  namespace howto {

    template<class Tvv>
    bool check_dimension(Tvv v, int r, int c)
    {
        if(!v.size() == r)
            return false;
        for(typename Tvv::iterator iter = v.begin(); iter != v.end(); iter++)
        {
            if(iter->size() != c)
                return false;
        }
        return true;
    }

    void mmul(const void* mat_a, int mar, int mac,
              const void* mat_b, int mbr, int mbc,
              void* mat_o)
    {
        assert(mac == mbr);

        // inner product length
        int ipl = mac;

        #define AELEM(r,c) ELEM(float,mat_a,mar,mac,r,c)
        #define BELEM(r,c) ELEM(float,mat_b,mbr,mbc,r,c)
        #define OELEM(r,c) ELEM(float,mat_o,mar,mbc,r,c)

        for(int orow = 0; orow < mar; orow++)
        {
            for(int oc = 0; oc < mbc; oc++)
            {
                float accum = 0.0f;
                for(int ipi = 0; ipi < ipl; ipi++)
                {
                    accum += AELEM(orow,ipi) * BELEM(ipi,oc);
                }
                OELEM(orow,oc) = accum;
            }

        }

        #undef AELEM
        #undef BELEM
        #undef OELEM
    }

    void madd(const void* mat_a, const void* mat_b, int mr, int mc, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mr,mc,i,j) = ELEM(float,mat_a,mr,mc,i,j) +
                                              ELEM(float,mat_b,mr,mc,i,j);
    }

    void msub(const void* mat_a, const void* mat_b, int mr, int mc, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mr,mc,i,j) = ELEM(float,mat_a,mr,mc,i,j) -
                                              ELEM(float,mat_b,mr,mc,i,j);
    }

    void mtrans(const void* mat, int mr, int mc, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mc,mr,j,i) = ELEM(float,mat,mr,mc,i,j);
    }

    void m_s_mul(const void* mat, int mr, int mc, float m, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mr,mc,i,j) = ELEM(float,mat,mr,mc,i,j) * m;
    }

    void m_s_div(const void* mat, int mr, int mc, float d, void* mat_o)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat_o,mr,mc,i,j) = ELEM(float,mat,mr,mc,i,j) / d;
    }

    float mdet_2_2(const void* mat)
    {
        return ELEM(float,mat,2,2,0,0) * ELEM(float,mat,2,2,1,1) -
               ELEM(float,mat,2,2,0,1) * ELEM(float,mat,2,2,1,0);
    }

    float mdet_3_3(const void* mat)
    {
        float det =
            (ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,1,1) * ELEM(float,mat,3,3,2,2)) +
            (ELEM(float,mat,3,3,1,0) * ELEM(float,mat,3,3,2,1) * ELEM(float,mat,3,3,0,2)) +
            (ELEM(float,mat,3,3,2,0) * ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,1,2)) -
            (ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,2,1) * ELEM(float,mat,3,3,1,2)) -
            (ELEM(float,mat,3,3,2,0) * ELEM(float,mat,3,3,1,1) * ELEM(float,mat,3,3,0,2)) -
            (ELEM(float,mat,3,3,1,0) * ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,2,2));

        return det;
    }

    void minv_2_2(const void* mat, void* mat_o)
    {
        float det = mdet_2_2(mat);

        ELEM(float,mat_o,2,2,0,0) = ELEM(float,mat,2,2,1,1) / det;
        ELEM(float,mat_o,2,2,0,1) = ELEM(float,mat,2,2,0,1) / -det;
        ELEM(float,mat_o,2,2,1,0) = ELEM(float,mat,2,2,1,0) / -det;
        ELEM(float,mat_o,2,2,1,1) = ELEM(float,mat,2,2,0,0) / det;
    }

    void minv_3_3(const void* mat, void* mat_o)
    {
        float det = mdet_3_3(mat);

        float mat_i[3][3];

        ELEM(float,mat_i,3,3,0,0) = ELEM(float,mat,3,3,1,1) * ELEM(float,mat,3,3,2,2) -
                                    ELEM(float,mat,3,3,1,2) * ELEM(float,mat,3,3,2,1);
        ELEM(float,mat_i,3,3,0,1) = ELEM(float,mat,3,3,0,2) * ELEM(float,mat,3,3,2,1) -
                                    ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,2,2);
        ELEM(float,mat_i,3,3,0,2) = ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,1,2) -
                                    ELEM(float,mat,3,3,0,2) * ELEM(float,mat,3,3,1,1);
        ELEM(float,mat_i,3,3,1,0) = ELEM(float,mat,3,3,1,2) * ELEM(float,mat,3,3,2,0) -
                                    ELEM(float,mat,3,3,1,0) * ELEM(float,mat,3,3,2,2);
        ELEM(float,mat_i,3,3,1,1) = ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,2,2) -
                                    ELEM(float,mat,3,3,0,2) * ELEM(float,mat,3,3,2,0);
        ELEM(float,mat_i,3,3,1,2) = ELEM(float,mat,3,3,0,2) * ELEM(float,mat,3,3,1,0) -
                                    ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,1,2);
        ELEM(float,mat_i,3,3,2,0) = ELEM(float,mat,3,3,1,0) * ELEM(float,mat,3,3,2,1) -
                                    ELEM(float,mat,3,3,1,1) * ELEM(float,mat,3,3,2,0);
        ELEM(float,mat_i,3,3,2,1) = ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,2,0) -
                                    ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,2,1);
        ELEM(float,mat_i,3,3,2,2) = ELEM(float,mat,3,3,0,0) * ELEM(float,mat,3,3,1,1) -
                                    ELEM(float,mat,3,3,0,1) * ELEM(float,mat,3,3,1,0);

        m_s_div(mat_i, 3, 3, det, mat_o);
    }

    template<class Tvv>
    void mat_from_vec(const void* mat, int mr, int mc, Tvv vec)
    {
        for(int i = 0; i < mr; i++)
            for(int j = 0; j < mc; j++)
                ELEM(float,mat,mr,mc,i,j) = vec[i][j];
    }

    CFO_EKF::sptr
    CFO_EKF::make(float samp_rate,
                  int Ncycle,
                  int Ntrain,
                  float initial_frequency)
                  // std::vector<std::vector<float> > initial_P_mtx,
                  // std::vector<std::vector<float> > state_update_cov_matrix,
                  // std::vector<std::vector<float> > state_update_matrix,
                  // std::vector<std::vector<float> > one_shot_matrix)
    {
      // assert(check_dimension(initial_P_mtx, 2, 2));
      // assert(check_dimension(state_update_cov_matrix, 2, 2));
      // assert(check_dimension(state_update_matrix, 2, 2));
      // assert(check_dimension(one_shot_matrix, 3, 3));

      return gnuradio::get_initial_sptr
        (new CFO_EKF_impl(samp_rate, Ncycle, Ntrain, initial_frequency)); 
                          //initial_P_mtx,state_update_cov_matrix, state_update_matrix,one_shot_matrix));
    }

    /*
     * The private constructor
     */
    CFO_EKF_impl::CFO_EKF_impl( float samp_rate,
                                int Ncycle,
                                int Ntrain,
                               float initial_frequency)
                               //std::vector<std::vector<float> > initial_P_mtx,
                               //std::vector<std::vector<float> > state_update_cov_matrix,
                               //std::vector<std::vector<float> > state_update_matrix,
                               //std::vector<std::vector<float> > one_shot_matrix)
      : gr::sync_block("CFO_EKF",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 4, sizeof(float))),
      d_samp_rate(samp_rate),
      d_Ncycle(Ncycle),
      d_Ntrain(Ntrain)
    {
        // write some data in file for debugda   
        op_file.open("/home/han/Desktop/data/watchpoint",std::ofstream::binary);
        if (!op_file.is_open())
        std::cout << "Failed to open output file. Fail bit: " << op_file.fail() << ", bad bit: " << op_file.bad() << std::endl;
        

        cnt_write = 0;
        print_num = 1;
        pi = 3.14159265358979;
        Ts = 1/d_samp_rate;
        Tcycle = Ts*d_Ncycle;
        mag_iq = 1;
        RSS = 0;
        RSS_sum = 0;
         

        state.x_hat[0][0] = 0;
        state.x_hat[1][0] = initial_frequency;

        state.x_update[0][0] = 0;
        state.x_update[1][0] = initial_frequency;

        // initial P matrix
        state.P[0][0] = 1; state.P[0][1] = 0;
        state.P[1][0] = 0; state.P[1][1] = 1;
        

        // state update cov
        state.Q[0][0] = 0.0080*5; state.Q[0][1] = 0.2223*5;
        state.Q[1][0] = 0.2223*5; state.Q[1][1] = 7.684*5;
        

        // state update mtx
        state.F[0][0] = 1; state.F[0][1] = 2*pi*Tcycle;
        state.F[1][0] = 0; state.F[1][1] = 1;
        
        

        // Oneshot Est. Cov. matrix R 
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            state.R[i][j]=0;
          }
        }
        state.R[0][0] = 0.1;
        state.R[1][1] = 0.1;
        state.R[2][2] = 100;



        // mat_from_vec(state.P, 2, 2, initial_P_mtx);
        // mat_from_vec(state.Q, 2, 2, state_update_cov_matrix);
        // mat_from_vec(state.F, 2, 2, state_update_matrix);
        // mat_from_vec(state.R, 3, 3, one_shot_matrix);

        //minv_2_2(state.F,state.F_inv);
        mtrans(state.F,2,2,state.F_t);

        cnt_samp = 0;
        cnt_train_samp = 0;
        corr_sum = 0;
        oneshot_cfo_est = 0;

        set_history(201);
    }

    /*
     * Our virtual destructor.
     */
    CFO_EKF_impl::~CFO_EKF_impl()
    {
      if (op_file.is_open())
        op_file.close();
    }

    int
    CFO_EKF_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in_iq = (const gr_complex *) input_items[0];
      // const float *in_cfo_est = (const float *) input_items[1];
      float *out_phase = (float *) output_items[0];
      float *out_cfo_est = (float *) output_items[1];
      float *out_oneshotcfo_est = (float *) output_items[2];
      float *out_RSS = (float *) output_items[3];



      int counter = noutput_items;



      float i2_2_2[2][2];
      float i_2_1[2][1];
      float i_2_2[2][2];
      float i_2_3[2][3];
      float i_3_2[3][2];
      float i_3_3[3][3];
      float H_t[2][3];
      

      float ident_2_2[2][2] = {{1, 0},
                               {0, 1}};

      float P_update[2][2];

      for (int i = 0; i < noutput_items; i++){
          // one shot estimation
          cnt_samp++;
          
          // reset at end of T_cycle
          if (cnt_samp==d_Ncycle){
            cnt_samp = 0;
            cnt_train_samp = 0;
            corr_sum = 0;
            RSS_sum = 0;


          }

          // during T_train
          if (cnt_samp < d_Ntrain){
            corr_sum = corr_sum + in_iq[i+63] * std::conj(in_iq[i]);
            RSS_sum = RSS_sum + std::abs(in_iq[i]) * std::abs(in_iq[i]);
          } // end of T_train

            
          // trigger one-shot estimation and EKF updates
          if (cnt_samp ==d_Ntrain-1){

            cnt_write++;

            oneshot_cfo_est = std::arg(corr_sum)/(63*Ts*2*pi);
            RSS = RSS_sum;



            // z is 3x1
            mag_iq = sqrt(real(in_iq[i])*real(in_iq[i])+imag(in_iq[i])*imag(in_iq[i]));
            state.z[0][0] = real(in_iq[i])/mag_iq;
            state.z[1][0] = imag(in_iq[i])/mag_iq;
            state.z[2][0] = oneshot_cfo_est;
            if (cnt_write < print_num){
              // print z
              std::cout << 'z' << ":" << state.z[0][0] << ":" << state.z[1][0] << ":" << state.z[2][0] << std::endl;
            }

            float sxp = sin(state.x_hat[0][0]);
            float cxp = cos(state.x_hat[0][0]);

            // y_tilde is 3x1
            state.y_tilde[0][0] = state.z[0][0] - cxp;
            state.y_tilde[1][0] = state.z[1][0] - sxp;
            state.y_tilde[2][0] = state.z[2][0] - state.x_hat[1][0];
            if (cnt_write < print_num){
              // print y_tilde
              std::cout << 'y' << ":" << state.y_tilde[0][0] << ":" << state.y_tilde[1][0] << ":" << state.y_tilde[2][0] << std::endl;
            }

            // Hessian Matrix 3x2
            state.H[0][0] = -sxp; state.H[0][1] = 0;
            state.H[1][0] =  cxp; state.H[1][1] = 0;
            state.H[2][0] = 0;    state.H[2][1] = 1;
            if (cnt_write < print_num){
              // print H
              std::cout << 'H' << ":" << state.H[0][0] << ":" << state.H[0][1] << std::endl;
              std::cout << 'H' << ":" << state.H[1][0] << ":" << state.H[1][1] << std::endl;
              std::cout << 'H' << ":" << state.H[2][0] << ":" << state.H[2][1] << std::endl;
            }
            

            mmul(state.H,3,2,state.P,2,2,i_3_2);
            mtrans(state.H,3,2,H_t);
            mmul(i_3_2,3,2,H_t,2,3,i_3_3);
            madd(i_3_3,state.R,3,3,state.S);
            if (cnt_write < print_num){
              // print S
              std::cout << 'S' << ":" << state.S[0][0] << ":" << state.S[0][1] << ":" << state.S[0][2] << std::endl;
              std::cout << 'S' << ":" << state.S[1][0] << ":" << state.S[1][1] << ":" << state.S[1][2] << std::endl;
              std::cout << 'S' << ":" << state.S[2][0] << ":" << state.S[2][1] << ":" << state.S[2][2] << std::endl;
            }

            mmul(state.P,2,2,H_t,2,3,i_2_3);
            minv_3_3(state.S,i_3_3);
            mmul(i_2_3,2,3,i_3_3,3,3,state.K);
            if (cnt_write < print_num){
              // print K
              std::cout << 'K' << state.K[0][0] << ":" << state.K[0][1] << ":" << state.K[0][2] << std::endl;
              std::cout << 'K' << state.K[1][0] << ":" << state.K[1][1] << ":" << state.K[1][2] << std::endl;
            }

            mmul(state.K,2,3,state.y_tilde,3,1,i_2_1);
            madd(i_2_1,state.x_hat,2,1,state.x_update);

            if (cnt_write < print_num){
              // print state.x_update
              std::cout << 'x' << ":" << state.x_update[0][0] << std::endl;
              std::cout << 'x' << ":" << state.x_update[1][0] << std::endl;
            }

            mmul(state.K,2,3,state.H,3,2,i_2_2);
            if (cnt_write < print_num){
              // print P_update
              std::cout << 'K' <<'H' << i_2_2[0][0] << ":" << i_2_2[0][1] << std::endl;
              std::cout << 'K' <<'H' << i_2_2[1][0] << ":" << i_2_2[1][1] << std::endl;
            }
            msub(ident_2_2,i_2_2,2,2,i2_2_2);
            if (cnt_write < print_num){
              // print P_update
              std::cout << 'I' << i2_2_2[0][0] << ":" << i2_2_2[0][1] << std::endl;
              std::cout << 'I' << i2_2_2[1][0] << ":" << i2_2_2[1][1] << std::endl;
            }
            mmul(i2_2_2,2,2,state.P,2,2,P_update);
            if (cnt_write < print_num){
              // print P_update
              std::cout << 'P' << P_update[0][0] << ":" << P_update[0][1] << std::endl;
              std::cout << 'P' << P_update[1][0] << ":" << P_update[1][1] << std::endl;
            }

            mmul(state.F,2,2,state.x_update,2,1,state.x_hat);
            if (cnt_write < print_num){
              // print state.x_update
              std::cout << 'x' << ":" << state.x_hat[0][0] << std::endl;
              std::cout << 'x' << ":" << state.x_hat[1][0] << std::endl;
            }


            mmul(state.F,2,2,P_update,2,2,i_2_2);
            mmul(i_2_2,2,2,state.F_t,2,2,i2_2_2);
            madd(state.Q,i2_2_2,2,2,state.P);
            if (cnt_write < print_num){
              // print Q
              std::cout << 'P' << state.P[0][0] << ":" << state.P[0][1] << ":" << std::endl;
              std::cout << 'P' << state.P[1][0] << ":" << state.P[1][1] << ":" << std::endl;
            }
            // write data into file for 10 times
            
            if (cnt_write < print_num){
              if (op_file.is_open())
              {   
                  // write state.x_update
                  int tmp;
                  tmp = 2; // Say first dimension has size 3
                  op_file.write((char*) &tmp, sizeof(int));
                  tmp = 1;  // Say second dimension has size 2
                  op_file.write((char*) &tmp, sizeof(int));
                  // Now write matrix
                  op_file.write((char*) state.x_update, 2 * 1 * sizeof(float));

                  // write P matrix
                  tmp = 2; // Say first dimension has size 3
                  op_file.write((char*) &tmp, sizeof(int));
                  tmp = 2;  // Say second dimension has size 2
                  op_file.write((char*) &tmp, sizeof(int));
                  // Now write matrix
                  op_file.write((char*) state.P, 2 * 2 * sizeof(float));
                  
              }
            }

            if (cnt_write==print_num){
              if (op_file.is_open())
              {
                   int tmp = -1;
                   op_file.write((char*) &tmp, sizeof(int));
                   op_file.flush();
              }
            }



          } //end of CFO est. pred. action

          out_phase[i] = state.x_update[0][0];
          out_cfo_est[i] = state.x_update[1][0];
          out_oneshotcfo_est[i] = state.z[2][0];
          out_RSS[i] = RSS;

      }
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

