/* -*- c++ -*- */
/* 
 * Copyright 2019 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_LOG_MIMO_IMPL_H
#define INCLUDED_HOWTO_LOG_MIMO_IMPL_H

#include <howto/log_mimo.h>

namespace gr {
  namespace howto {

    class log_mimo_impl : public log_mimo
    {
     private:
      // Nothing to declare in this block.
      uint32_t d_delay;
      uint32_t d_downsample;
      uint32_t d_remain ;
      uint32_t d_caplen;
      char d_state;
      uint32_t d_downsample_offset;

     public:
      log_mimo_impl(uint32_t delay, uint32_t downsample, uint32_t caplen);
      ~log_mimo_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_LOG_MIMO_IMPL_H */

