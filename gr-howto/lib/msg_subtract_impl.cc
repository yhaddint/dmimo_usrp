/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define F_PI ((float)(M_PI))


#include <gnuradio/io_signature.h>
#include "msg_subtract_impl.h"

namespace gr {
  namespace howto {

    msg_subtract::sptr
    msg_subtract::make()
    {
      return gnuradio::get_initial_sptr
        (new msg_subtract_impl());
    }

    /*
     * The private constructor
     */
    msg_subtract_impl::msg_subtract_impl()
      : gr::sync_block("msg_subtract",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(0, 0, 0)), 
      d_phase1_flag(0),
      d_phase2_flag(0)
    {
      message_port_register_in(pmt::mp("phase_in1"));
      message_port_register_in(pmt::mp("phase_in2"));
      set_msg_handler(pmt::mp("phase_in1"), boost::bind(&msg_subtract_impl::read_phase1, this, _1));
      set_msg_handler(pmt::mp("phase_in2"), boost::bind(&msg_subtract_impl::read_phase2, this, _1));
      message_port_register_out(pmt::mp("phase_out"));
    }

    void msg_subtract_impl::read_phase1(pmt::pmt_t pdu){
      // d_phase_prev = d_phase_in;
      float phase_in;
      phase_in =  *((float*)pmt::blob_data(pmt::cdr(pdu))); // +0.0*d_phase_prev
      phase_in = std::fmod(phase_in + F_PI, 2.0f * F_PI) - F_PI;
      d_phase1_flag = 1;
      d_phase1 = phase_in;
      if (d_phase1_flag && d_phase2_flag){
        d_phase1_flag = 0;
        d_phase2_flag = 0;
        float phase_out = d_phase1 - d_phase2;
        phase_out = std::fmod(phase_out + F_PI, 2.0f * F_PI) - F_PI;
        message_port_pub(pmt::mp("phase_out"),  pmt::cons(pmt::PMT_NIL,pmt::make_blob(&phase_out,sizeof(float))));
      }
    }

   void msg_subtract_impl::read_phase2(pmt::pmt_t pdu){
      // d_phase_prev = d_phase_in;
      float phase_in;
      phase_in =  *((float*)pmt::blob_data(pmt::cdr(pdu))); // +0.0*d_phase_prev
      phase_in = std::fmod(phase_in + F_PI, 2.0f * F_PI) - F_PI;
      d_phase2_flag = 1;
      d_phase2 = phase_in;
      if (d_phase1_flag && d_phase2_flag){
        d_phase1_flag = 0;
        d_phase2_flag = 0;
        float phase_out = d_phase1 - d_phase2;
        phase_out = std::fmod(phase_out + F_PI, 2.0f * F_PI) - F_PI;
        message_port_pub(pmt::mp("phase_out"),  pmt::cons(pmt::PMT_NIL,pmt::make_blob(&phase_out,sizeof(float))));
      }
    }

    /*
     * Our virtual destructor.
     */
    msg_subtract_impl::~msg_subtract_impl()
    {
    }

    int
    msg_subtract_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

