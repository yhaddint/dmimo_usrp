/* -*- c++ -*- */
/* 
 * Copyright 2020 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_RECIP_SOURCE_IMPL_H
#define INCLUDED_HOWTO_RECIP_SOURCE_IMPL_H

#include <howto/recip_source.h>

namespace gr {
  namespace howto {

    class recip_source_impl : public recip_source
    {
     private:
      // Nothing to declare in this block.
      std::vector<gr_complex> d_data;
      uint16_t d_delay1; 
      uint16_t d_dur1;
      float d_mag1;
      uint16_t d_delay2; 
      uint16_t d_dur2;
      float d_mag2;
      double d_phase;
      bool d_phase_rec;
      uint16_t d_trig;
      uint16_t d_state;
      uint16_t d_wait;
      uint16_t d_remain;
      void det_trig(pmt::pmt_t pdu);
      void read_phase(pmt::pmt_t pdu);

     public:
      recip_source_impl(const std::vector<gr_complex> &data, uint16_t delay1, uint16_t dur1, float mag1, uint16_t delay2, uint16_t dur2, float mag2);
      ~recip_source_impl();


      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_RECIP_SOURCE_IMPL_H */

