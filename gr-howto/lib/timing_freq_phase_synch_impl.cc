/* -*- c++ -*- */
/* 
 * Copyright 2018 CORES.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <gnuradio/fxpt.h>
#include "timing_freq_phase_synch_impl.h"

#define F_PI ((float)(M_PI))


namespace gr {
  namespace howto {
    

    timing_freq_phase_synch::sptr
    timing_freq_phase_synch::make(size_t timing_margin, size_t data_length, double sample_rate, float freq_scaler, bool usrp)
    {
      
      return gnuradio::get_initial_sptr
        (new timing_freq_phase_synch_impl(timing_margin, data_length, sample_rate, freq_scaler, usrp));
    }

    /*
     * The private constructor
     */
   
static std::vector<int> get_input_sizes(){
    std::vector<int> input_sizes;
    input_sizes.push_back(sizeof(char));
      input_sizes.push_back( sizeof(float));
      input_sizes.push_back(sizeof(float));
      input_sizes.push_back(sizeof(gr_complex));

    return input_sizes;
    }
    timing_freq_phase_synch_impl::timing_freq_phase_synch_impl(size_t timing_margin, size_t data_length, double sample_rate, float freq_scaler, bool usrp)
      : gr::block("timing_freq_phase_synch",
              gr::io_signature::makev(4, 4, get_input_sizes() ),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_sample_rate(sample_rate),
      d_offset(0),
      d_timing_margin(timing_margin),
      d_data_length(data_length),
      d_remain_data(0), 
      d_freq_scaler(freq_scaler), 
      d_usrp(usrp)
    {
      set_output_multiple(d_data_length);
    }

    /*
     * Our virtual destructor.
     */
    timing_freq_phase_synch_impl::~timing_freq_phase_synch_impl()
    {
    }

    void
    timing_freq_phase_synch_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */
      ninput_items_required[0] = noutput_items;
      ninput_items_required[1] = noutput_items;
      ninput_items_required[2] = noutput_items;
      ninput_items_required[3] = noutput_items;
    }



    int
    timing_freq_phase_synch_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {

      char trig_indx = 0;
      char freq_indx = 1;
      char phase_indx = 2;
      char data_indx = 3;

      const char *in_trig = (const char *) input_items[trig_indx];
      const float *in_freq  = (const float *) input_items[freq_indx];
      const float *in_phase  = (const float *) input_items[phase_indx];
      const gr_complex *in_data  = (const gr_complex *) input_items[data_indx];

      float oi, oq;

      gr_complex *out = (gr_complex *) output_items[0];

      if (d_usrp){
        std::vector<tag_t> tags;
        // Read timing tag from USRP
        // https://gnuradio.blogspot.com/2015/12/re-discuss-gnuradio-working-with-tags.html
          static const pmt::pmt_t TIME_KEY = pmt::string_to_symbol("rx_time");
            get_tags_in_range(
                tags, trig_indx, nitems_read(trig_indx), nitems_read(trig_indx) + noutput_items);
            std::vector<tag_t>::iterator itr;
            size_t j = 0;
            for (itr = tags.begin(); itr != tags.end(); itr++, j++)
            {
              if (pmt::eq((*itr).key, TIME_KEY))
              {
                d_whole_seconds_strt = pmt::to_uint64(pmt::tuple_ref((*itr).value, 0));
                d_frac_seconds_strt = pmt::to_double(pmt::tuple_ref((*itr).value, 1));
                d_real_time_seconds = 
                    static_cast<double>(d_whole_seconds_strt) +
                    static_cast<double>(d_frac_seconds_strt);
                std::cerr << " tag_offset=" << (*itr).offset
                          << " noutput_items=" << noutput_items
                          << std::setprecision(std::numeric_limits<double>::digits10 + 1)
                          << " time_seconds=" << d_real_time_seconds << std::endl;

                
              }
            }
          }

          //noutput_items = 10; //ninput_items[0];


      // std::cout << "Required output " << noutput_items << std::endl << " Inputs  "
      // << ninput_items[0] << " " 
      //          << ninput_items[1] << " " << ninput_items[2] << " " <<
      //          ninput_items[3] <<std::endl <<
      //          "Remain data " << d_remain_data << std::endl;
      
      noutput_items = 0;   
      bool op_bl = false; // We have output
      uint32_t i;
      uint32_t trig_loc;
      //// std::cout << "first " << d_offset<< " " << ninput_items[0] << std::endl;
      
      // If no data remaining from previous call
      if (d_remain_data == 0){
        // Look for trigger
        for ( i = d_offset ; i < ninput_items[trig_indx];i++){
            if (in_trig[i]==1){
              trig_loc = i;
              // If trigger found
              // std::cout << "Trigger found " <<std::endl;

              //// std::cout << i << std::endl;
              // We are outputting data_length
              noutput_items = d_data_length;

              if (d_usrp){
                // Calculate trigger time plus margin - > time of next transmission
                d_frac_seconds_tmp = d_frac_seconds_strt + ((double)(nitems_read(trig_indx) + i + d_timing_margin))/d_sample_rate; // + 43937
                d_whole_seconds_tmp = d_whole_seconds_strt + (uint64_t)floor(d_frac_seconds_tmp);
                d_frac_seconds_tmp = d_frac_seconds_tmp - floor(d_frac_seconds_tmp);

                

              // Write timing tag
              
                tag_t tag_ts;
                tag_ts.offset = nitems_written(trig_indx) ;
                tag_ts.key = pmt::mp("tx_time");
                tag_ts.value = pmt::make_tuple(
                    pmt::from_uint64( d_whole_seconds_tmp  ),
                    pmt::from_double(d_frac_seconds_tmp));
                add_item_tag(trig_indx, tag_ts);

                // Write start of burst               
                tag_t tag_sob;
                tag_sob.offset = nitems_written(trig_indx) ;
                tag_sob.key = pmt::mp("tx_sob");
                tag_sob.value = pmt::PMT_T;
                add_item_tag(trig_indx, tag_sob);
            }
              d_remain_data = d_data_length;

              // Here we are only estimating how much to consume from trigger port
              // Actual transmission is and consumption of data in next if
              // I we can't send th whole data length this  time
              if (trig_loc+d_data_length>ninput_items[trig_indx]){
                // Consume as much as you can and leave the rest for the next call
                consume(trig_indx,ninput_items[trig_indx]);
                d_offset =  (trig_loc+(uint32_t)d_data_length) - (uint32_t)ninput_items[0] ;
                // std::cout << "Consumed all trigger offset " << d_offset << std::endl;
              }
              else{
                // Else we will send data length and return
                d_offset = 0;
                consume(trig_indx, trig_loc+d_data_length);
                // std::cout << "Consumed trigger " << trig_loc+d_data_length << std::endl;
              }
              // We have data stop looking for trigger
              op_bl=true;
              break;
            }
          }
          // We did not find a trigger, just read all input on trigger
          if (op_bl == false){
            d_offset = 0;
            size_t max_poss_t = std::min(ninput_items[trig_indx],std::min(ninput_items[freq_indx],ninput_items[phase_indx]));
            consume (trig_indx,max_poss_t);
            consume (freq_indx,max_poss_t);
            consume (phase_indx,max_poss_t);
            // std::cout << "Consumed all Inputs " << max_poss_t << std::endl;
          }
        }

        // If there is data remaining from this or previous call try to send it
        if (d_remain_data > 0) {

          // If we have enough input on input 1
          
          size_t remain_freq = ninput_items[freq_indx] - trig_loc;
          size_t remain_phase = ninput_items[phase_indx] - trig_loc;
          size_t remain_data = ninput_items[data_indx];
          size_t max_possible = std::min(std::min(remain_freq,remain_phase),remain_data );

          if ( max_possible >= d_data_length){
                // send the whole thing 
                noutput_items = d_data_length;
              }
              else{
                // send as much as you can
                noutput_items = max_possible;
              }
              // if starting packet sample phase
              if (d_remain_data == d_data_length){
                d_phase = in_phase[trig_loc];
              }
              // consume data
              for(int i = 0; i < noutput_items ; i++) {
                  d_phase = d_phase + 1/d_sample_rate*2*F_PI * in_freq[trig_loc +i];
                  d_phase   = std::fmod(d_phase + F_PI, 2.0f * F_PI) - F_PI; //place phase in [-pi, +pi[
                  int32_t angle = gr::fxpt::float_to_fixed (d_phase);
                  gr::fxpt::sincos(angle, &oq, &oi);
                  out[i] = in_data[i] *gr_complex(oi, oq);
              }

               d_remain_data = std::max((int)d_data_length - (int)max_possible,0);

              consume(data_indx,noutput_items);
              consume (freq_indx,trig_loc+noutput_items);
              consume (phase_indx,trig_loc+noutput_items);
              // std::cout << "Consumed data "<< noutput_items << " freq phase " << trig_loc+noutput_items<< std::endl;

        }
        // if no data remaining and there is an output end burst
        if (d_remain_data == 0 && noutput_items>0){
              if (d_usrp){
                tag_t tag_eob;
                tag_eob.offset = nitems_written(0)+ noutput_items -1;
                tag_eob.key = pmt::mp("tx_eob");
                tag_eob.value = pmt::PMT_T;              
                add_item_tag(0, tag_eob);
              }

                d_phase = 0;
          }
        // std::cout << noutput_items << std::endl;
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

