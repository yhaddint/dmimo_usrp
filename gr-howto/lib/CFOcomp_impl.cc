/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "CFOcomp_impl.h"

#define _USE_MATH_DEFINES

#include <math.h>

namespace gr {
  namespace howto {

    CFOcomp::sptr
    CFOcomp::make(float samp_rate, float freq_scaler)
    {
      return gnuradio::get_initial_sptr
        (new CFOcomp_impl(samp_rate,freq_scaler));
    }

    /*
     * The private constructor
     */
    CFOcomp_impl::CFOcomp_impl(float samp_rate, float freq_scaler)
      : gr::block("CFOcomp",
              gr::io_signature::make(1, 1, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_samp_rate(samp_rate),
      d_freq_scaler(freq_scaler)
    {
      Ts = 1/d_samp_rate;
      newphase = 0;
      oldphase = 0;
    }

    /*
     * Our virtual destructor.
     */
    CFOcomp_impl::~CFOcomp_impl()
    {
    }

    void
    CFOcomp_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */
    }

    int
    CFOcomp_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const float *in = (const float *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];
      for (int i = 1; i < noutput_items; i++){
        newphase = fmod(oldphase + in[i]*Ts*2*M_PI*d_freq_scaler, 2*M_PI);
        out[i] = gr_complex(std::cos(newphase),std::sin(newphase));
        oldphase = newphase;
      }

      // Do <+signal processing+>
      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume_each (noutput_items);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

