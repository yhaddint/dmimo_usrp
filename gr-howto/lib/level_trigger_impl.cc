/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "level_trigger_impl.h"

namespace gr {
  namespace howto {

    level_trigger::sptr
    level_trigger::make(uint32_t hold, float trig)
    {
      return gnuradio::get_initial_sptr
        (new level_trigger_impl(hold, trig));
    }

    /*
     * The private constructor
     */
    level_trigger_impl::level_trigger_impl(uint32_t hold, float trig)
      : gr::sync_block("level_trigger",
              gr::io_signature::make(1, 1, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(char))), 
      d_hold(hold),
      d_trig(trig),
      d_remain(0)
    {}

    /*
     * Our virtual destructor.
     */
    level_trigger_impl::~level_trigger_impl()
    {
    }

    int
    level_trigger_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const float *in = (const float *) input_items[0];
      char *out = (char *) output_items[0];
      uint32_t trig_loc = 0;

      for (int i = 0 ; i<noutput_items;i++){
        out[i]=0;
      }
      // Do <+signal processing+>
      if (d_remain<=0){
        for (int i = 0;i<=noutput_items;i++){
          if (in[i]>d_trig){
            d_remain = d_hold;
            trig_loc = i;
            out[i] = 1;
            break;
          }

        }
      }
      if (d_remain>0){
        if ((noutput_items-trig_loc)>=d_remain){
          noutput_items = trig_loc + d_remain;
        }
        d_remain = d_remain - (noutput_items-trig_loc);


      }
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

