/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "trig_vector_source_impl.h"

namespace gr {
  namespace howto {

    trig_vector_source::sptr
    trig_vector_source::make(const std::vector<gr_complex> &data)
    {
      return gnuradio::get_initial_sptr
        (new trig_vector_source_impl(data));
    }

    /*
     * The private constructor
     */
    trig_vector_source_impl::trig_vector_source_impl(const std::vector<gr_complex> &data)
      : gr::sync_block("trig_vector_source",
              gr::io_signature::make(1, 1, sizeof(char)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_data(data),
      d_offset(0),
      d_tx(0)
    {}

    /*
     * Our virtual destructor.
     */
    trig_vector_source_impl::~trig_vector_source_impl()
    {
    }

    int
    trig_vector_source_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const char *in = (const char *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];


      // Do <+signal processing+>
      gr_complex op;
      for (int i = 0 ; i < noutput_items;i++){
        op=0;
        if (d_tx==1){
          if (d_offset<d_data.size()){
            op=d_data[d_offset];
            d_offset=d_offset+1;
          }
          else{
            d_tx=0;
          }

        }
        else {
          if (in[i]==1){
            d_tx=1;
            d_offset=0;
          }
        }

      out[i]=op;

      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

