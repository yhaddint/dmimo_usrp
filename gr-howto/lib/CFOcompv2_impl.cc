/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "CFOcompv2_impl.h"

#define _USE_MATH_DEFINES


namespace gr {
  namespace howto {

    CFOcompv2::sptr
    CFOcompv2::make(float samp_rate, float freq_scaler)
    {
      return gnuradio::get_initial_sptr
        (new CFOcompv2_impl(samp_rate, freq_scaler));
    }

    /*
     * The private constructor
     */
    CFOcompv2_impl::CFOcompv2_impl(float samp_rate, float freq_scaler)
      : gr::sync_block("CFOcompv2",
              gr::io_signature::make(1, 2, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_samp_rate(samp_rate),
      d_freq_scaler(freq_scaler)
    {
      Ts = 1/d_samp_rate;
      newphase = 0;
      oldphase = 0;
      samp_cnt = 0;
      set_history(1);
    }

    /*
     * Our virtual destructor.
     */
    CFOcompv2_impl::~CFOcompv2_impl()
    {
    }

    int
    CFOcompv2_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const float *in_phase = (const float *) input_items[0];
      const float *in_freq = (const float *) input_items[1];

      gr_complex *out = (gr_complex *) output_items[0];

      for (int i = 1; i < noutput_items; i++){
        if (in_phase[i]!=in_phase[i+1]){
          samp_cnt = 0;
        }
        oldphase = in_phase[i+1] + (samp_cnt) * in_freq[i+1] * Ts * 2 * M_PI;
        newphase = fmod(oldphase * d_freq_scaler, 2*M_PI);
        out[i] = gr_complex(std::cos(newphase),std::sin(newphase));
        samp_cnt++;
      }

      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

