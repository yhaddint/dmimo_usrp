/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "CFOestsync_impl.h"

namespace gr {
  namespace howto {

    CFOestsync::sptr
    CFOestsync::make(int update_rate, float samp_rate)
    {
      return gnuradio::get_initial_sptr
        (new CFOestsync_impl(update_rate, samp_rate));
    }

    /*
     * The private constructor
     */
    CFOestsync_impl::CFOestsync_impl(int update_rate, float samp_rate )
      : gr::block("CFOestsync",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(float))),
        d_update_rate(update_rate),
        d_samp_rate(samp_rate)
    {
      set_history(201);
      CFO = 0;
      corr_sum = 0;
      samp_cnt = 0;
    }

    /*
     * Our virtual destructor.
     */
    CFOestsync_impl::~CFOestsync_impl()
    {
    }

    void
    CFOestsync_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */
    }

    int
    CFOestsync_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      float *out = (float *) output_items[0];

      for (int i = 0; i < noutput_items; i++){
        samp_cnt++;
        if (samp_cnt < d_update_rate){
          out[i] = 0;
        }
        else{
          corr_sum = corr_sum + in[i+200] * std::conj(in[i]);
          out[i] = CFO*d_samp_rate/2/3.14159265358979; 
          if (samp_cnt % d_update_rate == 0){
            CFO = std::arg(corr_sum)/200;
            corr_sum = 0;
          }
        }
      }

      // Do <+signal processing+>
      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume_each (noutput_items);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

