/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_FRAC_TIMING_FREQ_PHASE_SYNCH_IMPL_H
#define INCLUDED_HOWTO_FRAC_TIMING_FREQ_PHASE_SYNCH_IMPL_H

#include <howto/frac_timing_freq_phase_synch.h>

namespace gr {
  namespace howto {

    class frac_timing_freq_phase_synch_impl : public frac_timing_freq_phase_synch
    {
     private:
      // Nothing to declare in this block.
      double d_sample_rate;
      double d_real_time_seconds;
      uint64_t d_whole_seconds_strt;
      double d_frac_seconds_strt;
      uint64_t d_whole_seconds_lst;
      double d_frac_seconds_lst;
      uint32_t d_offset;
      size_t d_timing_margin;
      size_t d_timing_margin2;
      float d_prev_phase;

      size_t d_data_length;
      size_t d_remain_data;


      int d_state;
      float d_phase;
      float d_freq;
      bool d_usrp;
      float d_freq_scaler;

      float d_updated_phase;


      uint32_t d_oversample;
      std::vector<gr_complex> d_zc_iq;
      double d_frac_correction;
      void consume_synch(size_t consm);
      void estimate_fractional_time(gr_complex * zc_sig_iq,int * best_sample_corr01,int *best_sample_ind);
      size_t get_available_synch(gr_vector_int &ninput_items);
      size_t get_available_trig(gr_vector_int &ninput_items);

     public:
      frac_timing_freq_phase_synch_impl(size_t timing_margin, size_t data_length, double sample_rate, float freq_scaler, uint32_t oversample, bool usrp);
      ~frac_timing_freq_phase_synch_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_frac_timing_freq_phase_synch_impl_H */

