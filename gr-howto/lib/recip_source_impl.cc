/* -*- c++ -*- */
/* 
 * Copyright 2020 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#define F_PI ((float)(M_PI))

#define STATE_WAITING_TRIG 0
#define STATE_TX0 1
#define STATE_WAITING_PHASE_TX 2
#define STATE_WAITING_PHASE_BLOCK 3
#define STATE_TX1 4
#define STATE_TX2 5

#define SLEEP_WAITING_TIME_uS 100
#define WAIT_MAX 1000

#include <gnuradio/io_signature.h>
#include <gnuradio/fxpt.h>
#include "recip_source_impl.h"

namespace gr {
  namespace howto {

    recip_source::sptr
    recip_source::make(const std::vector<gr_complex> &data, uint16_t delay1, uint16_t dur1, float mag1, uint16_t delay2, uint16_t dur2, float mag2)
    {
      return gnuradio::get_initial_sptr
        (new recip_source_impl(data, delay1, dur1, mag1, delay2, dur2, mag2));
    }



    void recip_source_impl::det_trig(pmt::pmt_t pdu){
          if (d_state == STATE_WAITING_TRIG){
            d_state = STATE_TX0;
            d_remain = d_data.size();
          }
          else{
            // std::cout << "Got phase in wrong state " << d_state << std::endl; 
          }
    }

    void recip_source_impl::read_phase(pmt::pmt_t pdu){
          // d_phase_prev = d_phase_in;

          d_phase =  *((float*)pmt::blob_data(pmt::cdr(pdu))); // +0.0*d_phase_prev
          d_phase = std::fmod(d_phase + F_PI, 2.0f * F_PI) - F_PI;
          d_phase_rec = 1;
          if (d_state==STATE_WAITING_PHASE_BLOCK){
            d_state = STATE_TX1;
            d_remain = d_dur1;
            d_wait = 0;
            // std::cout << "Phase: STATE_WAITING_PHASE_BLOCK-> STATE_TX1" << std::endl;
          }
          // std::cout << "Received phase " << d_phase_in<<std::endl;
          // std::cout << "Phase received " <<  nitems_written(0)  << std::endl;
    }

    /*
     * The private constructor
     */
    recip_source_impl::recip_source_impl(const std::vector<gr_complex> &data, uint16_t delay1, uint16_t dur1, float mag1, uint16_t delay2, uint16_t dur2, float mag2)
      : gr::sync_block("recip_source",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1,sizeof(gr_complex))),
    d_data(data),
    d_delay1(delay1), 
    d_dur1(dur1),
    d_mag1(mag1),
    d_delay2(delay2),
    d_dur2(dur2),
    d_mag2(mag2),
    d_state(STATE_WAITING_TRIG),
    d_trig(0),
    d_phase(0),
    d_remain(0),
    d_wait(0),
    d_phase_rec(0)
    {

      message_port_register_in(pmt::mp("trig_in"));
      set_msg_handler(pmt::mp("trig_in"), boost::bind(&recip_source_impl::det_trig, this, _1));

      message_port_register_in(pmt::mp("phase_in"));
      set_msg_handler(pmt::mp("phase_in"), boost::bind(&recip_source_impl::read_phase, this, _1));

    }
    

    /*
     * Our virtual destructor.
     */
    recip_source_impl::~recip_source_impl()
    {
    }

    int
    recip_source_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      gr_complex *out = (gr_complex *) output_items[0];

      unsigned n = 0;

      if (d_state == STATE_WAITING_TRIG) {
        usleep(SLEEP_WAITING_TIME_uS);// microseconds
      }


      if (d_state == STATE_TX0) {
        tag_t tag_sob;
        tag_sob.offset = nitems_written(0) ;
        tag_sob.key = pmt::mp("tx_sob");
        tag_sob.value = pmt::PMT_T;
        add_item_tag(0, tag_sob);


        n = std::min(d_remain,(uint16_t) noutput_items);
        uint16_t offset = d_data.size() -d_remain;
        for (int i = 0;i<n;i++){
          out[i] = d_data[i+offset];
        }
        d_remain = d_remain - n;
        if (d_remain == 0){
          d_state = STATE_WAITING_PHASE_TX;
          d_remain = d_delay1;
        }
      }
      else if (d_state == STATE_WAITING_PHASE_TX) {
        n = std::min(d_remain, (uint16_t) noutput_items);
        // std::cout << d_remain << std::endl;

        for (int i = 0;i<n;i++){
          out[i] = 0;
        }
        d_remain = d_remain - n;
        if (d_remain == 0){
          if (d_phase_rec == 0){
            d_state = STATE_WAITING_PHASE_BLOCK;
            // std::cout << "STATE_WAITING_PHASE_TX-> STATE_WAITING_PHASE_BLOCK" << std::endl;
          }
          else{
            // std::cout << "STATE_WAITING_PHASE_TX-> STATE_TX1" << std::endl;
            d_state = STATE_TX1;
            d_remain = d_dur1;
          }
        }
      }
      else if (d_state == STATE_WAITING_PHASE_BLOCK){
        usleep(SLEEP_WAITING_TIME_uS);// microseconds
        d_wait ++;
        if (d_wait>WAIT_MAX){
          d_wait=0;
          d_remain =0;
          d_state = STATE_WAITING_TRIG;
        }
      }
      else if (d_state == STATE_TX1) {
          n = std::min(d_remain, (uint16_t) noutput_items);
          float oi, oq;
          int32_t angle = gr::fxpt::float_to_fixed (d_phase);
          // std::cout << d_phase << std::endl;
          
          gr::fxpt::sincos(angle, &oq, &oi);
          for (int i = 0;i<n;i++){
            out[i] = gr_complex(d_mag1*oi,d_mag1* oq);
          }
          d_remain = d_remain - n;
          if (d_remain == 0){
            // d_state = STATE_TX2;
            // d_remain = d_dur1;


            d_state = STATE_WAITING_TRIG;
            d_remain = 0;
            d_wait = 0;
            d_phase = 0;
            d_phase_rec = 0;

            tag_t tag_eob;
            tag_eob.offset = nitems_written(0)+ n -1;
            tag_eob.key = pmt::mp("tx_eob");
            tag_eob.value = pmt::PMT_T;              
            add_item_tag(0, tag_eob);
          }
        }
        // else if (d_state == STATE_TX2) {
        //   n = std::min(d_remain, (uint16_t) noutput_items);
        //   for (int i = 0;i<n;i++){
        //     out[i] = d_mag1;
        //   }
        //   d_remain = d_remain - n;
        //   if (d_remain == 0){
        //     d_state = STATE_WAITING_TRIG;
        //     d_remain = 0;
        //     d_wait = 0;
        //     d_phase = 0;
        //     d_phase_rec = 0;

        //     tag_t tag_eob;
        //     tag_eob.offset = nitems_written(0)+ n -1;
        //     tag_eob.key = pmt::mp("tx_eob");
        //     tag_eob.value = pmt::PMT_T;              
        //     add_item_tag(0, tag_eob);
        //   }
        // }
        

      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return n;
    }

  } /* namespace howto */
} /* namespace gr */

