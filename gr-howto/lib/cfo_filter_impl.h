/* -*- c++ -*- */
/* 
 * Copyright 2021 gr-howto author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_CFO_FILTER_IMPL_H
#define INCLUDED_HOWTO_CFO_FILTER_IMPL_H

#include <howto/cfo_filter.h>

namespace gr {
  namespace howto {

    class cfo_filter_impl : public cfo_filter
    {
     private:
      // Nothing to declare in this block.

      bool d_first_samp;
      bool d_cycle_active;

      bool perform_oneshot;
      // bool last_trigger_level;
      int cnt_samp;
      int cnt_train_samp;
      float oneshot_cfo_est;
      float filter_cfo_est;

      gr_complex corr_sum;
      int d_Ntrain;
      int d_Ndelay;
      float d_Tcycle;
      float d_samp_rate;
      float Ts;
      float mag_iq;
      int cycle_samples;

      double d_v1m1, d_v2m1, d_v1m, d_v2m;

      std::vector<float> d_a;
      std::vector<float> d_b;

      double iirfilter(float x1,float y1);

     public:
      cfo_filter_impl(float samp_rate, int Ntrain, int Ndelay, float initial_frequency, std::vector<float> a, std::vector<float> b);
      ~cfo_filter_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_CFO_FILTER_IMPL_H */

