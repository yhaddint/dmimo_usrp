/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "detect_phase_impl.h"

#define F_PI ((float)(M_PI))

#include <math.h>

namespace gr {
  namespace howto {

    detect_phase::sptr
    detect_phase::make(float thresh, uint32_t delay)
    {
      return gnuradio::get_initial_sptr
        (new detect_phase_impl(thresh, delay));
    }

    /*
     * The private constructor
     */
    detect_phase_impl::detect_phase_impl(float thresh, uint32_t delay)
      : gr::sync_block("detect_phase",
              gr::io_signature::make(3, 3, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(float))),
    d_thresh(thresh),
    d_delay(delay),
    d_hold(1200),
    d_remain(0),
    d_out(0)
    {}

    /*
     * Our virtual destructor.
     */
    detect_phase_impl::~detect_phase_impl()
    {
    }

    int
    detect_phase_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const float *in_mag = (const float *) input_items[0];
      const float *in_i = (const float *) input_items[1];
      const float *in_q = (const float *) input_items[2];
      float *out = (float *) output_items[0];
      float phase1,phase2;


      int ninput_items = noutput_items;

      if (ninput_items < d_remain){
        noutput_items =ninput_items;
        d_remain = d_remain - ninput_items ;
      }
      else{
        for (int i =d_remain;i<ninput_items;i++){
          if (in_mag[i]>d_thresh){
            if (i+d_delay>ninput_items){
              // if not enough input
              // Do nothing leave it for next time
              noutput_items = i;
            }
            else{
              d_remain = d_hold;
;
              float si1,si2 =0;
              float sq1,sq2 =0;
              for (int j = 5 ; j< d_delay-10;j++){
                si1 = in_i[i+5];
                sq1 = in_q[i+5];
                si2 = in_i[i+d_delay+5];
                sq2 = in_q[i+d_delay+5];
               }
               si1=si1/(d_delay-10);
               sq1=sq1/(d_delay-10);
               si2=si2/(d_delay-10);
               sq2=sq2/(d_delay-10);
              phase1 = atan2(sq1 , si1);
              phase2 = atan2(sq2,si2);

              d_out = d_out + (phase2-phase1) ;
              d_out=std::fmod(d_out + F_PI, 2.0f * F_PI) - F_PI;
              d_remain = d_hold;
              break;

            }
          }
        }
      }
      for (int i =0;i<noutput_items;i++){
          out[i] = d_out;
       } 

       // TODO: Need to handle when threshold goes low
       // in case there are many windows inside same sample


      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

