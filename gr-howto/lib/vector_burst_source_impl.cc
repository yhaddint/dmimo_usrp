/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "vector_burst_source_impl.h"

#include <ctime>

namespace gr {
  namespace howto {

    vector_burst_source::sptr
    vector_burst_source::make(const std::vector<gr_complex> &data, float time)
    {
      return gnuradio::get_initial_sptr
        (new vector_burst_source_impl(data, time));
    }

    /*
     * The private constructor
     */
    vector_burst_source_impl::vector_burst_source_impl(const std::vector<gr_complex> &data, float time)
      : gr::sync_block("vector_burst_source",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_data(data),
      d_time(time),
      d_last(0)
    {
      set_output_multiple(data.size());
    }

    /*
     * Our virtual destructor.
     */
    vector_burst_source_impl::~vector_burst_source_impl()
    {
    }

    int
    vector_burst_source_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      gr_complex *out = (gr_complex *) output_items[0];


      unsigned int size = d_data.size ();
      

      int n = 0;

      clock_t begin_time = clock();
      float now = float( clock ()) /  CLOCKS_PER_SEC;

      if   ( now  - d_last < d_time ){
        return 0;
      }


      tag_t tag_sob;
      tag_sob.offset = nitems_written(0) ;
      tag_sob.key = pmt::mp("tx_sob");
      tag_sob.value = pmt::PMT_T;
      add_item_tag(0, tag_sob);

      for(int i = 0; i < size; i++) {
          out[i] = d_data[i];
      }
              

      tag_t tag_eob;
      tag_eob.offset = nitems_written(0)+ size - 1 ;
      tag_eob.key = pmt::mp("tx_eob");
      tag_eob.value = pmt::PMT_T;              
      add_item_tag(0, tag_eob);

          // std::cout <<n <<" "<< std::endl;
   
      noutput_items = size;
      

      d_last = float( clock ()) /  CLOCKS_PER_SEC;;


      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

