/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HOWTO_CFOESTSYNC_IMPL_H
#define INCLUDED_HOWTO_CFOESTSYNC_IMPL_H

#include <howto/CFOestsync.h>

namespace gr {
  namespace howto {

    class CFOestsync_impl : public CFOestsync
    {
     private:
      // Nothing to declare in this block.
      // friend howto_CFOestsync_impl_sptr howto_CFOestsync_impl (int update_rate);
      // howto_CFOestsync_impl (int update_rate);
        int d_update_rate;
        float d_samp_rate;
        gr_complex corr_sum;
        int samp_cnt;
        float CFO;

     public:
      CFOestsync_impl(int update_rate, float samp_rate);
      ~CFOestsync_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
    };

  } // namespace howto
} // namespace gr

#endif /* INCLUDED_HOWTO_CFOESTSYNC_IMPL_H */

