/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define STATE_WAITING_TRIGGER 0
#define STATE_WAITING_SIG 1
#define STATE_OUTPUT 2


#include <gnuradio/io_signature.h>
#include "log_bf_impl.h"

namespace gr {
  namespace howto {

    log_bf::sptr
    log_bf::make(uint32_t delay, uint32_t downsample, uint32_t caplen)
    {
      return gnuradio::get_initial_sptr
        (new log_bf_impl(delay, downsample,caplen));
    }

    /*
     * The private constructor
     */
    log_bf_impl::log_bf_impl(uint32_t delay, uint32_t downsample, uint32_t caplen)
      : gr::block("log_bf",
              gr::io_signature::make2(2, 2, sizeof(char),sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(float))),
      d_delay(delay),
      d_downsample(downsample),
      d_state(STATE_WAITING_TRIGGER),
      d_caplen(caplen),
      d_remain(0)
    {
      // set_output_multiple(downsample);
    }

    /*
     * Our virtual destructor.
     */
    log_bf_impl::~log_bf_impl()
    {
    }

    void
    log_bf_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */ 
      ninput_items_required[0] = noutput_items;
      ninput_items_required[1] = noutput_items;
    }

    int
    log_bf_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const char *trig_in = (const char *) input_items[0];
      const float *mag_in = (const float *) input_items[1];
      float *out = (float *) output_items[0];



      uint32_t ninput_items_total = std::min(ninput_items[0],ninput_items[1]);
      uint32_t current_nop = 0;

      int trig_loc = 0;
      int op_loc = 0;

      if (d_state == STATE_WAITING_TRIGGER){
        for (int i=0; i<ninput_items_total;i++){
          if (trig_in[i]==1){
            trig_loc = i;
            d_state = STATE_WAITING_SIG;
            d_remain = d_delay;
            break;
          }
        }

      }

      if (d_state == STATE_WAITING_SIG){
        if (ninput_items_total - trig_loc >= d_remain){
          op_loc = trig_loc + d_remain;
          d_remain = d_caplen;
          d_downsample_offset = op_loc%d_downsample;
          d_state = STATE_OUTPUT;
        }
        else{
          d_remain = d_remain - (ninput_items_total - trig_loc);
        }
      }


      if (d_state == STATE_OUTPUT){
        uint32_t max_op = std::min((uint32_t)noutput_items,ninput_items_total - op_loc);
        current_nop = (std::min(max_op,d_remain)/d_downsample)*d_downsample;

        for (int i = 0;i<current_nop/d_downsample;i++){
          out[i] = mag_in[i*d_downsample+(op_loc/d_downsample)*d_downsample+d_downsample_offset];
        }
        d_remain = d_remain - current_nop; 
        // std::cout << current_nop << std::endl;

        if (d_remain==0){
          d_state = STATE_WAITING_TRIGGER;
        }

        ninput_items_total = op_loc + current_nop;
      }
      noutput_items = current_nop/d_downsample;
      // Process output state

      // Do <+signal processing+>
      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume_each (ninput_items_total);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace howto */
} /* namespace gr */

